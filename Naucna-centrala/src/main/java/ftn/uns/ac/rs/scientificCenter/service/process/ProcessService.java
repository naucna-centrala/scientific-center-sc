package ftn.uns.ac.rs.scientificCenter.service.process;

import ftn.uns.ac.rs.scientificCenter.constants.CamundaConstants;
import ftn.uns.ac.rs.scientificCenter.dto.*;
import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.Role;
import ftn.uns.ac.rs.scientificCenter.security.SecurityUser;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.form.type.EnumFormType;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.task.CompleteTaskDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProcessService {

    private static final Logger logger = LoggerFactory.getLogger(ProcessService.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    FormService formService;

    @Autowired
    IdentityService identityService;

    public String startProcess(String key, Map<String, VariableValueDto> variables) {
        StartProcessInstanceDto startProcessInstanceDto = new StartProcessInstanceDto();
        startProcessInstanceDto.setVariables(variables);
        System.out.println(String.format(CamundaConstants.ENGINE_BASE_URL + CamundaConstants.START_PROCESS, key));
        ProcessInstanceDto response = restTemplate.postForObject(String.format(CamundaConstants.ENGINE_BASE_URL +
                CamundaConstants.START_PROCESS, key), startProcessInstanceDto, ProcessInstanceDto.class);
        return response.getId();
    }

    public List<TaskDto> getTasks(String processId, String assignee) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(CamundaConstants.ENGINE_BASE_URL + CamundaConstants.TASK);

        if (processId != null) {
            builder.queryParam("processInstanceId", processId);
        }

        if (assignee != null) {
            builder.queryParam("assignee", assignee);
        }

        ParameterizedTypeReference<List<TaskDto>> returnType = new ParameterizedTypeReference<List<TaskDto>>() {
        };
        List<TaskDto> res = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, null, returnType).getBody();
        return res;
    }

    public FormFieldsDTO getTaskFormData(String taskId, String processId) {

        TaskFormData tfd = formService.getTaskFormData(taskId);
        List<FormField> properties = tfd.getFormFields();
        List<FormDataDTO> data = new ArrayList<>();

        for(FormField fp : properties) {

            if(fp.getId().equals("Scientific area")){
                Object o = getVariable(processId, "all_scientific_area");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("all_scientific_area", value));
            }else if(fp.getId().equals("Add Reviewer")){
                Object o = getVariable(processId, "all_reviewers_list");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("all_reviewers_list", value));
            }else if(fp.getId().equals("Add Editor")){
                Object o = getVariable(processId, "all_editors_list");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("all_editors_list", value));
            }else if(fp.getId().equals("Reviewers")){
                Object o = getVariable(processId, "reviewer_list");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("reviewer_list", value));
            }else if(fp.getId().equals("Editors")){
                Object o = getVariable(processId, "editor_list");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("editor_list", value));
            }else if(fp.getId().equals("Scientific areas")){
                Object o = getVariable(processId, "scientific_area_list");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("scientific_area_list", value));
            }else if(fp.getId().equals("Magazine_choose")){
                Object o = getVariable(processId, "magazine_list");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("magazine_list", value));
            }else if(fp.getId().equals("paper_field")){
                Object o = getVariable(processId, "paper_fields");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("paper_fields", value));

            }else if(fp.getId().equals("coauthors")){
                Object o = getVariable(processId, "list_coauthors");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("list_coauthors", value));
            }else if(fp.getId().equals("more info")){
                List<String> objects = Arrays.asList("picked_field", "list_coauthors", "magazine", "file_name");
                for (String title:objects) {
                    Object o = getVariable(processId, title);
                    VariableValueDto value = new VariableValueDto();
                    value.setValue(o);
                    value.setType("Object");
                    data.add(new FormDataDTO(title, value));
                }
            }else if(fp.getId().equals("Number of reviewers for paper")){
                List<String> objects = Arrays.asList("Title", "list_all_reviewers", "picked_reviewers");
                for (String title:objects) {
                    Object o = getVariable(processId, title);
                    VariableValueDto value = new VariableValueDto();
                    value.setValue(o);
                    value.setType("Object");
                    data.add(new FormDataDTO(title, value));
                }
            }else if(fp.getId().equals("chosen_reviewers")){
                List<String> objects = Arrays.asList("Title","Number of reviewers for paper", "list_all_reviewers", "picked_reviewers");
                for (String title:objects) {
                    Object o = getVariable(processId, title);
                    VariableValueDto value = new VariableValueDto();
                    value.setValue(o);
                    value.setType("Object");
                    data.add(new FormDataDTO(title, value));
                }
            }else if(fp.getId().equals("Deadline")){
                Object o = getVariable(processId, "Title");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("Title", value));
            }else if(fp.getId().equals("all_rev")){
                Object o = getVariable(processId, "list_reviews");
                VariableValueDto value = new VariableValueDto();
                value.setValue(o);
                value.setType("Object");
                data.add(new FormDataDTO("list_reviews", value));
            }

            VariableValueDto value = new VariableValueDto();
            value.setType(fp.getType().getName());

            if(fp.getType().getName().equals("enum")){
                EnumFormType enumFormType = (EnumFormType) fp.getType();
                Map<String, String> values = enumFormType.getValues();
                value.setValue(values);
            }else{
                value.setValue(fp.getValue().getValue());

            }
            data.add(new FormDataDTO(fp.getId(), value));

        }

        logger.info("Get all fields");

        return new FormFieldsDTO(taskId, data, processId);
    }

    public void processTaskForm(String taskId, List<FormDataSubmitDTO> formFieldDtos){
        List<FormDataDTO> listForm = new ArrayList<>();

        for (FormDataSubmitDTO data:formFieldDtos) {
            VariableValueDto value = new VariableValueDto();
            value.setValue(data.getValue());
            FormDataDTO newSubmit = new FormDataDTO(data.getName(),value);
            listForm.add(newSubmit);
        }

        submitTaskForm(taskId,listForm);
    }

    public void processTaskFormObject(String taskId, List<FormDataSubmitObjectDTO> formFieldDtos) {

        List<FormDataDTO> listForm = new ArrayList<>();
        for (FormDataSubmitObjectDTO data:formFieldDtos) {
            VariableValueDto value = new VariableValueDto();
            value.setValue(data.getValue());
            FormDataDTO newSubmit = new FormDataDTO(data.getName(),value);
            listForm.add(newSubmit);
        }
       submitTaskForm(taskId,listForm);
    }

    public void processMixDataForm(String taskId, FormMixDataSubmitDTO mixDataSubmitDTO){
        List<FormDataDTO> listForm = new ArrayList<>();

        for (FormDataSubmitDTO data:mixDataSubmitDTO.getData()) {
            VariableValueDto value = new VariableValueDto();
            value.setValue(data.getValue());
            FormDataDTO newSubmit = new FormDataDTO(data.getName(),value);
            listForm.add(newSubmit);
        }

        for (FormDataSubmitObjectDTO data:mixDataSubmitDTO.getDataObject()) {
            VariableValueDto value = new VariableValueDto();
            value.setValue(data.getValue());
            FormDataDTO newSubmit = new FormDataDTO(data.getName(),value);
            listForm.add(newSubmit);
        }

        submitTaskForm(taskId,listForm);

    }

    public void submitTaskForm(String taskId, List<FormDataDTO> listForm) {

        String url = String.format(CamundaConstants.ENGINE_BASE_URL + CamundaConstants.SUBMIT_FORM, taskId);
        CompleteTaskDto completeTaskDto = new CompleteTaskDto();
        if(listForm.size()!=0){
            completeTaskDto.setVariables(listForm.stream().collect(Collectors.toMap(FormDataDTO::getName, FormDataDTO::getValue)));

        }
        restTemplate.postForLocation(url, completeTaskDto);

    }
    public void setVariable(String processInstanceId, String variableName, Object variableValue) {
        runtimeService.setVariable(processInstanceId, variableName, variableValue);

    }

    public Object getVariable(String processId, String key) {
        String url = String.format(CamundaConstants.ENGINE_BASE_URL + CamundaConstants.GET_VARIABLE, processId, key);

        return restTemplate.getForObject(url, VariableValueDto.class).getValue();
    }

    public void makeGroups(){

        Group authors = identityService.newGroup("authors");
        Group editors = identityService.newGroup("editors");
        Group reviewers = identityService.newGroup("reviewers");

        identityService.saveGroup(authors);
        identityService.saveGroup(editors);
        identityService.saveGroup(reviewers);

        log.info("Groups are made!");

    }

    public void insertUserIntoGroup(Collection<Role> types, SecurityUser user){

        for (Role type:types) {
            if(type.getName().equals(UserRole.AUTHOR)){
                List<User> users = identityService.createUserQuery().userIdIn(user.getUsername()).list();
                if(users.isEmpty()){
                    User u = identityService.newUser(user.getUsername());
                    u.setEmail(user.getEmail());
                    identityService.saveUser(u);
                    identityService.createMembership(user.getUsername(),"authors");
                    log.info("User with username " + user.getUsername() + " successfully added to a group authors!");
                }
            }else if(type.getName().equals(UserRole.EDITOR)){
                List<User> users = identityService.createUserQuery().userIdIn(user.getUsername()).list();
                if(users.isEmpty()){
                    User u = identityService.newUser(user.getUsername());
                    u.setEmail(user.getEmail());
                    identityService.saveUser(u);
                    identityService.createMembership(user.getUsername(),"editors");
                    log.info("User with username " + user.getUsername() + " successfully added to a group editors!");
                }

            }else if(type.getName().equals(UserRole.REVIEWER)){
                List<User> users = identityService.createUserQuery().userIdIn(user.getUsername()).list();
                if(users.isEmpty()){
                    User u = identityService.newUser(user.getUsername());
                    u.setEmail(user.getEmail());
                    identityService.saveUser(u);
                    identityService.createMembership(user.getUsername(),"reviewers");
                    log.info("User with username " + user.getUsername() + " successfully added to a group reviewers!");
                }
            }
        }

    }

}
