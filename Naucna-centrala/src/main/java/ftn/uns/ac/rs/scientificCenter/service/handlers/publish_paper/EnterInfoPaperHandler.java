package ftn.uns.ac.rs.scientificCenter.service.handlers.publish_paper;

import ftn.uns.ac.rs.scientificCenter.model.Coauthor;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;

@Service
@Slf4j
public class EnterInfoPaperHandler implements TaskListener {

    @Autowired
    IMagazineService magazineService;

    @Override
    public void notify(DelegateTask delegateTask) {

        Object o = delegateTask.getVariable("magazine");
        String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
        Long id = Long.parseLong(token[0].split("=")[1]);

        Magazine m = magazineService.findById(id);
        Collection<ScientificField> fields = m.getMagazine_fields();

        delegateTask.setVariable("paper_fields", fields);

        delegateTask.setVariable("list_coauthors", new ArrayList<Coauthor>());

        delegateTask.setVariable("picked_field", new ScientificField());
    }
}
