package ftn.uns.ac.rs.scientificCenter.service.tasks.registration;

import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.*;
import ftn.uns.ac.rs.scientificCenter.service.implementation.UserDetailsServiceImpl;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IConfirmationTokenService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEmailSenderService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IRoleService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IScientificFieldService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class SaveDataAndSendEmailRegistrationTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(SaveDataAndSendEmailRegistrationTask.class);

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    IConfirmationTokenService confirmationTokenService;

    @Autowired
    IEmailSenderService emailSenderService;

    @Autowired
    IScientificFieldService scientificFieldService;

    @Autowired
    IRoleService roleService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        List<Object> objects = (List<Object>) delegateExecution.getVariable("scientific_area_list");
        List<ScientificField> fields = new ArrayList<>();
        for (Object o:objects) {
            String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
            Long id = Long.parseLong(token[0].split("=")[1]);
            Optional<ScientificField> f = this.scientificFieldService.findById(id);
            fields.add(f.get());
        }

        User user = null;

        if(delegateExecution.getVariable("Reviewer").toString().equals("true")){

            Reviewer r = new Reviewer();

            r.setState((String)delegateExecution.getVariable("State"));
            r.setCity((String)delegateExecution.getVariable("City"));
            r.setEmail((String)delegateExecution.getVariable("Email"));
            r.setFirst_name((String)delegateExecution.getVariable("Firstname"));
            r.setLast_name((String)delegateExecution.getVariable("Lastname"));
            r.setPassword(encoder.encode((String)delegateExecution.getVariable("Password")));
            Role ro = this.roleService.findByName(UserRole.REVIEWER);
            r.getRoles().add(ro);
            r.setUsername((String)delegateExecution.getVariable("Username"));
            r.setUser_fields(fields);
            r.setEnabled(false);
            r.setVerified(false);

            user = this.userDetailsService.save(r);
            logger.info("New reviewer saved!");

        }else{

            Author u = new Author();

            u.setState((String)delegateExecution.getVariable("State"));
            u.setCity((String)delegateExecution.getVariable("City"));
            u.setEmail((String)delegateExecution.getVariable("Email"));
            u.setFirst_name((String)delegateExecution.getVariable("Firstname"));
            u.setLast_name((String)delegateExecution.getVariable("Lastname"));
            u.setPassword(encoder.encode((String)delegateExecution.getVariable("Password")));
            Role r = this.roleService.findByName(UserRole.AUTHOR);
            u.getRoles().add(r);
            u.setUsername((String)delegateExecution.getVariable("Username"));
            u.setUser_fields(fields);
            u.setEnabled(false);

            user = this.userDetailsService.save(u);
            logger.info("New author saved!");

        }

        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        this.confirmationTokenService.save(confirmationToken);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("test.upp.fax@gmail.com");
        mailMessage.setText("To confirm your account, please click here : "
                +"https://localhost:8088/register/confirm-account?token="+confirmationToken.getConfirmationToken()
        +"&process=" + delegateExecution.getProcessInstanceId());

        this.emailSenderService.sendEmail(mailMessage);
        logger.info("Email sent!");

        delegateExecution.setVariable("emailConfirm", false);
        delegateExecution.setVariable("paymentAdded",false);


    }
}
