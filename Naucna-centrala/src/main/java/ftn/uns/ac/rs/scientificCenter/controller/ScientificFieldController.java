package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.dto.ScientificFieldDTO;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IScientificFieldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/field")
@CrossOrigin("https://localhost:4200")
public class ScientificFieldController {

    private static final Logger logger = LoggerFactory.getLogger(ScientificFieldController.class);

    @Autowired
    IScientificFieldService scientificFieldService;

    @GetMapping(path = "/get", produces = "application/json")
    public @ResponseBody List<ScientificField> getAllFields(){
        logger.info("Get all scientific fields");
        return scientificFieldService.findAll();
    }

    @PostMapping(path = "/add", produces = "application/json")
    public @ResponseBody ScientificField addField(@RequestBody ScientificFieldDTO fieldDTO){
        logger.info("Add new scientific field");
        return scientificFieldService.save(fieldDTO);
    }
}
