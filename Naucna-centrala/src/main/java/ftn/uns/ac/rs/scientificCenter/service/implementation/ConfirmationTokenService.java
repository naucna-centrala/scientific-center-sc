package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.ConfirmationToken;
import ftn.uns.ac.rs.scientificCenter.model.User;
import ftn.uns.ac.rs.scientificCenter.repository.ConfirmationTokenRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IConfirmationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmationTokenService implements IConfirmationTokenService {

    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository;

    @Override
    public ConfirmationToken save(ConfirmationToken token) {
        return confirmationTokenRepository.save(token);
    }

    @Override
    public void delete(User user) {
        ConfirmationToken token = confirmationTokenRepository.findByUser(user);
        confirmationTokenRepository.delete(token);
    }
}
