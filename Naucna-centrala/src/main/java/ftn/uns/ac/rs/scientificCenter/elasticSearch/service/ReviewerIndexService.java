package ftn.uns.ac.rs.scientificCenter.elasticSearch.service;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultReviewerDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.Location;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ReviewerIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ScientificFieldIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.repository.PaperIndexRepository;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.repository.ReviewerIndexRepository;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.IReviewerIndexService;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ReviewerIndexService implements IReviewerIndexService {

    @Autowired
    ReviewerIndexRepository reviewerIndexRepository;

    @Autowired
    PaperIndexRepository paperIndexRepository;

    @Autowired
    LocationService locationService;


    @Override
    public void indexReviewer(Reviewer reviewer) {

        ReviewerIndex reviewerIndex = new ReviewerIndex();
        reviewerIndex.setCity(reviewer.getCity());
        reviewerIndex.setEmail(reviewer.getEmail());
        reviewerIndex.setFirst_name(reviewer.getFirst_name());
        reviewerIndex.setId(reviewer.getId());
        reviewerIndex.setLast_name(reviewer.getLast_name());
        reviewerIndex.setState(reviewer.getState());
        reviewerIndex.setUsername(reviewer.getUsername());

        for (ScientificField field:reviewer.getUser_fields()) {
            ScientificFieldIndex scientificFieldIndex = new ScientificFieldIndex(field.getId(),field.getTitle());
            reviewerIndex.getFields().add(scientificFieldIndex);

        }

        for (Paper paper:reviewer.getReviewers_paper()) {
            PaperIndex paperIndex = paperIndexRepository.findById(paper.getFilename()).get();
            if(paperIndex == null){
                log.error("Paper doesn't exist!");
            }
            reviewerIndex.getPapers().add(paperIndex);
        }


        Location location = locationService.findByTitle(reviewer.getCity());
        GeoPoint point = new GeoPoint(location.getLat() + "," + location.getLon());
        reviewerIndex.setLocation(point);

        reviewerIndexRepository.save(reviewerIndex);

        log.info("Reviewer with id  " + reviewer.getId() + " successfully indexed!");

    }

    @Override
    public List<FindResultReviewerDTO> findReviewerPaper(List<FindResultDTO> list) {

        List<FindResultReviewerDTO> finalList = new ArrayList<>();
        boolean check = false;

        Iterable<ReviewerIndex> allReviewers = reviewerIndexRepository.findAll();
        for (ReviewerIndex reviewer:allReviewers) {
            for (PaperIndex paper:reviewer.getPapers()) {
                for (FindResultDTO result:list) {
                    if(paper.getFilename().equals(result.getFilename())){
                        FindResultReviewerDTO f = new FindResultReviewerDTO(reviewer);
                        finalList.add(f);
                        check= true;
                        break;
                    }
                }
                if(check){
                    check = false;
                    break;
                }
            }
        }
        return finalList;
    }

}
