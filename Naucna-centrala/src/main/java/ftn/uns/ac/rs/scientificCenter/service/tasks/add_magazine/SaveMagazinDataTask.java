package ftn.uns.ac.rs.scientificCenter.service.tasks.add_magazine;

import ftn.uns.ac.rs.scientificCenter.dto.MagazineDTO;
import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import ftn.uns.ac.rs.scientificCenter.model.*;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.*;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaveMagazinDataTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(SaveMagazinDataTask.class);

    @Autowired
    IMagazineService magazineService;

    @Autowired
    IEditorialBoardService editorialBoardService;

    @Autowired
    IEditorService editorService;

    @Autowired
    IScientificFieldService scientificFieldService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    ISubscriptionService subscriptionService;


    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        MagazineDTO magazineDTO = new MagazineDTO();
        magazineDTO.setIssn((String)delegateExecution.getVariable("ISSN number"));
        magazineDTO.setName((String)delegateExecution.getVariable("Title"));

        if(delegateExecution.getVariable("Payment method").equals("WITH_SUBSCRIPTION")){
            magazineDTO.setPayment_method(PaymentMethod.WITH_SUBSCRIPTION);

        }else{
            magazineDTO.setPayment_method(PaymentMethod.OPEN_ACCESS);

        }

        List<Object> objects = (List<Object>) delegateExecution.getVariable("scientific_area_list");
        List<ScientificField> fields = new ArrayList<>();
        for (Object o:objects) {
            String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
            Long id = Long.parseLong(token[0].split("=")[1]);
            Optional<ScientificField> f = this.scientificFieldService.findById(id);
            fields.add(f.get());
        }
        EditorialBoard editorialBoard = new EditorialBoard();
        Editor main_editor = editorService.findByUsername((String)delegateExecution.getVariable("main_editor_username"));
        editorialBoard.setMain_editor(main_editor);
        EditorialBoard saveEditorialBoard = editorialBoardService.save(editorialBoard);
        magazineDTO.setEditorialBoard(saveEditorialBoard);

        Magazine m = this.magazineService.save(magazineDTO,fields);
        logger.info("Magazine with " + m.getUuid() + "uuid successfully created!");

        delegateExecution.setVariable("Main editor",delegateExecution.getVariable("main_editor_username") );


    }
}
