package ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultReviewerDTO;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import java.util.List;

public interface IReviewerIndexService {

    void indexReviewer(Reviewer reviewer);
    List<FindResultReviewerDTO> findReviewerPaper(List<FindResultDTO> list);
}
