package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.Coauthor;
import ftn.uns.ac.rs.scientificCenter.repository.CoauthorRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.ICoauthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoauthorService implements ICoauthorService {

    @Autowired
    CoauthorRepository coauthorRepository;

    @Override
    public Coauthor save(Coauthor coauthor){
        return coauthorRepository.save(coauthor);
    }

    @Override
    public Coauthor findById(Long id){
        return coauthorRepository.findById(id).get();
    }

}
