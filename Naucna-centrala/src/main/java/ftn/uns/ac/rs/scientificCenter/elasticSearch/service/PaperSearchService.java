package ftn.uns.ac.rs.scientificCenter.elasticSearch.service;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.AuthorIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.repository.PaperIndexRepository;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.IPaperSearchService;
import ftn.uns.ac.rs.scientificCenter.model.Coauthor;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.text.PDFTextStripper;

@Service
@Slf4j
public class PaperSearchService implements IPaperSearchService {

    private final Path dirLocation = Paths.get("src/main/resources/files");

    @Autowired
    PaperIndexRepository paperIndexRepository;

    @Override
    public void indexPaper(Paper paper) {

        PaperIndex paperIndex = new PaperIndex();
        paperIndex.setAbstract_paper(paper.getAbstract_paper());
        paperIndex.setField(paper.getPaper_fields().getTitle());
        paperIndex.setFilename(paper.getFilename());
        paperIndex.setKeywords(paper.getKeywords());
        paperIndex.setMagazine_name(paper.getMagazine().getName());
        paperIndex.setTitle(paper.getTitle());
        paperIndex.setAccess(paper.getMagazine().getPayment_method().toString());

        if(paper.getCoauthor().size() != 0){
            for (Coauthor a:paper.getCoauthor()) {
                AuthorIndex author = new AuthorIndex(a.getId(),a.getFirst_name(),a.getLast_name(),a.getCity());
                paperIndex.getAuthors().add(author);
            }
        }

        paperIndex.getAuthors().add(new AuthorIndex(paper.getAuthor().getId(),paper.getAuthor().getFirst_name(),
                paper.getAuthor().getLast_name(), paper.getAuthor().getCity()));

        Path path = dirLocation.resolve(paper.getFilename() + ".pdf");
        File paperFile = path.toFile();
        paperIndex.setText(getText(paperFile));

        paperIndexRepository.save(paperIndex);

        log.info("Paper " + paper.getTitle() + " successfully indexed!");

    }

    @Override
    public String getText(File file) {
        try {
            PDFParser parser = new PDFParser(new RandomAccessFile(file, "r"));
            parser.parse();
            PDFTextStripper textStripper = new PDFTextStripper();
            String text = textStripper.getText(parser.getPDDocument());

            log.info("Get text from pdf finished!");
            return text;
        } catch (IOException e) {
            System.err.println("Error while converting document to pdf");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public PaperIndex add(PaperIndex paper) {
        return paperIndexRepository.index(paper);
    }

    @Override
    public void delete(PaperIndex paper) {
        paperIndexRepository.delete(paper);
    }

    @Override
    public Iterable<PaperIndex> findAll() {
        return paperIndexRepository.findAll();
    }

    @Override
    public PaperIndex findById(String filename) {
        return paperIndexRepository.findById(filename).get();
    }

    @Override
    public String getPaperText(String filename){
        PaperIndex paper = findById(filename);
        if(paper == null){
            return null;
        }
        String text = paper.getText();
        return text;
    }


}
