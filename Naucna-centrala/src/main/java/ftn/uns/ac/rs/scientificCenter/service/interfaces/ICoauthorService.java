package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.Coauthor;

public interface ICoauthorService {

    Coauthor save(Coauthor coauthor);
    Coauthor findById(Long id);
}
