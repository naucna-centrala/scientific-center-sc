package ftn.uns.ac.rs.scientificCenter.service.handlers.publish_paper;

import ftn.uns.ac.rs.scientificCenter.dto.ReviewerPaperDTO;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IScientificFieldService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class AddReviewersPaperHandler implements TaskListener {

    @Autowired
    IMagazineService magazineService;

    @Autowired
    IScientificFieldService scientificFieldService;

    @Override
    public void notify(DelegateTask delegateTask) {

        List<ReviewerPaperDTO> reviewers = new ArrayList<>();

        Object o = delegateTask.getVariable("magazine");
        String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
        Long id = Long.parseLong(token[0].split("=")[1]);

        Magazine m = magazineService.findById(id);

        Object objects = delegateTask.getVariable("picked_field");
        String[] t = objects.toString().substring(1,objects.toString().length()-1).split(",");
        Long id_field = Long.parseLong(t[0].split("=")[1]);
        ScientificField field = scientificFieldService.findById(id_field).get();

        for (Reviewer r:m.getEditorialBoard().getReviewers()) {
            if(r.getUser_fields().contains(field)){
                reviewers.add(new ReviewerPaperDTO(r.getId(),r.getFirst_name(),r.getLast_name(),r.getUsername()));
            }
        }

        if(reviewers.isEmpty() || reviewers.size() < 2){
            reviewers.add(new ReviewerPaperDTO(m.getEditorialBoard().getMain_editor().getId(),
                    m.getEditorialBoard().getMain_editor().getFirst_name(), m.getEditorialBoard().getMain_editor().getLast_name(),
                    m.getEditorialBoard().getMain_editor().getUsername()));
        }

        delegateTask.setVariable("list_all_reviewers", reviewers);
        delegateTask.setVariable("picked_reviewers", new ArrayList<>());

        log.info("Reviewer list for paper made!");

    }
}
