package ftn.uns.ac.rs.scientificCenter.security;

import ftn.uns.ac.rs.scientificCenter.model.User;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.*;
import java.util.stream.Collectors;

@Data
//@NoArgsConstructor
public class SecurityUser implements UserDetails {
	private static final long serialVersionUID = -949811899438278427L;

	private Long id;
	private String username;
	private String password;
	private String email;
	private String firstname;
	private String lastname;
	private String city;
	private String state;
	private Collection<? extends GrantedAuthority> authorities;

	public SecurityUser(User user , Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.firstname = user.getFirst_name();
		this.lastname = user.getLast_name();
		this.city = user.getCity();
		this.state = user.getState();
		this.authorities = authorities;
	}

	public static SecurityUser build(User user) {
		List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());

		return new SecurityUser(user,authorities);
	}
	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getPassword() {
		return this.password;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SecurityUser user = (SecurityUser) o;
		return Objects.equals(id, user.id);
	}





}
