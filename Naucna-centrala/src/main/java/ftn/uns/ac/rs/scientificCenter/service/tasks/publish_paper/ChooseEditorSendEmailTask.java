package ftn.uns.ac.rs.scientificCenter.service.tasks.publish_paper;

import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.*;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Slf4j
public class ChooseEditorSendEmailTask implements JavaDelegate {

    @Autowired
    IMagazineService magazineService;

    @Autowired
    IEditorService editorService;

    @Autowired
    IScientificFieldService scientificFieldService;

    @Autowired
    IEmailSenderService emailSenderService;

    @Autowired
    IPaperService paperService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String email;

        Object objects = delegateExecution.getVariable("picked_field");
        String[] t = objects.toString().substring(1,objects.toString().length()-1).split(",");
        Long id_field = Long.parseLong(t[0].split("=")[1]);
        ScientificField field = this.scientificFieldService.findById(id_field).get();

        Object o = delegateExecution.getVariable("magazine");
        String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
        Long id = Long.parseLong(token[0].split("=")[1]);

        Magazine m = magazineService.findById(id);

        Collection<Editor> editors = m.getEditorialBoard().getEditors();

        if(editors.isEmpty()){
            delegateExecution.setVariable("chosen_editor", delegateExecution.getVariable("main_editor_username"));
            Editor e = editorService.findByUsername(delegateExecution.getVariable("main_editor_username").toString());
            email = e.getEmail();
        }else{
            Editor picked = null;
            for (Editor editor:editors) {
                for (ScientificField f:editor.getUser_fields()) {
                    if(f.getId() == field.getId()){
                        picked = editor;
                        break;
                    }
                }
            }

            if(picked == null){
                delegateExecution.setVariable("chosen_editor", delegateExecution.getVariable("main_editor_username"));
                Editor e = editorService.findByUsername(delegateExecution.getVariable("main_editor_username").toString());
                email = e.getEmail();
            }else{
                Editor e = editorService.findByUsername(picked.getUsername());
                email = e.getEmail();
                delegateExecution.setVariable("chosen_editor", e.getUsername());

            }

        }

        Paper p = paperService.findById((Long)delegateExecution.getVariable("paper_id"));

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("New paper for review!");
        mailMessage.setFrom("test.upp.fax@gmail.com");
        mailMessage.setText("There is new paper to be review! Paper details:"+ System.lineSeparator() +
                "Title: " + p.getTitle() + System.lineSeparator() + "Keywords: " +
                p.getKeywords() + System.lineSeparator() + "Abstract: " + p.getAbstract_paper() + System.lineSeparator()+
                "Author: " + p.getAuthor().getFirst_name() +" " + p.getAuthor().getLast_name() + System.lineSeparator() +
                "Magazine: " + p.getMagazine().getName());

        this.emailSenderService.sendEmail(mailMessage);
        log.info("Email sent to " + email + " for paper to be review");

        delegateExecution.setVariable("list_reviews", new ArrayList<>());

    }
}
