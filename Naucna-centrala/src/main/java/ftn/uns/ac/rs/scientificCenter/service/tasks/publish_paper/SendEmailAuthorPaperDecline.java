package ftn.uns.ac.rs.scientificCenter.service.tasks.publish_paper;

import ftn.uns.ac.rs.scientificCenter.model.Author;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IAuthorService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEmailSenderService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IHandleFileService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SendEmailAuthorPaperDecline implements JavaDelegate {

    @Autowired
    IAuthorService authorService;

    @Autowired
    IMagazineService magazineService;

    @Autowired
    IEmailSenderService emailSenderService;

    @Autowired
    IHandleFileService fileService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Author author = authorService.findByUsername(delegateExecution.getVariable("username_author").toString());

        String title = delegateExecution.getVariable("Title").toString();
        String abstract_paper = delegateExecution.getVariable("Abstract").toString();
        String keywords = delegateExecution.getVariable("Keywords").toString();

        Object o = delegateExecution.getVariable("magazine");
        String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
        Long id = Long.parseLong(token[0].split("=")[1]);
        Magazine m = magazineService.findById(id);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(author.getEmail());
        mailMessage.setSubject("Paper decline!");
        mailMessage.setFrom("test.upp.fax@gmail.com");
        mailMessage.setText("Your paper was decline because it was not thematic suitable! Paper details:"+ System.lineSeparator() +
                "Title: " + title + System.lineSeparator() + "Keywords: " +
                keywords + System.lineSeparator() + "Abstract: " + abstract_paper + System.lineSeparator()+
                "Author: " + author.getFirst_name() +" " + author.getLast_name() + System.lineSeparator() +
                "Magazine: " + m.getName());

        this.emailSenderService.sendEmail(mailMessage);
        log.info("Decline email sent to " + author.getEmail());

        fileService.deleteFile(delegateExecution.getVariable("file_name").toString());

    }
}
