package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.dto.ResponseMessage;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IHandleFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
@Slf4j
public class HandleFileService implements IHandleFileService {

    private final Path rootLocation = Paths.get("src/main/resources/files");
    private final Path angularLocation = Paths.get("../naucna-centrala-client/src/assets/papers");

    @Override
    public ResponseMessage storeFile(MultipartFile file){
        String message;
        String name = UUID.randomUUID().toString();
        InputStream inputStream = null;
        try {

            inputStream = file.getInputStream();
            Files.copy(inputStream, this.rootLocation.resolve(name + ".pdf"), StandardCopyOption.REPLACE_EXISTING);

            message = storeInAngular(file,name);

            log.info("Document with name" + name+ " successfully stored!");
            return new ResponseMessage(message);
        } catch (Exception e) {
            message = "Failed to upload!";
            log.error("Failed to store document!");

            return new ResponseMessage(message);
        }
    }

    @Override
    public String storeInAngular(MultipartFile file, String name){
        InputStream inputStream = null;
        try {

            inputStream = file.getInputStream();
            Files.copy(inputStream, this.angularLocation.resolve(name + ".pdf"), StandardCopyOption.REPLACE_EXISTING);

            log.info("Document with name" + name+ " successfully stored in angular!");
            return name;
        } catch (Exception e) {
            log.error("Failed to store document!");
            return name;

        }
    }

    @Override
    public void deleteFile(String filename) throws IOException {
        Files.delete(Paths.get("src/main/resources/files/" + filename + ".pdf"));
        Files.delete(Paths.get("../naucna-centrala-client/src/assets/papers/" + filename + ".pdf"));

        log.info("Document " + filename + " deleted");

    }

}
