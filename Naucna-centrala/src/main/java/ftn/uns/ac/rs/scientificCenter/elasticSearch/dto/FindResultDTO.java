package ftn.uns.ac.rs.scientificCenter.elasticSearch.dto;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.AuthorIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Field;

import java.util.ArrayList;
import java.util.Collection;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindResultDTO {

    private String filename;
    private String title;
    private String keywords;
    private String abstract_paper;
    private String magazine_name;
    private String field;
    private String text;
    private Collection<AuthorIndex> authors = new ArrayList<>();
    private String hightlight;
    private String access;

    public FindResultDTO(PaperIndex index){
        this.filename = index.getFilename();
        this.title = index.getTitle();
        this.keywords = index.getKeywords();
        this.abstract_paper = index.getAbstract_paper();
        this.magazine_name = index.getMagazine_name();
        this.field = index.getField();
        this.text = index.getText();
        this.authors = index.getAuthors();
        this.hightlight = index.getHightlight();
        this.access = index.getAccess();
    }
}
