package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.dto.MagazineDTO;
import ftn.uns.ac.rs.scientificCenter.dto.MagazineInfoDTO;
import ftn.uns.ac.rs.scientificCenter.dto.OrderDTO;
import ftn.uns.ac.rs.scientificCenter.dto.OrderResponseDTO;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import java.util.List;

public interface IMagazineService {

    Magazine save(MagazineDTO m, List<ScientificField> fields);
    OrderResponseDTO buyMagazine(OrderDTO order) throws Exception;
    List<Magazine> getAll();
    Magazine findByISSNNumber(String issn);
    Magazine updateActive(String issn, boolean active);
    Magazine findById(Long id);
    List<Magazine> findAllNotSubscribe(String username);
    List<Magazine> findAllSubscribe(String username);
    List<Magazine> findOpenAcess();
    List<MagazineInfoDTO> findAll();
}
