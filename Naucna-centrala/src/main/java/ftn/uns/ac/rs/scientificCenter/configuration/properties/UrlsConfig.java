package ftn.uns.ac.rs.scientificCenter.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import java.util.HashMap;

@Data
@Component
@RefreshScope
@ConfigurationProperties
public class UrlsConfig {
    private HashMap<String, String> endpoints = new HashMap<>();
}
