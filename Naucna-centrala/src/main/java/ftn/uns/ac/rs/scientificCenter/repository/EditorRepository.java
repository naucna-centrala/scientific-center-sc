package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.model.Editor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EditorRepository extends JpaRepository<Editor,Long> {

    Editor findByUsername(String username);
    Optional<Editor> findById(Long id);
}
