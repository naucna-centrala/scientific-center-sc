package ftn.uns.ac.rs.scientificCenter.elasticSearch.dto;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ReviewerIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ScientificFieldIndex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindResultReviewerDTO {

    private Long id;
    private String username;
    private String email;
    private String first_name;
    private String last_name;
    private String city;
    private String state;
    private Collection<ScientificFieldIndex> fields = new ArrayList<>();
    private Collection<PaperIndex> papers = new ArrayList<>();

    public FindResultReviewerDTO(ReviewerIndex index){
        this.id = index.getId();
        this.username = index.getUsername();
        this.email = index.getEmail();
        this.first_name = index.getFirst_name();
        this.last_name = index.getLast_name();
        this.city = index.getCity();
        this.state= index.getState();
        this.fields = index.getFields();
        this.papers = index.getPapers();
    }
}
