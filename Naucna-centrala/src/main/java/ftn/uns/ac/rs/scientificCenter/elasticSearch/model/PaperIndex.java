package ftn.uns.ac.rs.scientificCenter.elasticSearch.model;

import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Mapping;
import java.util.ArrayList;
import java.util.Collection;

@Document(indexName = "paperindex")
@Mapping(mappingPath = "mappings.json")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PaperIndex {

    @Id
    @Field(type = FieldType.Keyword)
    private String filename;

    private String title;
    private String keywords;
    private String abstract_paper;
    private String magazine_name;
    private String field;
    private String text;

    @Field(type = FieldType.Nested, includeInParent = true)
    private Collection<AuthorIndex> authors = new ArrayList<>();

    private String hightlight;
    private String access;
}
