package ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.Location;

import java.util.List;

public interface ILocationService {

    Location findByTitle(String title);
    List<Location> findLocationsAuthors(String filenamePaper);
}
