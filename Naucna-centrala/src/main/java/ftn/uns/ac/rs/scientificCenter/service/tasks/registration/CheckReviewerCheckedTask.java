package ftn.uns.ac.rs.scientificCenter.service.tasks.registration;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CheckReviewerCheckedTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(CheckReviewerCheckedTask.class);

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        if(delegateExecution.getVariable("Reviewer").toString().equals("true")){
            delegateExecution.setVariable("reviewerChecked", true);
        }else{
            delegateExecution.setVariable("reviewerChecked", false);
        }

        logger.info("Check reviewer finished!");

    }
}
