package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "editorial_board")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditorialBoard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "editorialBoard")
    @JsonIgnore
    private Magazine magazine;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "reviewer_magazine", joinColumns = @JoinColumn(name = "board_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "reviewer_id", referencedColumnName = "id"))
    private Collection<Reviewer> reviewers = new ArrayList<Reviewer>();

    @ManyToOne
    @JoinTable(name = "main_editors_board", joinColumns = @JoinColumn(name = "board_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "main_editor_id", referencedColumnName = "id"))
    private Editor main_editor;

    @ManyToMany
    @JoinTable(name = "editors_board", joinColumns = @JoinColumn(name = "board_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "editor_id", referencedColumnName = "id"))
    private Collection<Editor> editors = new ArrayList<>();
}
