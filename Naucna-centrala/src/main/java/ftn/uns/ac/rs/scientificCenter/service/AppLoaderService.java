package ftn.uns.ac.rs.scientificCenter.service;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.PaperSearchService;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.ReviewerIndexService;
import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.*;
import ftn.uns.ac.rs.scientificCenter.repository.*;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.identity.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class AppLoaderService implements ApplicationRunner {

	private static final Logger logger = LoggerFactory.getLogger(AppLoaderService.class);

	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private ScientificFieldRepository scientificFieldRepository;
	private ProcessService processService;
	private IdentityService identityService;
	private PaperRepository paperRepository;
	private PaperSearchService paperSearchService;
	private ReviewerRepository reviewerRepository;
	private ReviewerIndexService reviewerIndexService;

	public AppLoaderService(UserRepository adminRepository, RoleRepository roleRepository,
							ScientificFieldRepository scientificFieldRepository, ProcessService processService,
							IdentityService identityService, PaperRepository paperRepository,
							PaperSearchService paperSearchService, ReviewerIndexService reviewerIndexService,
							ReviewerRepository reviewerRepository) {

		this.userRepository = adminRepository;
		this.roleRepository = roleRepository;
		this.scientificFieldRepository = scientificFieldRepository;
		this.processService = processService;
		this.identityService = identityService;
		this.paperRepository = paperRepository;
		this.paperSearchService = paperSearchService;
		this.reviewerRepository = reviewerRepository;
		this.reviewerIndexService = reviewerIndexService;

	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<Paper> papers = paperRepository.findAll();
		for (Paper p:papers) {
			if(p.isAccepted()){
				paperSearchService.indexPaper(p);
			}
		}

		List<Reviewer> reviewers = reviewerRepository.findAll();
		for (Reviewer r:reviewers) {
			if(r.isVerified()){
				reviewerIndexService.indexReviewer(r);
			}
		}
		if (userRepository.findAll().size() == 0) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

			Role roleAdmin = this.roleRepository.findByName(UserRole.ADMIN);
			Role roleEditor = this.roleRepository.findByName(UserRole.EDITOR);
			Role roleReviewer = this.roleRepository.findByName(UserRole.REVIEWER);
			Role roleAuthor = this.roleRepository.findByName(UserRole.AUTHOR);

			ScientificField sc1 = this.scientificFieldRepository.findById(1L).get();
			ScientificField sc2 = this.scientificFieldRepository.findById(2L).get();
			ScientificField sc3 = this.scientificFieldRepository.findById(3L).get();

			User admin = new User();
			admin.setUsername("admin");
			admin.setPassword(encoder.encode("admin"));
			admin.setEmail("");
			admin.setFirst_name("");
			admin.setLast_name("");
			admin.setCity("");
			admin.setState("");
			admin.getRoles().add(roleAdmin);
			admin.setEnabled(true);

			userRepository.save(admin);
			logger.info("Admin added!");

			Editor editor = new Editor();
			editor.setUsername("editor");
			editor.setPassword(encoder.encode("editor"));
			editor.setEmail("editor@yopmail.com");
			editor.setFirst_name("Pavle");
			editor.setLast_name("Nikacevic");
			editor.setCity("Novi Sad");
			editor.setState("Serbia");
			editor.getRoles().add(roleEditor);
			editor.setEnabled(true);
			editor.getUser_fields().add(sc1);
			editor.getUser_fields().add(sc2);
			userRepository.save(editor);
			logger.info("Editor added!");

			Editor editor2 = new Editor();
			editor2.setUsername("editor2");
			editor2.setPassword(encoder.encode("editor2"));
			editor2.setEmail("editor2@yopmail.com");
			editor2.setFirst_name("Marija");
			editor2.setLast_name("Dimitrijevic");
			editor2.setCity("Novi Sad");
			editor2.setState("Serbia");
			editor2.getRoles().add(roleEditor);
			editor2.setEnabled(true);
			editor2.getUser_fields().add(sc2);
			editor2.getUser_fields().add(sc3);
			userRepository.save(editor2);
			logger.info("Editor2 added!");

			Editor editor3 = new Editor();
			editor3.setUsername("editor3");
			editor3.setPassword(encoder.encode("editor3"));
			editor3.setEmail("editor3@yopmail.com");
			editor3.setFirst_name("Marko");
			editor3.setLast_name("Markovic");
			editor3.setCity("Novi Sad");
			editor3.setState("Serbia");
			editor3.getRoles().add(roleEditor);
			editor3.setEnabled(true);
			editor3.getUser_fields().add(sc1);
			editor3.getUser_fields().add(sc3);
			userRepository.save(editor3);
			logger.info("Editor3 added!");

			Reviewer r = new Reviewer();
			r.setUsername("ana");
			r.setPassword(encoder.encode("sifra"));
			r.setEmail("reviewer1@yopmail.com");
			r.setFirst_name("Ana");
			r.setLast_name("Markov");
			r.setCity("Novi Sad");
			r.setState("Serbia");
			r.getRoles().add(roleReviewer);
			r.setEnabled(true);
			r.setVerified(true);
			r.getUser_fields().add(sc1);
			r.getUser_fields().add(sc3);
			userRepository.save(r);
			logger.info("Reviewer1 added!");

			Reviewer r2 = new Reviewer();
			r2.setUsername("igor");
			r2.setPassword(encoder.encode("sifra"));
			r2.setEmail("igor@yopmail.com");
			r2.setFirst_name("Igor");
			r2.setLast_name("Micic");
			r2.setCity("Novi Sad");
			r2.setState("Serbia");
			r2.getRoles().add(roleReviewer);
			r2.setEnabled(true);
			r2.setVerified(true);
			r2.getUser_fields().add(sc1);
			r2.getUser_fields().add(sc2);
			userRepository.save(r2);
			logger.info("Reviewer2 added!");

			Reviewer r3 = new Reviewer();
			r3.setUsername("pavle");
			r3.setPassword(encoder.encode("sifra"));
			r3.setEmail("pavle@yopmail.com");
			r3.setFirst_name("Igor");
			r3.setLast_name("Micic");
			r3.setCity("Novi Sad");
			r3.setState("Serbia");
			r3.getRoles().add(roleReviewer);
			r3.setEnabled(true);
			r3.setVerified(true);
			r3.getUser_fields().add(sc3);
			r3.getUser_fields().add(sc2);
			userRepository.save(r3);
			logger.info("Reviewer3 added!");

			Author a = new Author();
			a.setUsername("bojana");
			a.setPassword(encoder.encode("sifra"));
			a.setEmail("reviewer2@yopmail.com");
			a.setFirst_name("Bojana");
			a.setLast_name("Micic");
			a.setCity("Novi Sad");
			a.setState("Serbia");
			a.getRoles().add(roleAuthor);
			a.setEnabled(true);
			a.getUser_fields().add(sc1);
			a.getUser_fields().add(sc2);
			userRepository.save(a);
			logger.info("Author added!");

		}

		List<Group> groups = this.identityService.createGroupQuery().groupIdIn("editors","authors","reviewers").list();
		if(groups.isEmpty()){
			this.processService.makeGroups();
		}

	}
}
