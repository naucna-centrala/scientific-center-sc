package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.dto.ResponseMessage;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionRequestNCDTO;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionResponseDTO;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionStatusDto;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionViewDTO;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.ISubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/subscription")
@CrossOrigin("https://localhost:4200")
@Slf4j
public class SubscriptionController {

    @Autowired
    ISubscriptionService subscriptionService;

    @PostMapping(value = "/subscribe")
    public SubscriptionResponseDTO subscribe(@RequestBody SubscriptionRequestNCDTO subscriptionRequestNCDTO) throws Exception {

        SubscriptionResponseDTO transactionResponseDto = subscriptionService.subscribe(subscriptionRequestNCDTO);
        return transactionResponseDto;
    }

    @GetMapping(value = "/subscribe/success")
    public ResponseMessage successSubscription(@RequestParam String id) {

        return subscriptionService.successSubscription(id);

    }

    @GetMapping(value = "/subscribe/fail")
    public ResponseMessage failSubscription(@RequestParam String id) {

        return subscriptionService.failSubscription(id);
    }

    @GetMapping(value = "/get/all/{username}")
    public List<SubscriptionViewDTO> getAll(@PathVariable String username){
        return subscriptionService.findAllDone(username);
    }

    @GetMapping(value = "/unsubscribe/{kpId}")
    public ResponseMessage unsubscribe(@PathVariable String kpId){
        String message = subscriptionService.unsubscribe(kpId);
        return new ResponseMessage(message);
    }

    @GetMapping(value = "/check/subscribe/{username}")
    public ResponseMessage checkSubscribe(@PathVariable String username) {
        subscriptionService.checkSubscribe(username);

        log.info("Successfully subscriptions checked!");
        return new ResponseMessage("Successfully subscriptions checked!");
    }
    
    @GetMapping("/pending")
    public ResponseEntity<List<String>> getAllPendingIds() {
        log.info("Pending subscriptions!");
    	return ResponseEntity.ok(subscriptionService.pending());
    }
    
    @PostMapping("/update")
    public ResponseEntity<String> updateAll(@RequestBody List<SubscriptionStatusDto> list) {
    	subscriptionService.updateAll(list);
        log.info("Updated subscriptions!");
        return ResponseEntity.ok().build();
    }
}
