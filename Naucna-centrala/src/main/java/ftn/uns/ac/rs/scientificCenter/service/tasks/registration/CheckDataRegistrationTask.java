package ftn.uns.ac.rs.scientificCenter.service.tasks.registration;

import ftn.uns.ac.rs.scientificCenter.service.implementation.UserDetailsServiceImpl;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class CheckDataRegistrationTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(CheckDataRegistrationTask.class);

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        if(this.userDetailsService.findByUsername((String)delegateExecution.getVariable("Username")).equals(Optional.empty())
                && this.userDetailsService.findByEmail((String)delegateExecution.getVariable("Email")).equals(Optional.empty())) {
            delegateExecution.setVariable("isOK", true);
        }
        else {
            delegateExecution.setVariable("isOK", false);
        }

        logger.info("Check registration data finish.");
    }
}
