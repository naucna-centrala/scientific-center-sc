package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.model.Author;
import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    User save(Author author);
    User save(Editor editor);
    User save(Reviewer reviewer);
}
