package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.configuration.properties.UrlsConfig;
import ftn.uns.ac.rs.scientificCenter.dto.*;
import ftn.uns.ac.rs.scientificCenter.enumeration.SubscriptionStatus;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.Subscription;
import ftn.uns.ac.rs.scientificCenter.repository.SubscriptionRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.ISubscriptionService;
import ftn.uns.ac.rs.scientificCenter.utils.AES;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SubscriptionService implements ISubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    IMagazineService magazineService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UrlsConfig urlsConfig;

    @Override
    public SubscriptionResponseDTO subscribe(SubscriptionRequestNCDTO subscriptionRequestDTO) throws  Exception {

        Magazine magazine = magazineService.findById(subscriptionRequestDTO.getMagazine_id());
        if(magazine==null){
            logger.error("Magazine with id " + subscriptionRequestDTO.getMagazine_id() + " doesn't exist!");
            throw new Exception("Magazine with id " + subscriptionRequestDTO.getMagazine_id() + " doesn't exist!");
        }

        Subscription subscription = new Subscription();
        subscription.setBuyer(subscriptionRequestDTO.getUsername());
        subscription.setMagazine_id(subscriptionRequestDTO.getMagazine_id());
        subscription.setStatus(SubscriptionStatus.CREATED);
        String uuid = UUID.randomUUID().toString();
        subscription.setSubscriptionId(uuid);

        Subscription newSubscription = subscriptionRepository.save(subscription);

        SubscriptionRequestDTO sr = new SubscriptionRequestDTO();
        sr.setFailUrl(urlsConfig.getEndpoints().get("subscribe-fail"));
        sr.setKpSubscribeNC(AES.encrypt(newSubscription.getSubscriptionId()));
        sr.setSellerUuid(magazine.getUuid());
        sr.setSuccessUrl(urlsConfig.getEndpoints().get("subscribe-success"));
        sr.setUsername(newSubscription.getBuyer());
        sr.setNc("bojana");

        String url = urlsConfig.getEndpoints().get("subscription");

        ResponseEntity<SubscriptionResponseDTO> response = restTemplate.postForEntity(url, sr,
                SubscriptionResponseDTO.class);

        System.out.println(response.getBody().toString());



        return response.getBody();
    }

    @Override
    public ResponseMessage successSubscription(String id) {

        Subscription subscription = subscriptionRepository.findBySubscriptionId(AES.decrypt(id));
        subscription.setStatus(SubscriptionStatus.COMPLETED);
        subscriptionRepository.save(subscription);

        Magazine magazine = magazineService.findById(subscription.getMagazine_id());

        logger.info("User with username " + subscription.getBuyer() +" successfully subscribed to magazine " +
              magazine.getName());

        return new ResponseMessage("Sucessfully subscribe");
    }

    @Override
    public ResponseMessage failSubscription(String id) {

        Subscription subscription = subscriptionRepository.findBySubscriptionId(AES.decrypt(id));
        subscription.setStatus(SubscriptionStatus.FAILED);
        subscriptionRepository.save(subscription);

        Magazine magazine = magazineService.findById(subscription.getMagazine_id());

        logger.error("User with username " + subscription.getBuyer() +" failed to subscribe to magazine " +
                magazine.getName());

        return new ResponseMessage("Failed to subscribe");
    }

    @Override
    public Subscription save(Subscription subscription) {
        return subscriptionRepository.save(subscription);
    }

    @Override
    public List<Subscription> findAllUsername(String username) {
        List<Subscription> allSub = findAll();
        List<Subscription> usernameSub = new ArrayList<>();
        for (Subscription sub:allSub) {
            if(sub.getBuyer().equals(username)){
               usernameSub.add(sub);
            }
        }
        return usernameSub;
    }

    @Override
    public List<Subscription> findAll() {
        return subscriptionRepository.findAll();
    }

    @Override
    public List<SubscriptionViewDTO> findAllDone(String username) {
        List<Subscription> all = findAll();
        List<SubscriptionViewDTO> done = new ArrayList<>();

        for (Subscription s:all) {
            if(s.getStatus().equals(SubscriptionStatus.COMPLETED) && s.getBuyer().equals(username)){
                SubscriptionViewDTO sub = new SubscriptionViewDTO();
                sub.setSubscriptionId(s.getSubscriptionId());
                Magazine m = magazineService.findById(s.getMagazine_id());
                sub.setMagazine(m);
                done.add(sub);
            }

        }
        return done;
    }

    @Override
    public String unsubscribe(String kpId) {

        Subscription subscription = subscriptionRepository.findBySubscriptionId(kpId);
        subscription.setStatus(SubscriptionStatus.CANCEL);
        subscriptionRepository.save(subscription);

        String url = urlsConfig.getEndpoints().get("unsubscribe") + kpId;

        ResponseMessage response = restTemplate.getForEntity(url,ResponseMessage.class).getBody();

        log.info("Subscription " + kpId + " cancel!");

        return response.getMessage();
    }

    @Override
    public void checkSubscribe(String username) {
        List<Subscription> all = findAllUsername(username);
        List<String> kpId = new ArrayList<>();

        for (Subscription s:all) {
           kpId.add(s.getSubscriptionId());
        }

        String[] strings = kpId.stream().toArray(String[]::new);
        String url = urlsConfig.getEndpoints().get("check-subscription");

        String [] finalList = restTemplate.postForEntity(url,strings,String[].class).getBody();

        for (String id:finalList) {
            Subscription sub = getById(id);
            sub.setStatus(SubscriptionStatus.CANCEL);
            subscriptionRepository.save(sub);

            log.info("Subscription with id " + sub.getId() + " canceled!");
        }

    }

    @Override
    public Subscription getById(String id) {
        return subscriptionRepository.findBySubscriptionId(id);
    }
    
    @Override
    public List<String> pending() {
    	return subscriptionRepository.findByStatus(SubscriptionStatus.CREATED)
    			.stream()
    			.map(Subscription::getSubscriptionId)
    			.collect(Collectors.toList());
    }
    
    @Override
    public void updateAll(List<SubscriptionStatusDto> list) {
    	List<Subscription> subs = new ArrayList<>();
    	Subscription sub;
    	for(SubscriptionStatusDto status: list) {
    		sub = subscriptionRepository.findBySubscriptionId(status.getSubscriptionId());
    		if(status.getStatus() != sub.getStatus()) {
    			sub.setStatus(status.getStatus());
    			subs.add(sub);
    		}
    	}
    	subscriptionRepository.saveAll(subs);
    }
}
