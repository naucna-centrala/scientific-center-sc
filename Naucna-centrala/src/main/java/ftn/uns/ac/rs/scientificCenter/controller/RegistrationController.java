package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.model.ConfirmationToken;
import ftn.uns.ac.rs.scientificCenter.model.User;
import ftn.uns.ac.rs.scientificCenter.repository.ConfirmationTokenRepository;
import ftn.uns.ac.rs.scientificCenter.service.implementation.UserDetailsServiceImpl;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import org.camunda.bpm.engine.RuntimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.Optional;

@RestController
@RequestMapping(value = "/register")
@CrossOrigin("https://localhost:4200")
public class RegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    ProcessService processService;

    @Autowired
    RuntimeService runtimeService;

    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView,@RequestParam("token")String confirmationToken,
                                           @RequestParam("process") String processId) throws InterruptedException {

        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token != null)
        {
            Optional<User> user = userDetailsService.findByEmail(token.getUser().getEmail());

            user.get().setEnabled(true);
            userDetailsService.save(user.get());
            modelAndView.setViewName("accountVerified");
            logger.info("Account with token " + confirmationToken +  "verified!");

             runtimeService.createMessageCorrelation("message")
                    .processInstanceId(processId)
                    .setVariables(null)
                    .correlate();
            logger.info("Message sent! ");

        }
        else
        {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("error");
            logger.error("Error while verifing account with token ", confirmationToken);

        }

        return modelAndView;
    }

}
