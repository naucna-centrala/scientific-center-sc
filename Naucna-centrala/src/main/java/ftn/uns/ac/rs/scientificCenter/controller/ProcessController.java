package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.dto.*;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/process")
@CrossOrigin("https://localhost:4200")
public class ProcessController {

    private static final Logger logger = LoggerFactory.getLogger(ProcessController.class);

    @Autowired
    ProcessService processService;

    @Autowired
    RuntimeService runtimeService;

    @GetMapping(path = "/get/{process_name}/{username}", produces = "application/json")
    public @ResponseBody FormFieldsDTO get(@PathVariable String process_name, @PathVariable String username){

        Map<String, VariableValueDto> map = new HashMap<>();
        VariableValueDto v = new VariableValueDto();
        v.setValue(username);
        v.setType("string");
        map.put("process_username",v);
        String processId = processService.startProcess(process_name, map);
        List<TaskDto> processTasks;
        if(username.equals("!!NO")){
            processTasks = processService.getTasks(processId, null);
        }else{
            processTasks = processService.getTasks(processId, username);

        }
        TaskDto task = processTasks.stream().findFirst().get();
        FormFieldsDTO taskFormData = processService.getTaskFormData(task.getId(),processId);

        logger.info("Start process registration and get first form!");

        return taskFormData;
    }

    @GetMapping(path = "/get/next/form/{processId}/{username}", produces = "application/json")
    public @ResponseBody FormFieldsDTO getNextForm(@PathVariable String processId, @PathVariable String username) {

        List<TaskDto> processTasks = processService.getTasks(processId, username);

        if(processTasks.size()==0){
            return null;
        }
        TaskDto task = processTasks.get(0);

        FormFieldsDTO taskFormData = processService.getTaskFormData(task.getId(),processId);

        logger.info("Get next form for process:" + processId);

        return taskFormData;
    }

    @GetMapping(path = "/get/form/{processId}/{username}", produces = "application/json")
    public ResponseEntity  getAllTasks(@PathVariable String processId, @PathVariable String username) {

        List<FormFieldsDTO> allTasks = new ArrayList<>();
        final List<TaskDto> tasks = processService.getTasks(processId, username);
        if (tasks.size() > 0) {
            for (TaskDto task:tasks) {
                FormFieldsDTO taskFormData = processService.getTaskFormData(task.getId(),task.getProcessInstanceId());
                allTasks.add(taskFormData);
            }

            logger.info("Return all tasks for user: " + username + " and process:" + processId);

            return new ResponseEntity<>(allTasks, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.OK);

    }

    @GetMapping("/task/{username}/{taskName}")
    public ResponseEntity getUserTasks(@PathVariable String username, @PathVariable String taskName) {

        logger.info("Looking for tasks for username:" + username);

        List<FormFieldsDTO> allTasks = new ArrayList<>();
        final List<TaskDto> tasks = processService.getTasks(null, username);
        if (tasks.size() > 0) {
            for (TaskDto task:tasks) {
                if(task.getName().equals(taskName)){
                    FormFieldsDTO taskFormData = processService.getTaskFormData(task.getId(),task.getProcessInstanceId());
                    allTasks.add(taskFormData);
                }

            }

            logger.info("Return all tasks for user:" + username);

            return new ResponseEntity<>(allTasks, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.OK);

    }

    @PostMapping(path = "/post/{taskId}", produces = "application/json")
    public @ResponseBody ResponseEntity postForm(@RequestBody List<FormDataSubmitDTO> dto, @PathVariable String taskId) {

        processService.processTaskForm(taskId,dto);
        logger.info("Submit form!");

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PostMapping(path = "/post/object/{taskId}", produces = "application/json")
    public @ResponseBody ResponseEntity postFormObject(@RequestBody List<FormDataSubmitObjectDTO> dto, @PathVariable String taskId) {

        processService.processTaskFormObject(taskId,dto);
        logger.info("Submit form object!");

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PostMapping(path = "/post/mix/{taskId}", produces = "application/json")
    public @ResponseBody ResponseEntity postFormObject(@RequestBody FormMixDataSubmitDTO dto, @PathVariable String taskId) {

        processService.processMixDataForm(taskId,dto);
        logger.info("Submit form object!");

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PostMapping(path = "/change/variables/{processId}/{taskId}", produces = "application/json")
    public @ResponseBody ResponseEntity changeVariables(@RequestBody List<FormDataSubmitDTO> dto, @PathVariable String processId,
                                                        @PathVariable String taskId) {

        for (FormDataSubmitDTO data:dto) {
            processService.setVariable(processId,data.getName(),data.getValue());
        }
        List<FormDataDTO> data = new ArrayList<>();
        processService.submitTaskForm(taskId,data);

        logger.info("Changed all variables and submit task!");

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @GetMapping(value = "/send/message/{processId}")
    public @ResponseBody ResponseMessage sendMessage(@PathVariable String processId){

        runtimeService.createMessageCorrelation("pay_for_paper")
                .processInstanceId(processId)
                .setVariables(null)
                .correlate();
        logger.info("Message sent!");

        return new ResponseMessage("Message sent!");
    }

}
