package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionRequestDTO {

    private String sellerUuid;
    private String successUrl;
    private String failUrl;
    private String kpSubscribeNC;
    private String username;
    private double value;
    private String nc;
}
