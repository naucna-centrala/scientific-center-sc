package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.dto.ScientificFieldDTO;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import java.util.List;
import java.util.Optional;

public interface IScientificFieldService {

    ScientificField save(ScientificFieldDTO fieldDTO);
    List<ScientificField> findAll();
    Optional<ScientificField> findById(Long id);
}
