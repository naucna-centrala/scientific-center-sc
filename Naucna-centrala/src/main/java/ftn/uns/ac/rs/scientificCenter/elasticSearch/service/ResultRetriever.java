package ftn.uns.ac.rs.scientificCenter.elasticSearch.service;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultReviewerDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.AuthorIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ReviewerIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ScientificFieldIndex;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ResultRetriever {

    @Autowired
    ElasticsearchOperations elasticsearchTemplate;


    public List<FindResultDTO> getResultsWithHighlight(org.elasticsearch.index.query.QueryBuilder query) {

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .withHighlightFields(new HighlightBuilder.Field("title"),new HighlightBuilder.Field("magazine_name")
                        ,new HighlightBuilder.Field("text"),new HighlightBuilder.Field("keywords"),
                        new HighlightBuilder.Field("abstract_paper"))
                .build();

        Page<PaperIndex> booksEntities = elasticsearchTemplate.queryForPage(searchQuery, PaperIndex.class, new SearchResultMapper() {

            @SuppressWarnings("unchecked")
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse arg0, Class<T> arg1, Pageable arg2) {

                List<PaperIndex> books = new ArrayList<>();
                for(SearchHit searchHit : arg0.getHits()){
                    if(arg0.getHits().getHits().length <= 0){
                        return null;
                    }

                    PaperIndex resultData = new PaperIndex();
                    resultData.setTitle((String)searchHit.getSourceAsMap().get("title"));
                    resultData.setText((String)searchHit.getSourceAsMap().get("text"));
                    resultData.setMagazine_name((String)searchHit.getSourceAsMap().get("magazine_name"));
                    resultData.setKeywords((String)searchHit.getSourceAsMap().get("keywords"));
                    resultData.setFilename((String)searchHit.getSourceAsMap().get("filename"));
                    resultData.setField((String)searchHit.getSourceAsMap().get("field"));
                    resultData.setAuthors((Collection<AuthorIndex>) searchHit.getSourceAsMap().get("authors"));
                    resultData.setAbstract_paper((String)searchHit.getSourceAsMap().get("abstract_paper"));
                    resultData.setAccess((String)searchHit.getSourceAsMap().get("access"));

                    if(searchHit.getHighlightFields() != null){
                        StringBuilder highlights = new StringBuilder("...");

                        if(searchHit.getHighlightFields().get("text") != null){
                            highlights = makeHighlighter(searchHit,"text");
                        }
                        if(searchHit.getHighlightFields().get("title") != null){
                            highlights = makeHighlighter(searchHit,"title");
                        }
                        if(searchHit.getHighlightFields().get("magazine_name") != null){
                            highlights = makeHighlighter(searchHit,"magazine_name");

                        }
                        if(searchHit.getHighlightFields().get("keywords") != null){
                            highlights = makeHighlighter(searchHit,"keywords");

                        }
                        if(searchHit.getHighlightFields().get("abstract_paper") != null){
                            highlights = makeHighlighter(searchHit,"abstract_paper");
                        }

                        resultData.setHightlight(highlights.toString());
                    }
                    books.add(resultData);
                }

                if(books.size() > 0){
                    return new AggregatedPageImpl<T>((List<T>) books);
                }

                return null;
            }

            @Override
            public <T> T mapSearchHit(SearchHit searchHit, Class<T> aClass) {
                return null;
            }
        });
        List<FindResultDTO> finalResult = new ArrayList<>();
        if(booksEntities != null){
            for (PaperIndex index : booksEntities) {
                FindResultDTO r = new FindResultDTO(index);
                finalResult.add(r);
            }
        }

        return finalResult;

    }

    public StringBuilder makeHighlighter(SearchHit searchHit,String field){
        StringBuilder highlights = new StringBuilder("...");

        Text [] text = searchHit.getHighlightFields().get(field).fragments();
        for (Text t : text) {
            highlights.append(t.toString());
            highlights.append("...");
        }
        return highlights;
    }

    public List<FindResultReviewerDTO> getResultsReviewer(org.elasticsearch.index.query.QueryBuilder query) {

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .build();

        Page<ReviewerIndex> booksEntities = elasticsearchTemplate.queryForPage(searchQuery, ReviewerIndex.class, new SearchResultMapper() {

            @SuppressWarnings("unchecked")
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse arg0, Class<T> arg1, Pageable arg2) {

                List<ReviewerIndex> books = new ArrayList<>();
                for(SearchHit searchHit : arg0.getHits()){
                    if(arg0.getHits().getHits().length <= 0){
                        return null;
                    }

                    ReviewerIndex resultData = new ReviewerIndex();
                    resultData.setUsername((String)searchHit.getSourceAsMap().get("username"));
                    resultData.setState((String)searchHit.getSourceAsMap().get("state"));
                    resultData.setLast_name((String)searchHit.getSourceAsMap().get("last_name"));
                    resultData.setId((((Number)searchHit.getSourceAsMap().get("id")).longValue()));
                    resultData.setFirst_name((String)searchHit.getSourceAsMap().get("first_name"));
                    resultData.setEmail((String)searchHit.getSourceAsMap().get("email"));
                    resultData.setCity((String)searchHit.getSourceAsMap().get("city"));
                    resultData.setFields((Collection<ScientificFieldIndex>) searchHit.getSourceAsMap().get("fields"));
                    resultData.setPapers((Collection<PaperIndex>)searchHit.getSourceAsMap().get("papers"));

                    books.add(resultData);
                }

                if(books.size() > 0){
                    return new AggregatedPageImpl<T>((List<T>) books);
                }

                return null;
            }

            @Override
            public <T> T mapSearchHit(SearchHit searchHit, Class<T> aClass) {
                return null;
            }
        });
        List<FindResultReviewerDTO> finalResult = new ArrayList<>();
        if(booksEntities != null){
            for (ReviewerIndex index : booksEntities) {
                FindResultReviewerDTO r = new FindResultReviewerDTO(index);
                finalResult.add(r);
            }
        }

        return finalResult;

    }

    public List<FindResultDTO> getResultsWithoutHighlight(org.elasticsearch.index.query.QueryBuilder query) {
        System.out.println("AAAAAAAAAAAAAA");
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .build();

        Page<PaperIndex> booksEntities = elasticsearchTemplate.queryForPage(searchQuery, PaperIndex.class, new SearchResultMapper() {

            @SuppressWarnings("unchecked")
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse arg0, Class<T> arg1, Pageable arg2) {

                List<PaperIndex> books = new ArrayList<>();
                for(SearchHit searchHit : arg0.getHits()){
                    if(arg0.getHits().getHits().length <= 0){
                        return null;
                    }

                    PaperIndex resultData = new PaperIndex();
                    resultData.setTitle((String)searchHit.getSourceAsMap().get("title"));
                    resultData.setText((String)searchHit.getSourceAsMap().get("text"));
                    resultData.setMagazine_name((String)searchHit.getSourceAsMap().get("magazine_name"));
                    resultData.setKeywords((String)searchHit.getSourceAsMap().get("keywords"));
                    resultData.setFilename((String)searchHit.getSourceAsMap().get("filename"));
                    resultData.setField((String)searchHit.getSourceAsMap().get("field"));
                    resultData.setAuthors((Collection<AuthorIndex>) searchHit.getSourceAsMap().get("authors"));
                    resultData.setAbstract_paper((String)searchHit.getSourceAsMap().get("abstract_paper"));
                    resultData.setAccess((String)searchHit.getSourceAsMap().get("access"));

                    books.add(resultData);
                }

                if(books.size() > 0){
                    return new AggregatedPageImpl<T>((List<T>) books);
                }

                return null;
            }

            @Override
            public <T> T mapSearchHit(SearchHit searchHit, Class<T> aClass) {
                return null;
            }
        });
        List<FindResultDTO> finalResult = new ArrayList<>();
        if(booksEntities != null){
            for (PaperIndex index : booksEntities) {
                FindResultDTO r = new FindResultDTO(index);
                finalResult.add(r);
            }
        }

        return finalResult;

    }

}
