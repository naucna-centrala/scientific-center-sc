package ftn.uns.ac.rs.scientificCenter.elasticSearch.repository;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.ReviewerIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewerIndexRepository extends ElasticsearchRepository<ReviewerIndex,String> {
}
