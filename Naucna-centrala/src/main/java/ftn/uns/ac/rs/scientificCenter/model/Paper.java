package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "paper")
@Data
@AllArgsConstructor
@NoArgsConstructor
//dodati GeoPoin location, dodati reviewers, i doi?
public class Paper implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinTable(name = "author_paper", joinColumns = @JoinColumn(name = "paper_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"))
    @JsonBackReference
    private Author author;

    @OneToMany(mappedBy = "paper")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Collection<Coauthor> coauthor = new ArrayList<>();

    @Column(name = "title")
    private String title;

    @Column(name = "keywords")
    private String keywords;

    @Column(name = "abstract_paper")
    private String abstract_paper;

    @Column(name = "filename")
    private String filename;

    @Column(name = "accepted")
    private boolean accepted;

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "paper_fields", joinColumns = @JoinColumn(name = "paper_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "scientific_field_id", referencedColumnName = "id"))
    private ScientificField paper_fields;

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "magazine_papers", joinColumns = @JoinColumn(name = "paper_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "magazine_id", referencedColumnName = "id"))
//    @JsonIgnore
    private Magazine magazine;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "reviewers_paper", joinColumns = @JoinColumn(name = "paper_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "reviewer_id", referencedColumnName = "id"))
    private Collection<Reviewer> reviewers_paper = new ArrayList<>();

}
