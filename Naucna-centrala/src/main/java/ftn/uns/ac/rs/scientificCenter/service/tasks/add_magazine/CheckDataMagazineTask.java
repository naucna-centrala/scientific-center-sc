package ftn.uns.ac.rs.scientificCenter.service.tasks.add_magazine;

import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckDataMagazineTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(CheckDataMagazineTask.class);

    @Autowired
    IMagazineService magazineService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        if(this.magazineService.findByISSNNumber((String)delegateExecution.getVariable("ISSN number"))!= null){
            delegateExecution.setVariable("OkMagazine", false);

        }else{
            delegateExecution.setVariable("OkMagazine", true);
        }
        logger.info("Check magazine data finish.");

    }
}
