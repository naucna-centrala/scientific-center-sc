package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.EditorialBoard;
import ftn.uns.ac.rs.scientificCenter.repository.EditorialBoardRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEditorialBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditorialBoardService implements IEditorialBoardService {

    @Autowired
    EditorialBoardRepository editorialBoardRepository;

    @Override
    public EditorialBoard save(EditorialBoard editorialBoard) {
        return editorialBoardRepository.save(editorialBoard);
    }

    @Override
    public EditorialBoard findById(Long id) {
        return editorialBoardRepository.findById(id).get();
    }
}
