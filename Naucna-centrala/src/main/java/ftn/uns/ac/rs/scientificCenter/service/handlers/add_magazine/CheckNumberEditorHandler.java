package ftn.uns.ac.rs.scientificCenter.service.handlers.add_magazine;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CheckNumberEditorHandler implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {

        if((Long)delegateTask.getVariable("Number of editors") == 0) {
            delegateTask.setVariable("addEditor", false);
        }
        else {
            delegateTask.setVariable("addEditor", true);
        }

        log.info("Checked number of editors");

    }
}
