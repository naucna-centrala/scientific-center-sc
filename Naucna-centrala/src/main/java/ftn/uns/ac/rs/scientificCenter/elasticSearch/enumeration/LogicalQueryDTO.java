package ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogicalQueryDTO {

    private String query;
}
