package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "magazine")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Magazine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "issn", nullable = false)
    private String issn;

    @Column(name = "payment_method")
    @Enumerated(EnumType.STRING)
    private PaymentMethod payment_method;

    @Column(name = "active")
    private boolean active;

    @OneToOne(cascade = CascadeType.ALL)
    private EditorialBoard editorialBoard;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "magazine_fields", joinColumns = @JoinColumn(name = "magazine_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "field_id", referencedColumnName = "id"))
    private Collection<ScientificField> magazine_fields = new ArrayList<>();

    @OneToMany(mappedBy = "magazine")
    @JsonIgnore
    private Collection<Paper> papers = new ArrayList<>();
}
