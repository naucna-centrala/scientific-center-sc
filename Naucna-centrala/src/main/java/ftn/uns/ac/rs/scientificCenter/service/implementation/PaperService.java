package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.Author;
import ftn.uns.ac.rs.scientificCenter.model.Coauthor;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.repository.AuthorRepository;
import ftn.uns.ac.rs.scientificCenter.repository.PaperRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IPaperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PaperService implements IPaperService {

    @Autowired
    PaperRepository paperRepository;

    @Autowired
    AuthorRepository authorRepository;

    @Override
    public Paper save(Paper paper){
        return paperRepository.save(paper);
    }

    @Override
    public Paper findById(Long id){
        return paperRepository.findById(id).get();
    }

    @Override
    public List<Paper> findAllUsername(String username){
        Author a = authorRepository.findByUsername(username);
        List<Paper> finalList = new ArrayList<>();
        for (Paper p:a.getPapers()) {
            if(p.isAccepted()){
                finalList.add(p);
            }
        }
        return finalList;
    }

    @Override
    public List<Paper> getAll() {
        List<Paper> all = paperRepository.findAll();
        List<Paper> finalList = new ArrayList<>();
        for (Paper p:all) {
           if(p.isAccepted()){
               p.getCoauthor().add(new Coauthor(p.getAuthor().getFirst_name(),p.getAuthor().getLast_name(),p.getAuthor().getCity()));
               finalList.add(p);
           }
        }
        return finalList;
    }

    @Override
    public Paper findByFilename(String filename){
        return paperRepository.findByFilename(filename);
    }
}
