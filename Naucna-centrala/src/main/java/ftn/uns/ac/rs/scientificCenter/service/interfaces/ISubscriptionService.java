package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.dto.ResponseMessage;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionRequestNCDTO;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionResponseDTO;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionStatusDto;
import ftn.uns.ac.rs.scientificCenter.dto.SubscriptionViewDTO;
import ftn.uns.ac.rs.scientificCenter.model.Subscription;
import java.util.List;

public interface ISubscriptionService {

    SubscriptionResponseDTO subscribe(SubscriptionRequestNCDTO subscriptionRequestDTO) throws Exception;
    ResponseMessage successSubscription(String id);
    ResponseMessage failSubscription(String id);
    Subscription save(Subscription subscription);
    List<Subscription> findAllUsername(String username);
    List<Subscription> findAll();
    List<SubscriptionViewDTO> findAllDone(String username);
    String unsubscribe(String kpId);
    void checkSubscribe(String username);
    Subscription getById(String id);
    List<String> pending();
    void updateAll(List<SubscriptionStatusDto> list);

}
