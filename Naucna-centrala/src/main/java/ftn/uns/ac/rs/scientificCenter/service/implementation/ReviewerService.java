package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.repository.ReviewerRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IConfirmationTokenService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IReviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewerService implements IReviewerService {

    @Autowired
    ReviewerRepository reviewerRepository;

    @Autowired
    IConfirmationTokenService confirmationTokenService;

    @Override
    public List<Reviewer> findAll() {
        List<Reviewer> reviewersVerified = new ArrayList<>();
        List<Reviewer> reviewers = reviewerRepository.findAll();
        if(reviewers.size()!=0){
            for (Reviewer r: reviewers) {
                if(r.isVerified()){
                    reviewersVerified.add(r);
                }
            }
        }

        return reviewersVerified;
    }

    @Override
    public Reviewer findById(Long id) {
        return reviewerRepository.findById(id).get();
    }

    @Override
    public Optional<Reviewer> findByUsername(String username) {
        return reviewerRepository.findByUsername(username);
    }

    @Override
    public Reviewer save(Reviewer reviewer) {
        return reviewerRepository.save(reviewer);
    }

    @Override
    public List<Reviewer> findWithSameField(List<Long> fields) {
        List<Reviewer> reviewerList = findAll();
        List<Reviewer> finalList = new ArrayList<>();
        int i =0;
        for (Reviewer e:reviewerList) {
            i =0;
            for (ScientificField fieldEditor:e.getUser_fields()) {
                for (Long id: fields) {
                    if(fieldEditor.getId().equals(id)){
                        if(i==0){
                            finalList.add(e);
                            i =1;
                        }
                    }
                }
            }

        }
        return finalList;
    }

    @Override
    public void delete(Reviewer reviewer){
        this.confirmationTokenService.delete(reviewer);
        this.reviewerRepository.delete(reviewer);
    }
}
