package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;

import java.util.List;

public interface IEditorService {

    List<Editor> findAll();
    Editor findByUsername(String username);
    Editor findById(Long id);
    List<Editor> findWithSameField(List<Long> fields);
}
