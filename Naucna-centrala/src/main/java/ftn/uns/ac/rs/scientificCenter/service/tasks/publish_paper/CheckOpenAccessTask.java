package ftn.uns.ac.rs.scientificCenter.service.tasks.publish_paper;

import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class CheckOpenAccessTask implements JavaDelegate {

    @Autowired
    IMagazineService magazineService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Magazine m = null;

       List<Object> objects =  (List<Object>) delegateExecution.getVariable("magazine");
        for (Object object:objects) {
            String[] token = object.toString().substring(1,object.toString().length()-1).split(",");
            Long id = Long.parseLong(token[0].split("=")[1]);
            m = this.magazineService.findById(id);
        }

        if(m != null ){
            if(m.getPayment_method().equals(PaymentMethod.OPEN_ACCESS)){

                delegateExecution.setVariable("openAccess",true);
                log.info("Magazine is open-access! Proceed to payment!");

            }else{
                delegateExecution.setVariable("openAccess", false);
                log.info("Magazine is not open-access!");
            }
        }


    }
}
