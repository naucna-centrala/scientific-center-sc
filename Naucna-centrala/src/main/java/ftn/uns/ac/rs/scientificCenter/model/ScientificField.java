package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "scientific_fields")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScientificField implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToMany(mappedBy = "user_fields")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private Collection<User> user_fields = new ArrayList<>();

    @ManyToMany(mappedBy = "magazine_fields")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private Collection<Magazine> magazine_fields = new ArrayList<>();

    @OneToMany(mappedBy = "paper_fields")
    @JsonIgnore
    private Collection<Paper> paper_fields = new ArrayList<>();

}
