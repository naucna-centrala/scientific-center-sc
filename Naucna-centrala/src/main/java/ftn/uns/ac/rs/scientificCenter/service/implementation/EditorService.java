package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.repository.EditorRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEditorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class EditorService  implements IEditorService {

    @Autowired
    EditorRepository editorRepository;

    @Override
    public List<Editor> findAll() {
        return editorRepository.findAll();
    }

    @Override
    public Editor findByUsername(String username) {
        return editorRepository.findByUsername(username);
    }

    @Override
    public Editor findById(Long id) {
        return editorRepository.findById(id).get();
    }

    @Override
    public List<Editor> findWithSameField(List<Long> id_fields) {

        int i = 0;
        List<Editor> editorList = findAll();
        List<Editor> finalList = new ArrayList<>();
        for (Editor e:editorList) {
            i = 0;
            for (ScientificField fieldEditor:e.getUser_fields()) {
                for (Long id: id_fields) {
                    if(fieldEditor.getId().equals(id)){
                        if(i==0){
                            finalList.add(e);
                            i = 1;
                        }

                    }
                }
            }

        }
        return finalList;
    }
}
