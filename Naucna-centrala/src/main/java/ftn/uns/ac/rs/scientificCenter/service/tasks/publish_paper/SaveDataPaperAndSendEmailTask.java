package ftn.uns.ac.rs.scientificCenter.service.tasks.publish_paper;

import ftn.uns.ac.rs.scientificCenter.model.*;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.*;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class SaveDataPaperAndSendEmailTask implements JavaDelegate {

    @Autowired
    IMagazineService magazineService;

    @Autowired
    IAuthorService authorService;

    @Autowired
    IScientificFieldService scientificFieldService;

    @Autowired
    ICoauthorService coauthorService;

    @Autowired
    IPaperService paperService;

    @Autowired
    IEmailSenderService emailSenderService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Paper paper;
        if(delegateExecution.getVariable("paper_id") == null){
            paper = new Paper();
        }else{
            paper = paperService.findById((Long)delegateExecution.getVariable("paper_id"));
        }

        List<String> emails = new ArrayList<>();

        Object o = delegateExecution.getVariable("magazine");
        String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
        Long id = Long.parseLong(token[0].split("=")[1]);

        Magazine m = magazineService.findById(id);
        paper.setMagazine(m);
        emails.add(m.getEditorialBoard().getMain_editor().getEmail());

        paper.setAbstract_paper(delegateExecution.getVariable("Abstract").toString());
        paper.setAccepted(false);
        paper.setKeywords(delegateExecution.getVariable("Keywords").toString());
        paper.setTitle(delegateExecution.getVariable("Title").toString());

        Author author = authorService.findByUsername(delegateExecution.getVariable("username_author").toString());
        paper.setAuthor(author);
        emails.add(author.getEmail());

        Object objects = delegateExecution.getVariable("picked_field");
        String[] t = objects.toString().substring(1,objects.toString().length()-1).split(",");
        Long id_field = Long.parseLong(t[0].split("=")[1]);
        Optional<ScientificField> f = this.scientificFieldService.findById(id_field);
        paper.setPaper_fields(f.get());

        paper.setFilename(delegateExecution.getVariable("file_name").toString());

        List<Object> coauthors = (List<Object>) delegateExecution.getVariable("list_coauthors");
        List<Coauthor> coauthorList = new ArrayList<>();
        if(!coauthors.isEmpty()){
            for (Object c:coauthors) {
                Coauthor newCoauthor = new Coauthor();
                String[] tt = c.toString().substring(1,c.toString().length()-1).split(",");
                newCoauthor.setCity(tt[0].split("=")[1]);
                newCoauthor.setEmail(tt[1].split("=")[1]);
                newCoauthor.setFirst_name(tt[2].split("=")[1]);
                newCoauthor.setLast_name(tt[3].split("=")[1]);
                newCoauthor.setState(tt[4].split("=")[1]);

                newCoauthor = coauthorService.save(newCoauthor);
                coauthorList.add(newCoauthor);
            }
        }
        paper.setCoauthor(coauthorList);
        Paper p = paperService.save(paper);
        delegateExecution.setVariable("paper_id",p.getId());

        for (Coauthor c:coauthorList) {
            Coauthor findC = coauthorService.findById(c.getId());
            findC.setPaper(p);
            coauthorService.save(findC);
        }

        log.info("Paper successfully saved!");

        for (String email:emails) {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(email);
            mailMessage.setSubject("New paper added!");
            mailMessage.setFrom("test.upp.fax@gmail.com");
            mailMessage.setText("New paper is registered to sistem! Paper details:"+ System.lineSeparator() +
                    "Title: " + p.getTitle() + System.lineSeparator() + "Keywords: " +
                    p.getKeywords() + System.lineSeparator() + "Abstract: " + p.getAbstract_paper() + System.lineSeparator()+
                    "Author: " + p.getAuthor().getFirst_name() +" " + p.getAuthor().getLast_name() + System.lineSeparator() +
                    "Magazine: " + p.getMagazine().getName());

            this.emailSenderService.sendEmail(mailMessage);
            log.info("Email sent to " + email);

            delegateExecution.setVariable("main_editor_username", m.getEditorialBoard().getMain_editor().getUsername());
        }

    }
}
