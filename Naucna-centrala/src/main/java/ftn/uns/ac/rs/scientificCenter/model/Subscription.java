package ftn.uns.ac.rs.scientificCenter.model;

import ftn.uns.ac.rs.scientificCenter.enumeration.SubscriptionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "subscription")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subscription implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Long id;

    @Column(name = "subscription_id", nullable = true)
    private String subscriptionId; //encrypted

    @Column(name = "magazine_id", nullable = true)
    private Long magazine_id;

    @Column(name = "buyer", nullable = true)
    private String buyer; //username

    @Column(name = "status", nullable = true)
    private SubscriptionStatus status;
}
