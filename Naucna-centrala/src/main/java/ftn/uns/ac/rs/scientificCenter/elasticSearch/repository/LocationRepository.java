package ftn.uns.ac.rs.scientificCenter.elasticSearch.repository;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location,Long> {

    Location findByTitle(String title);
}
