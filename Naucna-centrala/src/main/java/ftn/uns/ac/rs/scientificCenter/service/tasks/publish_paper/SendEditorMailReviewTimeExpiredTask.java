package ftn.uns.ac.rs.scientificCenter.service.tasks.publish_paper;

import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEditorService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEmailSenderService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IPaperService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SendEditorMailReviewTimeExpiredTask implements JavaDelegate {

    @Autowired
    IEditorService editorService;

    @Autowired
    IEmailSenderService emailSenderService;

    @Autowired
    IPaperService paperService;


    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Editor editor = editorService.findByUsername(delegateExecution.getVariable("chosen_editor").toString());

        Paper p = paperService.findById((Long) delegateExecution.getVariable("paper_id"));

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(editor.getEmail());
        mailMessage.setSubject("Reviewing time expired!");
        mailMessage.setFrom("test.upp.fax@gmail.com");
        mailMessage.setText("Reviewing time expired! Please choose another reviewer! Paper info: "+ System.lineSeparator() +
                "Title: " + p.getTitle() + System.lineSeparator() + "Keywords: " +
                p.getKeywords() + System.lineSeparator() + "Abstract: " + p.getAbstract_paper() + System.lineSeparator()+
                "Author: " + p.getAuthor().getFirst_name() +" " + p.getAuthor().getLast_name() + System.lineSeparator() +
                "Magazine: " + p.getMagazine().getName());

        this.emailSenderService.sendEmail(mailMessage);

        log.info("Time expired for reviewing. Email sent to" + editor.getEmail());

        delegateExecution.setVariable("Number of reviewers for paper", 1);

    }
}
