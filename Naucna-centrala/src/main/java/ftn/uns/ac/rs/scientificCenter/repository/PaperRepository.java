package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.model.Paper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaperRepository extends JpaRepository<Paper,Long> {

    Paper findByFilename(String filename);
}
