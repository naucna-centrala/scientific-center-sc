package ftn.uns.ac.rs.scientificCenter.service.handlers.publish_paper;

import ftn.uns.ac.rs.scientificCenter.dto.MagazineInfoDTO;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class AddAllMagazinesHandler implements TaskListener {

    @Autowired
    IMagazineService magazineService;

    @Override
    public void notify(DelegateTask delegateTask) {

        MagazineInfoDTO magazine = new MagazineInfoDTO();
        delegateTask.getExecution().setVariable("magazine",magazine);

        List<MagazineInfoDTO> magazineList = magazineService.findAll();
        delegateTask.getExecution().setVariable("magazine_list",magazineList);

        log.info("Create magazines list");

    }
}
