package ftn.uns.ac.rs.scientificCenter.service.handlers.add_magazine;

import ftn.uns.ac.rs.scientificCenter.dto.ReviewerDTO;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IReviewerService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class AddReviewersHandler implements TaskListener {

    @Autowired
    IReviewerService reviewerService;

    @Override
    public void notify(DelegateTask delegateTask) {

        List<Reviewer> reviewerList = new ArrayList<>();
        delegateTask.getExecution().setVariable("reviewer_list",reviewerList);

        List<Object> objects = (List<Object>) delegateTask.getVariable("scientific_area_list");
        List<Long> fields = new ArrayList<>();
        for (Object o:objects) {
            String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
            Long id = Long.parseLong(token[0].split("=")[1]);
            fields.add(id);
        }

        List<Reviewer> reviewers = reviewerService.findWithSameField(fields);
        List<ReviewerDTO> allReviewers = new ArrayList<>();
        for (Reviewer r:reviewers) {
            allReviewers.add(new ReviewerDTO(r.getId(),r.getUsername()));
        }
        delegateTask.getExecution().setVariable("all_reviewers_list",allReviewers);

        log.info("Create reviewers list");


    }
}
