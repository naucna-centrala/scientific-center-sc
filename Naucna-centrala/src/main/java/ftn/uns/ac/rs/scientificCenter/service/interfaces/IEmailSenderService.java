package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import org.springframework.mail.SimpleMailMessage;

public interface IEmailSenderService {

    void sendEmail(SimpleMailMessage email);
}
