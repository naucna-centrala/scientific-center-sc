package ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration;

public enum  LogicalSign {

    AND,
    OR,
    NOT
}
