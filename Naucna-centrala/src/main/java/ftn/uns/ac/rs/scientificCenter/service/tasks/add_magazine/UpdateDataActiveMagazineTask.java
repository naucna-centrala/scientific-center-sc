package ftn.uns.ac.rs.scientificCenter.service.tasks.add_magazine;

import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateDataActiveMagazineTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(UpdateDataActiveMagazineTask.class);

    @Autowired
    IMagazineService magazineService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Magazine updateMagazine = magazineService.updateActive((String)delegateExecution.getVariable("ISSN number")
                ,(boolean)delegateExecution.getVariable("Active"));

        logger.info("Successfully magazine with issn number " + updateMagazine.getIssn() + " updated!");

    }
}
