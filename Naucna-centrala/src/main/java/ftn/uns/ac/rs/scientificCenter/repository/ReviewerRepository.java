package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer,Long> {

    Optional<Reviewer> findByUsername(String username);
    Optional<Reviewer> findById(Long id);
}
