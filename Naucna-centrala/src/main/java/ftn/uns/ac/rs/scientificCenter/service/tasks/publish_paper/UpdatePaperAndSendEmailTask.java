package ftn.uns.ac.rs.scientificCenter.service.tasks.publish_paper;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.IPaperSearchService;
import ftn.uns.ac.rs.scientificCenter.model.Author;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IAuthorService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEmailSenderService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IPaperService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UpdatePaperAndSendEmailTask implements JavaDelegate {

    @Autowired
    IPaperService paperService;

    @Autowired
    IEmailSenderService emailSenderService;

    @Autowired
    IAuthorService authorService;

    @Autowired
    IPaperSearchService paperSearchService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Author author = authorService.findByUsername(delegateExecution.getVariable("username_author").toString());

        Paper p = paperService.findById((Long) delegateExecution.getVariable("paper_id"));
        p.setAccepted(true);
        paperService.save(p);

        log.info("Paper " + p.getTitle() + " accepted!");

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(author.getEmail());
        mailMessage.setSubject("Paper accepted!");
        mailMessage.setFrom("test.upp.fax@gmail.com");
        mailMessage.setText("Your paper is accepted! Paper info: "+ System.lineSeparator() +
                "Title: " + p.getTitle() + System.lineSeparator() + "Keywords: " +
                p.getKeywords() + System.lineSeparator() + "Abstract: " + p.getAbstract_paper() + System.lineSeparator()+
                "Author: " + p.getAuthor().getFirst_name() +" " + p.getAuthor().getLast_name() + System.lineSeparator() +
                "Magazine: " + p.getMagazine().getName());

        this.emailSenderService.sendEmail(mailMessage);

        log.info("Email about accepting paper, sent to " + author.getEmail());

        paperSearchService.indexPaper(p);

        log.info("Paper " + p.getTitle() + " ready for search!");

    }

}
