package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.Role;

public interface IRoleService {

    Role findByName(UserRole name);
}
