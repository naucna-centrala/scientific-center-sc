package ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration;

public enum SearchReviewerType {

    REGULAR,
    MORE_LIKE_THIS,
    GEO
}
