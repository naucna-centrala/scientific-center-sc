package ftn.uns.ac.rs.scientificCenter.elasticSearch.service;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.Location;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.repository.LocationRepository;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.ILocationService;
import ftn.uns.ac.rs.scientificCenter.model.Coauthor;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.service.implementation.PaperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class LocationService implements ILocationService {

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    PaperService paperService;

    @Override
    public Location findByTitle(String title){
        return locationRepository.findByTitle(title);
    }

    @Override
    public List<Location> findLocationsAuthors(String filenamePaper){

        List<Location> allLocations= new ArrayList<>();
        Paper paper = paperService.findByFilename(filenamePaper);

        allLocations.add(findByTitle(paper.getAuthor().getCity()));

        for (Coauthor a:paper.getCoauthor()) {
            allLocations.add(findByTitle(a.getCity()));
        }

        return allLocations;

    }

}
