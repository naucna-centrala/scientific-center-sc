package ftn.uns.ac.rs.scientificCenter.service.handlers.registration;

import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IScientificFieldService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ScientificAreaHandler implements TaskListener {

	@Autowired
	IScientificFieldService scientificFieldService;

	public void notify(DelegateTask delegateTask) {

		log.info("Create scientific area list");

		List<ScientificField> scientificFieldList = new ArrayList<>();
		delegateTask.getExecution().setVariable("scientific_area_list",scientificFieldList);

		List<ScientificField> scientificFields = scientificFieldService.findAll();
		delegateTask.getExecution().setVariable("all_scientific_area",scientificFields);

	}
}