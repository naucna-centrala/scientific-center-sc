package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.enumeration.SubscriptionStatus;
import ftn.uns.ac.rs.scientificCenter.model.Subscription;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription,Long> {

    Subscription findBySubscriptionId(String subscriptionId);
    List<Subscription> findByStatus(SubscriptionStatus status);

}
