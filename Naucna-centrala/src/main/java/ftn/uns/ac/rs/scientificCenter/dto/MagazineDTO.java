package ftn.uns.ac.rs.scientificCenter.dto;

import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import ftn.uns.ac.rs.scientificCenter.model.EditorialBoard;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MagazineDTO {

    private String name;
    private String issn;
    private PaymentMethod payment_method;
    private EditorialBoard editorialBoard;
}
