package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MagazinePricesDTO {

    private String uuid;
    private double price;
}
