package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.configuration.properties.UrlsConfig;
import ftn.uns.ac.rs.scientificCenter.dto.MagazineDTO;
import ftn.uns.ac.rs.scientificCenter.dto.MagazineInfoDTO;
import ftn.uns.ac.rs.scientificCenter.dto.OrderDTO;
import ftn.uns.ac.rs.scientificCenter.dto.OrderResponseDTO;
import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import ftn.uns.ac.rs.scientificCenter.enumeration.SubscriptionStatus;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.model.Subscription;
import ftn.uns.ac.rs.scientificCenter.repository.MagazineRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.ISubscriptionService;
import ftn.uns.ac.rs.scientificCenter.utils.AES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class MagazineService implements IMagazineService {

    @Autowired
    private MagazineRepository magazineRepository;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ISubscriptionService subscriptionService;

    @Autowired
    UrlsConfig urlsConfig;

    @Override
    public Magazine save(MagazineDTO m, List<ScientificField> fields) {

        Magazine newMagazine = new Magazine();
        newMagazine.setActive(false);
        newMagazine.setEditorialBoard(m.getEditorialBoard());
        newMagazine.setIssn(m.getIssn());
        newMagazine.setName(m.getName());
        newMagazine.setPayment_method(m.getPayment_method());
        String uuid = UUID.randomUUID().toString();
        System.out.println("Uuid sellera je: " + uuid);
        newMagazine.setUuid(AES.encrypt(uuid));
        newMagazine.setMagazine_fields(fields);

        return magazineRepository.save(newMagazine);
    }

    @Override
    public OrderResponseDTO buyMagazine(OrderDTO order) throws Exception {
        Magazine magazine = magazineRepository.findByUuid(order.getSellerUuid());
        if(magazine == null){
            throw new Exception("The seller doesn't exist!");
        }
        String paymentUrl = urlsConfig.getEndpoints().get("buy-magazine");
        magazine.setUuid(AES.decrypt(magazine.getUuid()));
        ResponseEntity<OrderResponseDTO> response = restTemplate
                .postForEntity(paymentUrl, order, OrderResponseDTO.class);
        return response.getBody();
    }

    @Override
    public List<Magazine> getAll() {
        List<Magazine> allMagazines = magazineRepository.findAll();
        List<Magazine> magazines = new ArrayList<>();
        for (Magazine m:allMagazines) {
            if(m.isActive()){
                magazines.add(m);
            }
        }
        return magazines;
    }

    @Override
    public Magazine findByISSNNumber(String issn) {
        return magazineRepository.findByIssn(issn);
    }

    @Override
    public Magazine updateActive(String issn, boolean active) {

        Magazine m = magazineRepository.findByIssn(issn);
        if(m != null){
            m.setActive(active);
            Magazine newMagazine = magazineRepository.save(m);
            return newMagazine;

        }
        return null;
    }

    @Override
    public Magazine findById(Long id) {
        return magazineRepository.findById(id).get();
    }

    @Override
    public List<Magazine> findAllNotSubscribe(String username) {
        List<Subscription> allSubscriptions = subscriptionService.findAllUsername(username);
        List<Magazine> allMagazines = getAll();
        List<Magazine> finalList = new ArrayList<>();
        boolean i;
        for (Magazine m:allMagazines) {
            i = true;
            if(m.getPayment_method().equals(PaymentMethod.WITH_SUBSCRIPTION)){
                for (Subscription sub:allSubscriptions) {
                    if(sub.getMagazine_id() == m.getId() && sub.getStatus().equals(SubscriptionStatus.COMPLETED)){
                        i= false;
                    }
                }
                if(i){
                    finalList.add(m);
                }
            }

        }
        return finalList;
    }

    @Override
    public List<Magazine> findAllSubscribe(String username) {
        List<Subscription> allSubscriptions = subscriptionService.findAllUsername(username);
        List<Magazine> allMagazines = getAll();
        List<Magazine> finalList = new ArrayList<>();
        for (Subscription sub:allSubscriptions) {
            for (Magazine m:allMagazines) {
                if(sub.getMagazine_id() == m.getId() && sub.getStatus().equals(SubscriptionStatus.COMPLETED)){
                    finalList.add(m);
                }
            }
        }
        return finalList;
    }

    @Override
    public List<Magazine> findOpenAcess() {
        List<Magazine> all = getAll();
        List<Magazine>openAcess = new ArrayList<>();

        for (Magazine m:all) {
            if(m.getPayment_method().equals(PaymentMethod.OPEN_ACCESS)){
                openAcess.add(m);
            }
        }
        return openAcess;
    }

    @Override
    public List<MagazineInfoDTO> findAll() {
        List<Magazine> allMagazines = magazineRepository.findAll();
        List<MagazineInfoDTO> magazines = new ArrayList<>();
        for (Magazine m:allMagazines) {
            if(m.isActive()){
                magazines.add(new MagazineInfoDTO(m.getId(),m.getName(),m.getIssn(),m.getPayment_method(),m.getMagazine_fields(),m.getUuid()));
            }
        }
        return magazines;
    }
}
