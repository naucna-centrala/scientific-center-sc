package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormMixDataSubmitDTO {

    private List<FormDataSubmitDTO> data = new ArrayList<>();
    private List<FormDataSubmitObjectDTO> dataObject = new ArrayList<>();
}
