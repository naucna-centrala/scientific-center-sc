package ftn.uns.ac.rs.scientificCenter.enumeration;

public enum SubscriptionStatus {

    CREATED,
    COMPLETED,
    FAILED,
    CANCEL
}
