package ftn.uns.ac.rs.scientificCenter.elasticSearch.service;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.LogicalSign;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.SearchType;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.Location;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.MoreLikeThisQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.mustache.MultiSearchTemplateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Slf4j
@Component
public class QueryBuilder {

    private static int maxEdits = 1; //fuzzy broj izmena koji su dozvoljene , vrednosti: 0,1,2,auto

    public static int getMaxEdits(){
        return maxEdits;
    }

    public static org.elasticsearch.index.query.QueryBuilder buildQuery(SearchType queryType, String field,
                                                                        String value)throws IllegalArgumentException{
        if (field == null || field.equals("")) {
            log.error("Field not specified");
            throw new IllegalArgumentException("Field not specified");
        }

        if (value == null) {
            log.error("Value not specified");
            throw new IllegalArgumentException("Field not specified");
        }

        if (queryType == null) {
            log.error("Search type not specified");
            throw new IllegalArgumentException("Search type not specified");
        }

        org.elasticsearch.index.query.QueryBuilder retVal = null;

        if(field.equals("authors.first_name") || field.equals("authors.last_name")){
            retVal = findQueryAuthor(queryType,field,value);
        }else{
            retVal = findQuery(queryType,field,value);
        }

        if(field.equals("fields.title")){
            retVal = QueryBuilders.nestedQuery("fields", boolQuery().should(matchQuery(field, value)),
                    ScoreMode.Avg);
        }

        return retVal;

    }

    public static org.elasticsearch.index.query.QueryBuilder findQueryAuthor(SearchType searchType,String field,String value){
        org.elasticsearch.index.query.QueryBuilder retVal = null;
        switch (searchType){
            case REGULAR:
                retVal = QueryBuilders.nestedQuery("authors", boolQuery().should(matchQuery(field, value)),
                        ScoreMode.Avg);
                break;

            case FUZZY:
                retVal = QueryBuilders.fuzzyQuery(field, value).fuzziness(Fuzziness.fromEdits(getMaxEdits()));
                break;

            case PHRASE:
                retVal = QueryBuilders.nestedQuery("authors", boolQuery().should(matchPhraseQuery(field, value)),
                        ScoreMode.Avg);
                break;

            case PREFIX:
                retVal = QueryBuilders.prefixQuery(field, value);
                break;

            case RANGE:
                String[] values = value.split(" ");
                retVal = QueryBuilders.rangeQuery(field).from(values[0]).to(values[1]);
                break;

        }
        return retVal;

    }

    public static org.elasticsearch.index.query.QueryBuilder findQuery(SearchType searchType,String field,String value){
        org.elasticsearch.index.query.QueryBuilder retVal = null;
        switch (searchType){
            case REGULAR:
                retVal = QueryBuilders.termQuery(field, value);
                break;

            case FUZZY:
                retVal = QueryBuilders.fuzzyQuery(field, value).fuzziness(Fuzziness.fromEdits(getMaxEdits()));
                break;

            case PHRASE:
                retVal = QueryBuilders.matchPhraseQuery(field, value);
                break;

            case PREFIX:
                retVal = QueryBuilders.prefixQuery(field, value);
                break;

            case RANGE:
                String[] values = value.split(" ");
                retVal = QueryBuilders.rangeQuery(field).from(values[0]).to(values[1]);
                break;

        }
        return retVal;

    }

    public static org.elasticsearch.index.query.QueryBuilder parseLogicalQuery(String query){
        org.elasticsearch.index.query.QueryBuilder query1;
        org.elasticsearch.index.query.QueryBuilder query2;
        org.elasticsearch.index.query.QueryBuilder queryResult = null;

        String[] firstValue = null;
        String[] secondValue = null;
        LogicalSign sign = null;
        String[] tokens = query.split(" ");
        int size = tokens.length;
        int counter = 0;
        while(counter + 1 < size){
            if(counter == 0){
                firstValue = tokens[counter].split("=");
                query1 = QueryBuilder.buildQuery(SearchType.REGULAR, firstValue[0],firstValue[1]);

            }else{
                query1 = queryResult;
            }
            sign = LogicalSign.valueOf(tokens[counter + 1]);
            secondValue = tokens[counter + 2].split("=");
            query2 = QueryBuilder.buildQuery(SearchType.REGULAR, secondValue[0],secondValue[1]);
            queryResult = doLogicalSearch(query1,query2,sign);
            counter = counter + 2;

        }

        return queryResult;
    }

    public static org.elasticsearch.index.query.QueryBuilder doLogicalSearch(org.elasticsearch.index.query.QueryBuilder query1,
                                                                      org.elasticsearch.index.query.QueryBuilder query2, LogicalSign sign){

        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        switch (sign){
            case AND:
                builder.must(query1);
                builder.must(query2);
                break;

            case OR:
                builder.should(query1);
                builder.should(query2);
                break;

            case NOT:
                builder.must(query1);
                builder.mustNot(query2);
                break;
        }

        return builder;
    }

    public static org.elasticsearch.index.query.QueryBuilder moreLikeThisQueries(String text, String filename){

//        MoreLikeThisQueryBuilder.Item[] items = {new MoreLikeThisQueryBuilder.Item("paperindex", "_doc", filename)};
//
        String[] texts ={text};
//        org.elasticsearch.index.query.MoreLikeThisQueryBuilder query =
//                org.elasticsearch.index.query.QueryBuilders.moreLikeThisQuery(texts);
//

        BoolQueryBuilder builder = QueryBuilders.boolQuery().should(moreLikeThisQuery(new String[]{"text"},texts,null));
//        builder.must(query);
//        System.out.println(builder);

        return builder;
    }

    public static BoolQueryBuilder geoSearch(List<Location> locations){

        GeoDistanceQueryBuilder geoDistanceFilterBuilder = new GeoDistanceQueryBuilder("location")
                .distance("100", DistanceUnit.KILOMETERS)
                .geoDistance(GeoDistance.ARC);

        for (Location l:locations) {
            geoDistanceFilterBuilder.point(new GeoPoint(l.getLat()+ "," + l.getLon()));
        }

        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        builder.mustNot(geoDistanceFilterBuilder);

        return builder;
    }

}
