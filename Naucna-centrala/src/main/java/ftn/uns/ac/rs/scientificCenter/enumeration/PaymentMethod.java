package ftn.uns.ac.rs.scientificCenter.enumeration;

public enum PaymentMethod {

    OPEN_ACCESS,
    WITH_SUBSCRIPTION
}
