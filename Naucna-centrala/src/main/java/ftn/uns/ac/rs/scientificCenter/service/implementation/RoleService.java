package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.Role;
import ftn.uns.ac.rs.scientificCenter.repository.RoleRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService implements IRoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Role findByName(UserRole name) {
        return roleRepository.findByName(name);
    }
}
