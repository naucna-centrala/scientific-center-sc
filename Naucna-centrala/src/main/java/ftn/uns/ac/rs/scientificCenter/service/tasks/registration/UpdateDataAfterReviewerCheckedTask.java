package ftn.uns.ac.rs.scientificCenter.service.tasks.registration;

import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.Author;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.model.Role;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IAuthorService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IReviewerService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IRoleService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class UpdateDataAfterReviewerCheckedTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(UpdateDataAfterReviewerCheckedTask.class);

    @Autowired
    IReviewerService reviewerService;

    @Autowired
    IRoleService roleService;

    @Autowired
    IAuthorService authorService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Optional<Reviewer> user = this.reviewerService.findByUsername((String) delegateExecution.getVariable("Username"));
        if(user.get()!= null){

            if((Boolean) delegateExecution.getVariable("Confirm status")){
                user.get().setVerified((Boolean) delegateExecution.getVariable("Confirm status"));
                reviewerService.save(user.get());
                logger.info("Reviewer " + user.get().getUsername() + " verified!");
            }else{

                Author a = new Author();
                a.setState(user.get().getState());
                a.setCity(user.get().getCity());
                a.setEmail(user.get().getEmail());
                a.setFirst_name(user.get().getFirst_name());
                a.setLast_name(user.get().getLast_name());
                a.setPassword(user.get().getPassword());
                Role r = this.roleService.findByName(UserRole.AUTHOR);
                a.getRoles().add(r);
                a.setUsername(user.get().getUsername());
                a.setUser_fields(user.get().getUser_fields());
                a.setEnabled(user.get().isEnabled());

                this.reviewerService.delete(user.get());
                this.authorService.save(a);
                logger.info("Reviewer " + user.get().getUsername() + "not verified! Author created!");

            }


        }

    }
}
