package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.Paper;

import java.util.List;

public interface IPaperService {

    Paper save(Paper paper);
    Paper findById(Long id);
    List<Paper> findAllUsername(String username);
    List<Paper> getAll();
    Paper findByFilename(String filename);
}
