package ftn.uns.ac.rs.scientificCenter.service.handlers.publish_paper;

import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class AssigneePublishPaperHandler implements ExecutionListener {

    @Autowired
    ProcessService processService;

    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {

        IdentityService identityService = delegateExecution.getProcessEngine().getIdentityService();

        String username_login = delegateExecution.getVariable("process_username").toString();

        UserQuery e = identityService.createUserQuery().userId(username_login).memberOfGroup("authors");
        List<User> u = e.list();

        if(u.isEmpty()){
            delegateExecution.getProcessEngine().close();
            log.info("Process stopped! Role is not author!");

        }else{

            delegateExecution.setVariable("username_author",username_login);
            delegateExecution.setVariable("paper_id",null);
            log.info("Process task assignee to author with " + username_login + " username!");

        }

    }
}
