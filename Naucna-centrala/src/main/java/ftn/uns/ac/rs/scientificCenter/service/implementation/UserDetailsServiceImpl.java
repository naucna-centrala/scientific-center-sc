package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.model.Role;
import ftn.uns.ac.rs.scientificCenter.model.User;
import ftn.uns.ac.rs.scientificCenter.repository.UserRepository;
import ftn.uns.ac.rs.scientificCenter.security.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found -> username : " + username));

		return SecurityUser.build(user);
	}

	public Optional<User> findByEmail(String email){
		return userRepository.findByEmail(email);
	}

	public Optional<User> findById(Long id){
		return userRepository.findById(id);
	}

	public User save(User u){
		return userRepository.save(u);
	}

	public Collection<Role> checkLogin(String username){

		Optional<User> u = this.userRepository.findByUsername(username);

		return u.get().getRoles();
	}

	public Optional<User> findByUsername(String username){return userRepository.findByUsername(username);}



}
