package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.dto.ResponseMessage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IHandleFileService {

    String storeInAngular(MultipartFile file, String name);
    ResponseMessage storeFile(MultipartFile file);
    void deleteFile(String filename) throws IOException;
}
