package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Editor extends User implements Serializable {

    @Column(name = "title", nullable = true)
    private String title;

    @OneToMany(mappedBy = "main_editor")
    @JsonIgnore
    private Collection<EditorialBoard> main_editorial_board = new ArrayList<>();

    @ManyToMany(mappedBy = "editors")
    @JsonIgnore
    private Collection<EditorialBoard> editorial_board = new ArrayList<>();

}
