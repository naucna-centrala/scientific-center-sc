package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.dto.AuthenticationResponseDTO;
import ftn.uns.ac.rs.scientificCenter.dto.UserDTO;
import ftn.uns.ac.rs.scientificCenter.model.Role;
import ftn.uns.ac.rs.scientificCenter.model.User;
import ftn.uns.ac.rs.scientificCenter.repository.UserRepository;
import ftn.uns.ac.rs.scientificCenter.security.JwtProvider;
import ftn.uns.ac.rs.scientificCenter.security.SecurityUser;
import ftn.uns.ac.rs.scientificCenter.service.implementation.UserDetailsServiceImpl;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(value = "/user")
@CrossOrigin("https://localhost:4200")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserDetailsServiceImpl userService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    ProcessService processService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserDTO loginRequest) {

        Optional<User> u =this.userRepository.findByUsername(loginRequest.getUsername());
        if(u.get() != null && !(u.get().isEnabled())){
            logger.warn("Account not verify!");
            return new ResponseEntity<>("Not verify", HttpStatus.NOT_FOUND);
        }

        Authentication authentication = null;
        UsernamePasswordAuthenticationToken t = new UsernamePasswordAuthenticationToken(
               loginRequest.getUsername(), loginRequest.getPassword());
        try {
            authentication = this.authenticationManager.authenticate(t);
        } catch (AuthenticationException e) {
            logger.warn(String.format("Invalid login with username: %s",
                    loginRequest.getUsername()));
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);

        }
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-authentication so we can generate token
        UserDetails userDetails = this.userService.loadUserByUsername(loginRequest.getUsername());

        SecurityUser su = (SecurityUser) userDetails;
        String token = this.jwtProvider.generateToken(userDetails);
        // Return the token
        AuthenticationResponseDTO authResponse = new AuthenticationResponseDTO(token,"Bearer",userDetails.getUsername(),userDetails.getAuthorities());
        logger.info("Successfully login");

        Collection<Role> type = userService.checkLogin(su.getUsername());
        processService.insertUserIntoGroup(type,su);

        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }




}
