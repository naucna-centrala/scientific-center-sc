package ftn.uns.ac.rs.scientificCenter.service.handlers.add_magazine;

import ftn.uns.ac.rs.scientificCenter.dto.EditorDTO;
import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEditorService;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class AddEditorsHandler implements TaskListener {

    @Autowired
    IEditorService editorService;

    @Autowired
    ProcessService processService;

    @Override
    public void notify(DelegateTask delegateTask) {

        List<Editor> editorList = new ArrayList<>();
        delegateTask.getExecution().setVariable("editor_list",editorList);

        List<Object> objects = (List<Object>) delegateTask.getVariable("scientific_area_list");
        List<Long> fields = new ArrayList<>();
        for (Object o:objects) {
            String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
            Long id = Long.parseLong(token[0].split("=")[1]);
            fields.add(id);
        }

        List<Editor> editors = editorService.findWithSameField(fields);
        List<EditorDTO> allEditors = new ArrayList<>();
        for (Editor r:editors) {
            if(!r.getUsername().equals(delegateTask.getVariable("process_username").toString())){
                allEditors.add(new EditorDTO(r.getId(),r.getUsername()));

            }
        }
        delegateTask.getExecution().setVariable("all_editors_list",allEditors);

        log.info("Create editors list");

    }
}
