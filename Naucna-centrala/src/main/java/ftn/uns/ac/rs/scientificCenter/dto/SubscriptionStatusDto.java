package ftn.uns.ac.rs.scientificCenter.dto;

import ftn.uns.ac.rs.scientificCenter.enumeration.SubscriptionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionStatusDto {
	private String subscriptionId;
	private SubscriptionStatus status;
}
