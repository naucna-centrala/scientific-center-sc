package ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration;

public enum SearchType {

    REGULAR,
    FUZZY,
    PHRASE,
    RANGE,
    PREFIX
}
