package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.dto.ResponseMessage;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IHandleFileService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IPaperService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IReviewerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/paper")
@CrossOrigin(origins = "https://localhost:4200")
@Slf4j
public class PaperController {

    @Autowired
    IHandleFileService fileService;

    @Autowired
    IPaperService paperService;

    @Autowired
    IReviewerService reviewerService;

    @PostMapping("/savefile")
    public ResponseEntity<ResponseMessage> handleFileUpload(@RequestParam("file") MultipartFile file) {
        ResponseMessage message = new ResponseMessage();
        try{
            message = fileService.storeFile(file);
            return ResponseEntity.status(HttpStatus.OK).body(message);
        }catch (Exception e){
            message.setMessage("Failed to upload!");
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }


    @GetMapping(value = "/getfile/{filename}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody InputStreamResource getFile(@PathVariable String filename) throws IOException {
        ClassPathResource pdfFile = new ClassPathResource("files/" +filename + ".pdf");
        log.info("Document " + filename + " load");
        return new InputStreamResource(pdfFile.getInputStream());
    }

    @GetMapping(value = "/delete/{filename}")
    public void deleteDocument(@PathVariable String filename) throws IOException {
        fileService.deleteFile(filename);
        log.info("Document with name " + filename + " deleted");
    }

    @GetMapping(value = "/getPaper/{username}")
    public @ResponseBody List<Paper> getPapers(@PathVariable String username) throws IOException {

        List<Paper> papers = paperService.findAllUsername(username);

        log.info("Get all papers for user " + username);
        return papers;
    }

    @GetMapping(value="/download/{name}")
    public ResponseEntity<?> download(@PathVariable String name ) throws IOException{

        Path dirLocation = Paths.get("src/main/resources/files");
        Path path = dirLocation.resolve(name);

        if(new File(path.toString()).exists()) {

            byte[] content = Files.readAllBytes(path);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(content.length);
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

            log.info("Successfully downloaded file " + name);

            return new ResponseEntity<byte[]>(content,headers,HttpStatus.OK);

        }
        log.warn("File " + name + " not found!");

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/getAll")
    public List<Paper> getAll(){
        return paperService.getAll();
    }

    @GetMapping(value = "/all/reviewers")
    public List<Reviewer> getAllReviewers(){
        return reviewerService.findAll();
    }
}
