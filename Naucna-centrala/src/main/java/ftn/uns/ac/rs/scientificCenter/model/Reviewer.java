package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reviewer extends User implements Serializable {

    @Column(name = "title", nullable = true)
    private String title;

    @Column(name = "verified") // admin set this - list only verified
    private boolean verified;

    @ManyToMany(mappedBy = "reviewers")
    @JsonIgnore
    private Collection<EditorialBoard> magazines = new ArrayList<>();

    @ManyToMany(mappedBy = "reviewers_paper")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    private Collection<Paper> reviewers_paper = new ArrayList<>();

}
