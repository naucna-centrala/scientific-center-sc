package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormDataDTO {

    private String name;
    private VariableValueDto value;

}
