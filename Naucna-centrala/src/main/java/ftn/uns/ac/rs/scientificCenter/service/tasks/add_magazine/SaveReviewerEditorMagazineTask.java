package ftn.uns.ac.rs.scientificCenter.service.tasks.add_magazine;

import ftn.uns.ac.rs.scientificCenter.model.Editor;
import ftn.uns.ac.rs.scientificCenter.model.EditorialBoard;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEditorService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IEditorialBoardService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IReviewerService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class SaveReviewerEditorMagazineTask implements JavaDelegate {

    @Autowired
    IEditorService editorService;

    @Autowired
    IReviewerService reviewerService;

    @Autowired
    IMagazineService magazineService;

    @Autowired
    IEditorialBoardService editorialBoardService;

    private static final Logger logger = LoggerFactory.getLogger(SaveReviewerEditorMagazineTask.class);

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Magazine magazine = magazineService.findByISSNNumber((String)delegateExecution.getVariable("ISSN number"));
        if(magazine == null){
            logger.error("Error finding magazine with ISSN number:" + delegateExecution.getVariable("ISSN number"));
            return;
        }
        EditorialBoard editorialBoard = editorialBoardService.findById(magazine.getEditorialBoard().getId());
        if(editorialBoard == null){
            logger.error("Error finding editorial board with id :" + magazine.getEditorialBoard().getId());
            return;
        }

        if((Long)delegateExecution.getVariable("Number of editors") != 0) {
            List<Object> objects = (List<Object>) delegateExecution.getVariable("editor_list");
            for (Object o:objects) {
                String[] token = o.toString().substring(1,o.toString().length()-1).split(",");

                Long id = Long.parseLong(token[0].split("=")[1]);
                Editor f = this.editorService.findById(id);
                if(f == null){
                    logger.error("Error finding editor with id :" + id);
                    return;
                }
                editorialBoard.getEditors().add(f);
            }
        }else{
            delegateExecution.setVariable("editor_list", new ArrayList<>());
        }

        List<Object> objects = (List<Object>) delegateExecution.getVariable("reviewer_list");
        for (Object o:objects) {
            String[] token = o.toString().substring(1,o.toString().length()-1).split(",");
            Long id = Long.parseLong(token[0].split("=")[1]);
            Reviewer r = this.reviewerService.findById(id);
            if(r == null){
                logger.error("Error finding reviewer with id :" + id);
                return;
            }
            editorialBoard.getReviewers().add(r);
        }

        editorialBoardService.save(editorialBoard);
        logger.info("Reviewers and editors successfully added to the board!");

    }
}
