package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.enumeration.UserRole;
import ftn.uns.ac.rs.scientificCenter.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {

    Role findByName(UserRole author);
}
