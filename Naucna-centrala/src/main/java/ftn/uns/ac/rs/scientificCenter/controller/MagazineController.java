package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.configuration.properties.UrlsConfig;
import ftn.uns.ac.rs.scientificCenter.dto.*;
import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IMagazineService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.ISubscriptionService;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import org.camunda.bpm.engine.RuntimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/magazine")
@CrossOrigin("https://localhost:4200")
public class MagazineController {

    private static final Logger logger = LoggerFactory.getLogger(MagazineController.class);

    @Autowired
    ProcessService processService;

    @Autowired
    ISubscriptionService subscriptionService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    IMagazineService magazineService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UrlsConfig urlsConfig;

    @GetMapping(produces = "application/json")
    public @ResponseBody List<Magazine> getAllMagazines(){
        return magazineService.getAll();
    }

    @PostMapping("/buy")
    public OrderResponseDTO buyMagazine(@RequestBody OrderDTO order) throws Exception{
        return magazineService.buyMagazine(order);
    }

    @GetMapping(value = "/get/{issn}", produces = "application/json")
    public @ResponseBody ResponseMessage getOne(@PathVariable String issn) throws UnsupportedEncodingException {

        Magazine magazine = magazineService.findByISSNNumber(issn);
        ResponseMessage message = new ResponseMessage();
        message.setMessage(magazine.getUuid());

        logger.info("Successfully magazine found with issn number:" + issn);
        return message;
    }

    @GetMapping(value = "/payment/added")
    public @ResponseBody SubscriptionResponseDTO continueProcessAddMagazine(@RequestParam("processId") String processId) throws Exception {

        runtimeService.createMessageCorrelation("message_payment_method")
                .processInstanceId(processId)
                .setVariables(null)
                .correlate();
        logger.info("Message sent!");
        logger.info("Process with id " + processId + " continue!");

        return null;
    }

    @GetMapping(value = "/get/subscribe/{username}", produces = "application/json")
    public @ResponseBody List<Magazine> getAllSubscribe(@PathVariable String username) throws UnsupportedEncodingException {

        List<Magazine> allMagazines = magazineService.findAllSubscribe(username);

        logger.info("Find all magazines with subscription to username:" + username);
        return allMagazines;
    }

    @GetMapping(value = "/get/not/subscribe/{username}", produces = "application/json")
    public @ResponseBody List<Magazine> getNotSubscribe(@PathVariable String username) throws UnsupportedEncodingException {

        List<Magazine> allMagazines = magazineService.findAllNotSubscribe(username);

        logger.info("Find all magazines with no subscription to username:" + username);
        return allMagazines;
    }

    @PostMapping(value="/get/prices", produces = "application/json")
    public @ResponseBody List<MagazinePricesDTO> getPrices(@RequestBody List<Magazine> magazines){

        List<String> listUuid = new ArrayList<>();
        for (Magazine m:magazines) {
            listUuid.add(m.getUuid());
        }

        String getPricesUrl = urlsConfig.getEndpoints().get("seller-prices");
        ResponseEntity<ListPricesDTO> response = restTemplate
                .postForEntity(getPricesUrl, listUuid, ListPricesDTO.class);
        return response.getBody().getListPrices();
    }

    @GetMapping(value = "/get/free", produces = "application/json")
    public @ResponseBody List<Magazine> getOpenAcess(){

        List<Magazine> all = magazineService.findOpenAcess();
        logger.info("Return open acess magazines!");

        return all;
    }


}
