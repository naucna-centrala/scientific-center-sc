package ftn.uns.ac.rs.scientificCenter.elasticSearch.dto;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.SearchReviewerType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchReviewerDTO {

    private SearchReviewerType searchType;
    private String field;
    private String value;
}
