package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Author extends User implements Serializable {

    @OneToMany(mappedBy = "author")
    @JsonManagedReference
    private Collection<Paper> papers = new ArrayList<>();

}
