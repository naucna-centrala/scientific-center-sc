package ftn.uns.ac.rs.scientificCenter.elasticSearch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.search.aggregations.support.ValuesSource;
import org.springframework.data.elasticsearch.annotations.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "reviewerindex")
@Mapping(mappingPath = "reviewerMappings.json")
public class ReviewerIndex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private String email;
    private String first_name;
    private String last_name;
    private String city;
    private String state;

    @Field(type = FieldType.Nested, includeInParent = true)
    private Collection<ScientificFieldIndex> fields = new ArrayList<>();

    @Field(type = FieldType.Nested, includeInParent = true)
    private Collection<PaperIndex> papers = new ArrayList<>();

    @GeoPointField
    private GeoPoint location;

}
