package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.Reviewer;
import java.util.List;
import java.util.Optional;

public interface IReviewerService {

    List<Reviewer> findAll();
    Reviewer findById(Long id);
    Optional<Reviewer> findByUsername(String username);
    Reviewer save(Reviewer reviewer);
    List<Reviewer> findWithSameField(List<Long> fields);
    void delete(Reviewer reviewer);
}
