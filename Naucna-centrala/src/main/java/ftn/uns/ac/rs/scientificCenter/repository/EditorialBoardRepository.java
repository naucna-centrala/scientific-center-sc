package ftn.uns.ac.rs.scientificCenter.repository;

import ftn.uns.ac.rs.scientificCenter.model.EditorialBoard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface EditorialBoardRepository extends JpaRepository<EditorialBoard,Long> {

    Optional<EditorialBoard> findById(Long id);
}
