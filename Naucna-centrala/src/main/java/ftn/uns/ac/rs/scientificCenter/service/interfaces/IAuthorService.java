package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.Author;

public interface IAuthorService {

    Author save(Author author);
    Author findByUsername(String username);
}
