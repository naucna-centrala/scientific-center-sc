package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponseDTO {
    private String transactionId;
    private List<String> paymentMethods;
    private String uuid;
    private double amount;
}
