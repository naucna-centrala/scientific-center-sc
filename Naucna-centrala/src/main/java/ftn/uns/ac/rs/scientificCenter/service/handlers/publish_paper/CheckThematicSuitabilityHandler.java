package ftn.uns.ac.rs.scientificCenter.service.handlers.publish_paper;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CheckThematicSuitabilityHandler implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {

        if((boolean)delegateTask.getVariable("Check_thematic")){
            delegateTask.setVariable("accepted_thematic",true);
        }else{
            delegateTask.setVariable("accepted_thematic",false);
        }

        log.info("Check thematic suitability done");
    }
}
