package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewInfoPaperDTO implements Serializable {

    private String first_name;
    private String last_name;
    private String recommendation;
    private String comment_editor;
    private String comment_author;
}
