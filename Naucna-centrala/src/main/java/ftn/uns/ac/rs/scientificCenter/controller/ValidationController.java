package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.dto.ValidationDTO;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/validate")
@CrossOrigin("https://localhost:4200")
public class ValidationController {

    private static final Logger logger = LoggerFactory.getLogger(ValidationController.class);

    @Autowired
    IValidationService validationService;

    @GetMapping(path = "/check/fields/{number}", produces = "application/json")
    public @ResponseBody ValidationDTO checkScientificFields(@PathVariable int number){

        ValidationDTO validationDTO = validationService.checkNumberScientificFields(number);
        logger.info("Validation scientific fields done!");

        return validationDTO;
    }

    @GetMapping(path = "/check/reviewer/{number}/{processId}", produces = "application/json")
    public @ResponseBody ValidationDTO checkReviewers(@PathVariable int number, @PathVariable String processId){

        ValidationDTO validationDTO = validationService.checkNumberReviewer(number,processId);
        logger.info("Validation reviewers done!");

        return validationDTO;
    }

    @GetMapping(path = "/check/editor/{number}/{processId}", produces = "application/json")
    public @ResponseBody ValidationDTO checkEditors(@PathVariable int number, @PathVariable String processId){

        ValidationDTO validationDTO = validationService.checkNumberEditor(number,processId);
        logger.info("Validation editors done!");

        return validationDTO;
    }

}
