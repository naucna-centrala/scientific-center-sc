package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.dto.ScientificFieldDTO;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import ftn.uns.ac.rs.scientificCenter.repository.ScientificFieldRepository;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IScientificFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ScientificFieldService implements IScientificFieldService {

    @Autowired
    ScientificFieldRepository scientificFieldRepository;

    @Override
    public ScientificField save(ScientificFieldDTO fieldDTO) {
        ScientificField field = new ScientificField();
        field.setTitle(fieldDTO.getTitle());
        return scientificFieldRepository.save(field);
    }

    @Override
    public List<ScientificField> findAll() {
        return scientificFieldRepository.findAll();
    }

    @Override
    public Optional<ScientificField> findById(Long id) {
        return scientificFieldRepository.findById(id);
    }
}
