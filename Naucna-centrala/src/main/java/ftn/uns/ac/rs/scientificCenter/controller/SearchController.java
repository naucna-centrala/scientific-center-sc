package ftn.uns.ac.rs.scientificCenter.controller;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.FindResultReviewerDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.SearchDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.dto.SearchReviewerDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.LogicalQueryDTO;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.SearchReviewerType;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.SearchType;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.Location;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.ILocationService;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.IPaperSearchService;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces.IReviewerIndexService;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.ParseQuery;
import ftn.uns.ac.rs.scientificCenter.elasticSearch.service.ResultRetriever;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.MoreLikeThisQuery;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/search")
@CrossOrigin(origins = "https://localhost:4200")
@Slf4j
public class SearchController {

    @Autowired
    ResultRetriever resultRetriever;

    @Autowired
    IReviewerIndexService reviewerIndexService;

    @Autowired
    IPaperSearchService paperSearchService;

    @Autowired
    ILocationService locationService;

    @Autowired
    ElasticsearchOperations elasticsearchOperations;

    @PostMapping(value = "/query")
    public List<FindResultDTO> searchQuery(@RequestBody SearchDTO searchDTO) throws IOException {

        String value = ParseQuery.parseQuery(searchDTO.getValue());

        QueryBuilder query = ftn.uns.ac.rs.scientificCenter.elasticSearch.service.QueryBuilder.buildQuery
                (searchDTO.getSearchType(),searchDTO.getField(),value);

        List<FindResultDTO> result = resultRetriever.getResultsWithHighlight(query);
        return result;
    }

    @PostMapping(value = "/query/logical")
    public List<FindResultDTO> logicalQuery(@RequestBody LogicalQueryDTO logicalQuery) throws IOException {

        QueryBuilder query = ftn.uns.ac.rs.scientificCenter.elasticSearch.service.QueryBuilder.parseLogicalQuery(logicalQuery.getQuery());

        if(query == null){
            log.error("The query is spelled wrong!");
            return null;
        }

        List<FindResultDTO> result = resultRetriever.getResultsWithHighlight(query);
        return result;
    }

    @PostMapping(value = "/reviewer")
    public List<FindResultReviewerDTO> searchQueryReviewer(@RequestBody SearchReviewerDTO searchDTO) throws IOException {

        List<FindResultReviewerDTO> result = null;
        String value = ParseQuery.parseQuery(searchDTO.getValue());

        if(searchDTO.getSearchType().equals(SearchReviewerType.REGULAR)){
            QueryBuilder query = ftn.uns.ac.rs.scientificCenter.elasticSearch.service.QueryBuilder.buildQuery
                    (SearchType.REGULAR,searchDTO.getField(),value);
            result = resultRetriever.getResultsReviewer(query);

            log.info("Finish searching reviewers.Search type: scientific field, Found:" + result.size());

        }else if(searchDTO.getSearchType().equals(SearchReviewerType.MORE_LIKE_THIS)){

            String text = paperSearchService.getPaperText(value);
            if(text == null){
                log.error("Paper with filename " + value + " doesn't exist!");
                return null;
            }

//            QueryBuilder query = ftn.uns.ac.rs.scientificCenter.elasticSearch.service.QueryBuilder.moreLikeThisQueries(text, value);
//            List<FindResultDTO> resultPaper = resultRetriever.getResultsWithoutHighlight(query);
//            result = reviewerIndexService.findReviewerPaper(resultPaper);

            MoreLikeThisQuery moreLikeThisQuery = new MoreLikeThisQuery();
            moreLikeThisQuery.addFields("text");
            moreLikeThisQuery.setId(value);
            moreLikeThisQuery.setMinDocFreq(1);

            Page<PaperIndex> sampleEntities = elasticsearchOperations.moreLikeThis(moreLikeThisQuery, PaperIndex.class);

            List<FindResultDTO> finalResult = new ArrayList<>();
            if(sampleEntities != null){
                for (PaperIndex index : sampleEntities) {
                    FindResultDTO r = new FindResultDTO(index);
                    finalResult.add(r);
                }
            }

            result = reviewerIndexService.findReviewerPaper(finalResult);

            log.info("Finish searching reviewers.Search type: more like this, Found:" + result.size());


        }else if(searchDTO.getSearchType().equals(SearchReviewerType.GEO)){

            List<Location> locations = locationService.findLocationsAuthors(searchDTO.getValue());
            QueryBuilder query = ftn.uns.ac.rs.scientificCenter.elasticSearch.service.QueryBuilder.geoSearch(locations);

            result = resultRetriever.getResultsReviewer(query);

            log.info("Finish searching reviewers.Search type: geo, Found:" + result.size());

        }

        return result;
    }
}
