package ftn.uns.ac.rs.scientificCenter.enumeration;

public enum UserRole {
    AUTHOR,
    REVIEWER,
    EDITOR,
    ADMIN
}
