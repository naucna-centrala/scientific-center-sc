package ftn.uns.ac.rs.scientificCenter.service.implementation;

import ftn.uns.ac.rs.scientificCenter.dto.ValidationDTO;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IScientificFieldService;
import ftn.uns.ac.rs.scientificCenter.service.interfaces.IValidationService;
import ftn.uns.ac.rs.scientificCenter.service.process.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ValidationService implements IValidationService {

    @Autowired
    IScientificFieldService scientificFieldService;

    @Autowired
    ProcessService processService;

    @Override
    public ValidationDTO checkNumberScientificFields(int number) {

        int sizeAll = scientificFieldService.findAll().size();
        ValidationDTO validation = new ValidationDTO();

        if(sizeAll < number){
            validation.setMessage("Please enter scientific fields from 1 to " + sizeAll);
            validation.setValidate(false);
        }else{
            validation.setValidate(true);
            validation.setMessage("Validation successful!");
        }

        return validation;
    }

    @Override
    public ValidationDTO checkNumberReviewer(int number,String processId) {

        List<Object> objects = (List<Object>) processService.getVariable(processId,"all_reviewers_list");
        int sizeAll = objects.size();
        ValidationDTO validation = new ValidationDTO();

        if(sizeAll < number){
            validation.setMessage("Please enter number of reviewers from 2 to " + sizeAll);
            validation.setValidate(false);
        }else{
            validation.setValidate(true);
            validation.setMessage("Validation successful!");
        }
        return validation;
    }

    @Override
    public ValidationDTO checkNumberEditor(int number, String processId) {
        List<Object> objects = (List<Object>) processService.getVariable(processId,"all_editors_list");
        int sizeAll = objects.size();
        ValidationDTO validation = new ValidationDTO();

        if(sizeAll < number){
            validation.setMessage("Please enter number of editors from 0 to " + sizeAll);
            validation.setValidate(false);
        }else{
            validation.setValidate(true);
            validation.setMessage("Validation successful!");
        }
        return validation;
    }
}
