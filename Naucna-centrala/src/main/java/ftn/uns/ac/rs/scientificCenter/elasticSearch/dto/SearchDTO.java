package ftn.uns.ac.rs.scientificCenter.elasticSearch.dto;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.enumeration.SearchType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchDTO {

    private SearchType searchType;
    private String field;
    private String value;
}
