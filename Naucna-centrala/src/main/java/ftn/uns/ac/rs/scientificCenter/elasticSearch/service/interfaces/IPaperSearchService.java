package ftn.uns.ac.rs.scientificCenter.elasticSearch.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.elasticSearch.model.PaperIndex;
import ftn.uns.ac.rs.scientificCenter.model.Paper;
import java.io.File;

public interface IPaperSearchService {

    void indexPaper(Paper paper);
    String getText(File file);
    PaperIndex add(PaperIndex paper);
    void delete(PaperIndex paper);
    Iterable<PaperIndex> findAll();
    PaperIndex findById(String filename);
    String getPaperText(String filename);

}
