package ftn.uns.ac.rs.scientificCenter.dto;

import ftn.uns.ac.rs.scientificCenter.model.Magazine;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionViewDTO implements Serializable {

    private String subscriptionId;
    private Magazine magazine;
}
