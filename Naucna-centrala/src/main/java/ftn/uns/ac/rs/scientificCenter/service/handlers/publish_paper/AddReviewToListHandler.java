package ftn.uns.ac.rs.scientificCenter.service.handlers.publish_paper;

import ftn.uns.ac.rs.scientificCenter.dto.ReviewInfoPaperDTO;
import ftn.uns.ac.rs.scientificCenter.model.User;
import ftn.uns.ac.rs.scientificCenter.service.implementation.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class AddReviewToListHandler implements TaskListener {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Override
    public void notify(DelegateTask delegateTask) {

        List<ReviewInfoPaperDTO> reviews = (List<ReviewInfoPaperDTO>) delegateTask.getVariable("list_reviews");

        ReviewInfoPaperDTO r = new ReviewInfoPaperDTO();

        User u = userDetailsService.findByUsername(delegateTask.getVariable("oneUserId").toString()).get();
        r.setFirst_name(u.getFirst_name());
        r.setLast_name(u.getLast_name());

        r.setComment_editor(delegateTask.getVariable("comment_editor").toString());
        r.setRecommendation(delegateTask.getVariable("recommendation").toString());
        r.setComment_author(delegateTask.getVariable("comment_author").toString());

        reviews.add(r);

        delegateTask.setVariable("list_reviews", reviews);

        log.info("Review added to the list!");

    }

}
