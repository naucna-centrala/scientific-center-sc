package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.EditorialBoard;

public interface IEditorialBoardService {

    EditorialBoard save(EditorialBoard editorialBoard);
    EditorialBoard findById(Long id);
}
