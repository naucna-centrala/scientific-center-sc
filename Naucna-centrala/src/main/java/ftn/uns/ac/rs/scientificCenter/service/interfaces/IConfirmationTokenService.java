package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.model.ConfirmationToken;
import ftn.uns.ac.rs.scientificCenter.model.User;

public interface IConfirmationTokenService {

    ConfirmationToken save(ConfirmationToken token);
    void delete(User user);
}
