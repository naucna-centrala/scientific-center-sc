package ftn.uns.ac.rs.scientificCenter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;

@Entity
@Table(name = "coauthor")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coauthor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @Column(name = "email")
    @Email
    private String email;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @ManyToOne
    @JoinTable(name = "coauthors_paper", joinColumns = @JoinColumn(name = "coauthor_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "paper_id", referencedColumnName = "id"))
    @JsonIgnore
    private Paper paper;

    public Coauthor(String first_name,String last_name, String city){
        this.first_name = first_name;
        this.last_name = last_name;
        this.city = city;
    }
}
