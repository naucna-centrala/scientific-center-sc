package ftn.uns.ac.rs.scientificCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormFieldsDTO {

	private String taskId;
	private List<FormDataDTO> formFields;
	private String processInstanceId;

	
}
