package ftn.uns.ac.rs.scientificCenter.dto;

import ftn.uns.ac.rs.scientificCenter.enumeration.PaymentMethod;
import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MagazineInfoDTO implements Serializable {

    private Long id;
    private String name;
    private String issn;
    private PaymentMethod payment_method;
    private Collection<ScientificField> magazine_fields;
    private String uuid;
}
