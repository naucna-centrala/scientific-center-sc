package ftn.uns.ac.rs.scientificCenter.elasticSearch.service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ParseQuery {

    private static char[] lat = { 'A', 'B', 'V', 'G', 'D', '\u0110', 'E',
            '\u017D', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S',
            'T', '\u0106', 'U', 'F', 'H', 'C', '\u010C', '\u0160', 'a', 'b',
            'v', 'g', 'd', '\u0111', 'e', '\u017e', 'z', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's', 't', '\u0107', 'u', 'f', 'h', 'c',
            '\u010d', '\u0161' };

    private static char[] cyr = { '\u0410', '\u0411', '\u0412', '\u0413',
            '\u0414', '\u0402', '\u0415', '\u0416', '\u0417', '\u0418',
            '\u0408', '\u041A', '\u041B', '\u041C', '\u041D', '\u041E',
            '\u041F', '\u0420', '\u0421', '\u0422', '\u040B', '\u0423',
            '\u0424', '\u0425', '\u0426', '\u0427', '\u0428', '\u0430',
            '\u0431', '\u0432', '\u0433', '\u0434', '\u0452', '\u0435',
            '\u0436', '\u0437', '\u0438', '\u0458', '\u043A', '\u043B',
            '\u043C', '\u043D', '\u043E', '\u043F', '\u0440', '\u0441',
            '\u0442', '\u045B', '\u0443', '\u0444', '\u0445', '\u0446',
            '\u0447', '\u0448' };

    private static char[] cyr_other = { '\u0409', '\u040A', '\u040F', '\u0459', '\u045A', '\u045F'};

    public static String toLatin(String s) {
        String t = s;
        for (int i = 0; i < cyr.length; i++)
            t = t.replace(cyr[i], lat[i]);
        t = replace(t, "\u0409", "LJ");
        t = replace(t, "\u040A", "NJ");
        t = replace(t, "\u040F", "D\u017D");
        t = replace(t, "\u0459", "lj");
        t = replace(t, "\u045A", "nj");
        t = replace(t, "\u045F", "d\u017E");
        return t;
    }

    private static String replace(String s, String src, String dest) {
        StringBuffer retVal = new StringBuffer();
        int finishedPos = 0;
        int startPos = 0;
        int srcLen = src.length();
        while ((startPos = s.indexOf(src, startPos)) != -1) {
            if (startPos != finishedPos)
                retVal.append(s.substring(finishedPos, startPos));
            startPos += srcLen;
            retVal.append(dest);
            finishedPos = startPos;
        }
        if (finishedPos < s.length())
            retVal.append(s.substring(finishedPos));

        return retVal.toString();
    }

    public static String parseQuery(String query){
        String queryBegin = query;
        query = query.toLowerCase();

        for(int i =0;i< query.length();i++){
            String letter = Character.toString(query.charAt(i));
            if(new String(cyr).contains(letter) || new String(cyr_other).contains(letter)){
                query = toLatin(query);
                break;
            }
        }

        if(query.contains("\u017e")){
            query = replace(query,"\u017e","z");

        }
        if(query.contains("\u0107")){
            query = replace(query,"\u0107","c");

        }
        if(query.contains("\u010d")){
            query = replace(query,"\u010d","c");

        }
        if(query.contains("\u0161")){
            query = replace(query,"\u0161","s");

        }
        if(query.contains("\u0111")){
            query = replace(query,"\u0111","d");

        }
        log.info("Successfully parsed " + queryBegin + " to " + query);

        return query;
    }
}
