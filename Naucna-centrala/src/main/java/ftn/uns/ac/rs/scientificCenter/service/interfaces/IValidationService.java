package ftn.uns.ac.rs.scientificCenter.service.interfaces;

import ftn.uns.ac.rs.scientificCenter.dto.ValidationDTO;

public interface IValidationService {

    ValidationDTO checkNumberScientificFields(int number);
    ValidationDTO checkNumberReviewer(int number,String processId);
    ValidationDTO checkNumberEditor(int number,String processId);
}
