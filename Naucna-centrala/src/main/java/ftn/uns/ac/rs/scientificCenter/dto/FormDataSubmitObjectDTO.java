package ftn.uns.ac.rs.scientificCenter.dto;

import ftn.uns.ac.rs.scientificCenter.model.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormDataSubmitObjectDTO {
    private String name;
    private List<Object> value = new ArrayList<>();
}
