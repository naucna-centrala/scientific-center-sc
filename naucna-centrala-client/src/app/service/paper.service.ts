import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { saveAs } from "file-saver";

@Injectable({
  providedIn: 'root'
})
export class PaperService {

  constructor(private http: HttpClient) { }

  url = '/api/paper';

  uploadDocument(file:File): Observable<any>{

    const options = {
      headers: new HttpHeaders()
    };
    let formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post<any>(`${this.url}/savefile`, formData, options);

  }

  loadDocument(filename:String){
    
    return this.http.get(this.url + "/getfile/" + filename) as Observable<any>;
  }

  deleteDocument(filename:String){
    return this.http.get(this.url + "/delete/" + filename);
  }

  getPapers(username:string){
    return this.http.get(this.url + '/getPaper/' + username) as Observable<any>;
  }

  downloadFile(filename: string){
    return this.http.get(`${this.url}/download/` + filename, { responseType: 'blob' });
  }

  getAllPapers(){
    return this.http.get(this.url +"/getAll") as Observable<any>;
  }

  getAllReviewers(){
    return this.http.get(this.url +"/all/reviewers") as Observable<any>;
  }
  
}
