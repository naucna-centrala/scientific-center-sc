import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ValidationDTO } from '../dto/ValidationDTO';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor(private http: HttpClient) { }

//   httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8',
//                                 'Authorization' : 'Bearer ' + this.tokenService.getToken()})
//   };

  url = '/api/validate';

  checkScientificField(all:number): Observable<any> {
    return this.http.get<ValidationDTO>(this.url + "/check/fields/"+ all);
  }
 
  checkReviewersNumber(all:number, processId:string): Observable<any> {
    return this.http.get<ValidationDTO>(this.url + "/check/reviewer/"+ all + "/" +processId);
  }

  checkEditorsNumber(all:number, processId:string): Observable<any> {
    return this.http.get<ValidationDTO>(this.url + "/check/editor/"+ all + "/" +processId);
  }
  

}
