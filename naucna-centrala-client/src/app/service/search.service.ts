import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SearchDTO } from '../dto/SearchDTO';
import { LogicalQueryDTO } from '../dto/LogicalQueryDTO';
import { SearchReviewerDTO } from '../dto/SearchReviewerDTO';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  url = '/api/search';

  searchQuery(query: SearchDTO){
    return this.http.post(this.url+ "/query",query) as Observable<any>;
  }

  searchLogical(query: LogicalQueryDTO){
    return this.http.post(this.url+ "/query/logical",query) as Observable<any>;
  }

  searchReviewer(query: SearchReviewerDTO){
    return this.http.post(this.url+ "/reviewer",query) as Observable<any>;

  }

}
