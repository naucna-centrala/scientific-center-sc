import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SubscriptionRequestNCDTO } from '../dto/SubscriptionRequestNCDTO';
import { SubscriptionResponseDTO } from '../dto/SubscriptionResponseDTO';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  constructor(private http: HttpClient) { }

  url = '/api/subscription';

 
  successSubscription(id:string){
    return this.http.get(this.url + "/subscribe/success/" + id) as Observable<any>;

  }

  failSubscription(id:string){
    return this.http.get(this.url + "/subscribe/fail/" + id) as Observable<any>;
  }

  subscribe(request: SubscriptionRequestNCDTO){
    return this.http.post<SubscriptionResponseDTO>(this.url + "/subscribe", request) ;

  }

  getAllSubscribe(username:string){
    return this.http.get(this.url +"/get/all/" + username) as Observable<any>;
  }

  unsubscribe(kpId: string){
    return this.http.get(this.url + "/unsubscribe/" + kpId) as Observable<any>;

  }

  checkSubscription(username:string){
    return this.http.get(this.url + "/check/subscribe/" + username);
  }


}
