import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Magazine } from '../model/Magazine';
import { OrderDTO } from '../dto/OrderDTO';
import { OrderResponseDTO } from '../dto/OrderResponseDTO';
import { ResponseMessage } from '../dto/ResponseMessageDTO';
import { SubscriptionResponseDTO } from '../dto/SubscriptionResponseDTO';
import { MagazinePricesDTO } from '../dto/MagazinePricesDTO';

@Injectable({
  providedIn: 'root'
})
export class MagazineService {

  constructor(private http: HttpClient) { }

//   httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8',
//                                 'Authorization' : 'Bearer ' + this.tokenService.getToken()})
//   };

  url = '/api/magazine';

  getMagazines(): Observable<any> {
    return this.http.get(this.url) as Observable<any>;
  }

  buyMagazine(orderDTO: OrderDTO):Observable<any>{
      const urlBuy = this.url + "/buy";
      return this.http.post(urlBuy, orderDTO) as Observable<any>;
  }

  getOneMagazine(issn:string){
    return this.http.get<ResponseMessage>(this.url +"/get/" + issn);
  }

  continueProcessAddMagazine(processId:string){
    return this.http.get<SubscriptionResponseDTO>(this.url +"/payment/added?processId=" + processId);
  }

  getAllSubscribe(username:string){
    return this.http.get<Magazine []>(this.url + "/get/subscribe/" + username);

  }

  getNotSubscribe(username:string){
    return this.http.get<Magazine []>(this.url + "/get/not/subscribe/" + username);

  }

  getPrices(magazines:Magazine[]){
    return this.http.post<MagazinePricesDTO[]>(this.url + "/get/prices",magazines);

  }

  getOpenAccess(){
    return this.http.get(this.url + "/get/free") as Observable<any>;
  }





}
