import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormFieldsDTO } from '../dto/FormFieldsDTO';
import { FormDataDTO } from '../dto/FormDataDTO';
import { FormDataObjectDTO } from '../dto/FormDataObjectDTO';
import { FormMixDataSubmitDTO } from '../dto/FormMixDataSubmitDTO';

@Injectable({
  providedIn: 'root'
})
export class ProcessService {

  constructor(private http: HttpClient) { }

//   httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8',
//                                 'Authorization' : 'Bearer ' + this.tokenService.getToken()})
//   };

  url = '/api/process';

  startProcess(process_name: string, username:string){
    return this.http.get<FormFieldsDTO>(this.url + '/get/' + process_name +'/' + username);
  }


  getNextForm(processId:string, username:string){
    return this.http.get<FormFieldsDTO>(this.url + '/get/next/form/'+ processId + '/' + username);
  }

  submitForm(data: FormDataDTO[], taskId: string){
    return this.http.post<string>(this.url + '/post/' + taskId, data);
  }

  submitFormObject(data: FormDataObjectDTO[], taskId: string){
    return this.http.post<string>(this.url + '/post/object/' + taskId, data);
  }

  submitMixForm(data:FormMixDataSubmitDTO, taskId:String){
    return this.http.post<string>(this.url + '/post/mix/' + taskId, data);

  }

  getTasksUsername(username:string,taskName: string){
    return this.http.get(this.url + '/task/'+ username +'/'+ taskName) as Observable<any>;

  }

  changeVariables(processId: string, taskId:string, data: FormDataDTO[]){

    return this.http.post(this.url + '/change/variables/' + processId + '/' + taskId, data);

  }

  getTasks(processId:string, username: string){
    return this.http.get(this.url + '/get/form/'+ processId + '/' + username) as Observable<any>;
  }

  sendMessage(processId:string){
    return this.http.get(this.url +"/send/message/" + processId) as Observable<any>;
  }

 
}
