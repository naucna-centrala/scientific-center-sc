import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MagazineComponent } from './components/magazine/magazine.component';
import { SuccessTransactionComponent } from './components/success-transaction/success-transaction.component';
import { FailTransactionComponent } from './components/fail-transaction/fail-transaction.component';
import { GuestHomeComponent } from './components/guest-home/guest-home.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { RoleGuardService } from './auth/role-guard.service';
import { AuthorHomeComponent } from './components/author-home/author-home.component';
import { EditorHomeComponent } from './components/editor-home/editor-home.component';
import { AddMagazineComponent } from './components/add-magazine/add-magazine.component';
import { ActivateMagazineComponent } from './components/admin-home/activate-magazine/activate-magazine.component';
import { SuccessSubscriptionComponent } from './components/success-subscription/success-subscription.component';
import { FailSubscriptioniComponent } from './components/fail-subscriptioni/fail-subscriptioni.component';
import { SuccessMagazineAddComponent } from './components/success-magazine-add/success-magazine-add.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { PublishPaperComponent } from './components/publish-paper/publish-paper.component';
import { CheckThematicPaperComponent } from './components/editor-home/check-thematic-paper/check-thematic-paper.component';
import { FormatPaperComponent } from './components/editor-home/format-paper/format-paper.component';
import { CorrectPaperComponent } from './components/author-home/correct-paper/correct-paper.component';
import { AddReviewerPaperComponent } from './components/editor-home/add-reviewer-paper/add-reviewer-paper.component';
import { ReviewerHomeComponent } from './components/reviewer-home/reviewer-home.component';
import { ReviewPaperComponent } from './components/review-paper/review-paper.component';
import { AcceptPaperComponent } from './components/editor-home/accept-paper/accept-paper.component';
import { PaperViewComponent } from './components/author-home/paper-view/paper-view.component';
import { SearchReviewersComponent } from './components/search-reviewers/search-reviewers.component';

const routes: Routes = [
  {
    path: 'magazine',
    component: MagazineComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['AUTHOR','EDITOR']
    }
    
  },{
    path: 'subscription',
    component: SubscriptionComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['AUTHOR','EDITOR']
    }
    
  },
  {
    path: '',
    component: GuestHomeComponent,
    
  },
  {
    path: 'search/reviewer',
    component: SearchReviewersComponent,
    
  },
  {
    path: 'success',
    component: SuccessTransactionComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['AUTHOR','EDITOR']
    }
  },
  {
    path: 'fail',
    component: FailTransactionComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['AUTHOR','EDITOR']
    }
  },
  {
    path: 'admin',
    component: AdminHomeComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['ADMIN']
    }
  },
  {
    path: 'author',
    component: AuthorHomeComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['AUTHOR']
    }
  },
  {
    path: 'editor',
    component: EditorHomeComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR']
    }
  },
  {
    path: 'addmagazine',
    component: AddMagazineComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR']
    }
  },
  {
    path: 'activate/magazine',
    component: ActivateMagazineComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['ADMIN']
    }
  },
  {
    path: 'subscription/success',
    component: SuccessSubscriptionComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['AUTHOR','EDITOR']
    }
  },
  {
    path: 'subscription/fail',
    component: FailSubscriptioniComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['AUTHOR','EDITOR']
    }
  },
  {
    path: 'add/magazine/success/:id',  //id= processInstance
    component: SuccessMagazineAddComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: ['EDITOR']
    }
  },
  {
    path: 'publish/paper/:id',
    component: PublishPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['AUTHOR']
    }
  },
  {
    path: 'paper/check/thematic',
    component: CheckThematicPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR']
    }
  },
  {
    path: 'paper/format',
    component: FormatPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR']
    }
  },
  {
    path: 'correct/paper',
    component: CorrectPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['AUTHOR']
    }
  },
  {
    path: 'add/reviewer/paper',
    component: AddReviewerPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR']
    }
  },
  {
    path: 'do/review',
    component: ReviewPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR', 'REVIEWER']
    }
  },
  {
    path: 'reviewer',
    component: ReviewerHomeComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['REVIEWER']
    }
  },
  {
    path: 'accept/paper',
    component: AcceptPaperComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['EDITOR']
    }
  },
  {
    path: 'papers',
    component: PaperViewComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole : ['AUTHOR']
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
