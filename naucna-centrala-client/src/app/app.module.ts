import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MagazineComponent } from './components/magazine/magazine.component';
import { MagazineService } from './service/magazine.service';
import { HttpClientModule } from '@angular/common/http';
import { SuccessTransactionComponent } from './components/success-transaction/success-transaction.component';
import { FailTransactionComponent } from './components/fail-transaction/fail-transaction.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { MatDialogModule, MatButtonModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuestHomeComponent } from './components/guest-home/guest-home.component';
import { RoleGuardService } from './auth/role-guard.service';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthorHomeComponent } from './components/author-home/author-home.component';
import { ProcessService } from './service/process.service';
import { EditorHomeComponent } from './components/editor-home/editor-home.component';
import { AddMagazineComponent } from './components/add-magazine/add-magazine.component';
import { ActivateMagazineComponent } from './components/admin-home/activate-magazine/activate-magazine.component';
import { ValidationService } from './service/validation.service';
import { SuccessSubscriptionComponent } from './components/success-subscription/success-subscription.component';
import { FailSubscriptioniComponent } from './components/fail-subscriptioni/fail-subscriptioni.component';
import { SubscriptionService } from './service/subscription.service';
import { SuccessMagazineAddComponent } from './components/success-magazine-add/success-magazine-add.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { PublishPaperComponent } from './components/publish-paper/publish-paper.component';
import { MaterialModule } from './material.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PaperService } from './service/paper.service';
import { CheckThematicPaperComponent } from './components/editor-home/check-thematic-paper/check-thematic-paper.component';
import { FormatPaperComponent } from './components/editor-home/format-paper/format-paper.component';
import { CorrectPaperComponent } from './components/author-home/correct-paper/correct-paper.component';
import { AddReviewerPaperComponent } from './components/editor-home/add-reviewer-paper/add-reviewer-paper.component';
import { ReviewPaperComponent } from './components/review-paper/review-paper.component';
import { ReviewerHomeComponent } from './components/reviewer-home/reviewer-home.component';
import { AcceptPaperComponent } from './components/editor-home/accept-paper/accept-paper.component';
import { PaperViewComponent } from './components/author-home/paper-view/paper-view.component';
import {AtomSpinnerModule} from 'angular-epic-spinners';
import { SearchService } from './service/search.service';
import { SearchReviewersComponent } from './components/search-reviewers/search-reviewers.component';

@NgModule({
  declarations: [
    AppComponent,
    MagazineComponent,
    SuccessTransactionComponent,
    FailTransactionComponent,
    NavbarComponent,
    LoginComponent,
    GuestHomeComponent,
    AdminHomeComponent,
    RegistrationComponent,
    AuthorHomeComponent,
    EditorHomeComponent,
    AddMagazineComponent,
    ActivateMagazineComponent,
    SuccessSubscriptionComponent,
    FailSubscriptioniComponent,
    SuccessMagazineAddComponent,
    SubscriptionComponent,
    PublishPaperComponent,
    CheckThematicPaperComponent,
    FormatPaperComponent,
    CorrectPaperComponent,
    AddReviewerPaperComponent,
    ReviewPaperComponent,
    ReviewerHomeComponent,
    AcceptPaperComponent,
    PaperViewComponent,
    SearchReviewersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    MatButtonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    PdfViewerModule,
    AtomSpinnerModule
    
  ],
  entryComponents: [
    LoginComponent,
    RegistrationComponent
  ],
  providers: [RoleGuardService, MagazineService, ProcessService, ValidationService, SubscriptionService, PaperService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
