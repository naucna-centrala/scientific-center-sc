export class SubscriptionRequestNCDTO{

    username : string;
    magazine_id : BigInteger;

    constructor(username: string, magazine_id){
        this.username = username;
        this.magazine_id = magazine_id;
    }
}