import { ScientificField } from '../model/ScientificField';

export class FormDataObjectDTO{
    name: string;
    value: Object[];

    constructor(name:string, value:Object[]){
        this.name = name;
        this.value = value;
    }
}