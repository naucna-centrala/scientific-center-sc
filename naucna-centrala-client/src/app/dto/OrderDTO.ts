export class OrderDTO{

    sellerUuid: string;
    description: string;
    successUrl: string;
    failUrl: string;

}