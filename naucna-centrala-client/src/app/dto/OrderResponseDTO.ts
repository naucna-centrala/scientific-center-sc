export class OrderResponseDTO{

    transactionId: string;
    paymentMethods: string[];
    uuid: string;
    amount:number;
}