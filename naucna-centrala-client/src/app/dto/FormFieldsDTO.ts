import { FormDataDTO } from './FormDataDTO';

export class FormFieldsDTO{
    taskId: string;
	formFields: FormDataDTO[];
	processInstanceId: string;
}