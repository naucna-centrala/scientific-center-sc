export class SearchReviewerDTO{
    searchType: SearchReviewerType;
    field: string;
    value: string;
}

export enum SearchReviewerType {

    REGULAR,
    MORE_LIKE_THIS,
    GEO
}