export class LogicalQueryDTO{
    query: string;

    constructor(query:string){
        this.query = query;
    }
}