import { FormDataDTO } from './FormDataDTO';
import { FormDataObjectDTO } from './FormDataObjectDTO';

export class FormMixDataSubmitDTO{
    data: FormDataDTO[];
    dataObject: FormDataObjectDTO[];

    constructor(data: FormDataDTO[], dataObject: FormDataObjectDTO[]){
        this.data = data;
        this.dataObject = dataObject;
    }
}