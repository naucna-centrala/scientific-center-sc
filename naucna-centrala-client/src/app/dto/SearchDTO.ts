export class SearchDTO{
    searchType: SearchType
    field:string;
    value:string;
}

export enum SearchType{
    REGULAR,
    FUZZY,
    PHRASE,
    RANGE,
    PREFIX
}