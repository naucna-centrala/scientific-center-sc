export class Coauthor{
    id: BigInteger;
    first_name:string;
    last_name:string;
    email:string;
    city:string;
    state:string;

}