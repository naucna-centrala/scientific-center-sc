import { ScientificField } from './ScientificField';

export class Editor{

    title:string;
    id:BigInteger;
    username:string;
    email:string;
    first_name:string;
    last_name:string;
    city:string;
    state:string;
    user_fields:ScientificField[];
    

}