import { EditorialBoard } from './EditorialBoard';

export class Magazine{
    id: BigInteger;
    name: string;
    issn: string;
    payment_method: PaymentMethod;
    active: boolean;
    uuid:string;
    editorialBoard: EditorialBoard;

}

enum PaymentMethod{
    OPEN_ACCESS,
    WITH_SUBSCRIPTION

}