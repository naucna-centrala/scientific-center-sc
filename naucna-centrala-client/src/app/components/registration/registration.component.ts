import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ProcessService } from 'src/app/service/process.service';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { FormDataObjectDTO } from 'src/app/dto/FormDataObjectDTO';
import { ValidationService } from 'src/app/service/validation.service';
import { ValidationDTO } from 'src/app/dto/ValidationDTO';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  // private formFieldsDto = null;
  formFields = [];
  processInstance = "";
  enumValues = [];
  view: string = "firstPage"
  listOfValues: FormDataDTO[] = [];
  num_scientific: number;
  taskId_registration: string;
  formFieldsSecond = [];
  checked_reviewer: Boolean;
  deletedScientific: Object;
  check:Boolean;
  validateSF: ValidationDTO;


  constructor(@Inject(MAT_DIALOG_DATA) private data: any,private dialogRef: MatDialogRef<any>, 
  private registerService: ProcessService, private validateService: ValidationService) { }

  ngOnInit() {
    this.dialogRef.updateSize('50%', '80%');

    let x = this.registerService.startProcess('registration', '!!NO');

    x.subscribe(
      res => {
        console.log(res);
        this.formFields = res.formFields;
        this.processInstance = res.processInstanceId;
        this.taskId_registration = res.taskId;
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  cancelClicked(){
    this.dialogRef.close();
  }

  onNext(value){
    this.listOfValues = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      if(property == "Number of scientific area"){
        if(value[property] <= 0){
          alert("Please enter more scientific area!(>0)");
          return;
        }
        this.num_scientific = value[property];
      }else if(property == "Reviewer"){
        
        this.checked_reviewer = value[property];
      }
      this.listOfValues.push(new FormDataDTO(property,value[property]));
    }
    this.validateScientificFields(this.num_scientific);
      
  }
  
  validateScientificFields(value){
    let y = this.validateService.checkScientificField(value);

    y.subscribe(data=>{
      console.log(data);
      this.validateSF = data;
      if(this.validateSF.validate == false){
          alert(this.validateSF.message);  
      }else{
        this.submitVariables();
      }
    })

  }

  submitVariables(){
    if(this.view == 'thirdPage'){
      let y = this.registerService.changeVariables(this.processInstance,this.taskId_registration,this.listOfValues);
      y.subscribe(
        data => {
          this.getScientificForm();
        }
      )

    }else{
      this.registerService.submitForm(this.listOfValues,this.taskId_registration)
        .subscribe(data=>{
          this.getScientificForm();
      })
    }
  }

  getScientificForm(){
    let y = this.registerService.getNextForm(this.processInstance,"guest");
    
    y.subscribe(
      result=>{
        console.log(result);
        this.view ="secondPage";
        this.formFieldsSecond = result.formFields;
        this.processInstance = result.processInstanceId;
        this.taskId_registration = result.taskId;
        this.formFieldsSecond.forEach( (field) =>{
        if( field.value.type=='Object' && field.name == "all_scientific_area"){
          field.value.value.forEach((f)=>{
          this.enumValues.push(f);
        })
                
        }
        });
      })
  }

  createRange(){
    var items: number[] = [];
    for(var i = 1; i <= this.num_scientific; i++){
       items.push(i);
    }
    return items;
  }

  onSubmit(value){
    let listScientific = [];
    let listToSave = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property].title);
      this.checkFields(listScientific,value[property]);
      if(this.check == true){
        alert("Please choose different fields!");
        return;
      }
      listScientific.push(value[property]);
     
    }
    listToSave.push(new FormDataObjectDTO("scientific_area_list",listScientific));

    let x = this.registerService.submitFormObject(listToSave,this.taskId_registration);

    x.subscribe(
      data =>{
        let y = this.registerService.getNextForm(this.processInstance,"guest");

        y.subscribe(
          d =>{
            if(d == null){
              alert("Successfully registered!");
              this.dialogRef.close();
            }else{
              if(!this.checked_reviewer){
                console.log(d);
                this.formFields = d.formFields;
                this.processInstance = d.processInstanceId;
                this.taskId_registration = d.taskId;
                this.view = "thirdPage";
                this.enumValues = [];
              }else{
                d.formFields.forEach(e=>{
                  if(e.name =='Confirm status'){
                    alert("Successfully registered!");
                    this.dialogRef.close();
                  }
                })
                console.log(d);
                this.formFields = d.formFields;
                this.processInstance = d.processInstanceId;
                this.taskId_registration = d.taskId;
                this.view = "thirdPage";
                this.enumValues = [];
                
              }
            }
            
          })      
      })
  }

  checkFields(listScientific,value){
    let i = 0;
    listScientific.forEach(field=>{
      if(JSON.stringify(field) === JSON.stringify(value)){
        this.check = true;
        i = 1;
      }
    })
    if(i == 0){
      this.check = false;
    }
  }
  


}
