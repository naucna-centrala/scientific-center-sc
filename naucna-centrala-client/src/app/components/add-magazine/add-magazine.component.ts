import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { ProcessService } from 'src/app/service/process.service';
import { FormDataObjectDTO } from 'src/app/dto/FormDataObjectDTO';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { User } from 'src/app/model/User';
import { ValidationDTO } from 'src/app/dto/ValidationDTO';
import { ValidationService } from 'src/app/service/validation.service';
import { SubscriptionService } from 'src/app/service/subscription.service';
import { SubscriptionRequestNCDTO } from 'src/app/dto/SubscriptionRequestNCDTO';
import { MagazineService } from 'src/app/service/magazine.service';

@Component({
  selector: 'app-add-magazine',
  templateUrl: './add-magazine.component.html',
  styleUrls: ['./add-magazine.component.css']
})
export class AddMagazineComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  formFields = [];
  processInstance = "";
  enumValues = [];
  enumR = [];
  enumE= [];
  view: string = "firstPage"
  listOfValues: FormDataDTO[] = [];
  num_scientific: number;
  taskId_magazine: string;
  formFieldsSecond = [];
  checked_reviewer: Boolean;
  tasks = [];
  num_editors: number;
  num_reviewers: number;
  check: Boolean;
  validateSF:ValidationDTO;
  validateR:ValidationDTO;
  submitedNumbE: boolean = false;
  submitedNumbR: boolean = false;
  add: boolean = false;
  i : boolean = true;
  check_subscription: string;
  issn:string;
  loaded = "false";

  constructor(private router: Router, private magazineService: ProcessService, private tokenService: TokenStorageService,
    private validateService:ValidationService, private subscriptionService: SubscriptionService,
    private databaseMagazine: MagazineService) { }

  ngOnInit() {
    this.header.editorView();
    let x = this.magazineService.startProcess('Add_magazine', this.tokenService.getUsername());

    x.subscribe(
      res => {
        console.log(res);
        this.formFields = res.formFields;
        this.processInstance = res.processInstanceId;
        this.taskId_magazine = res.taskId;
        this.formFields.forEach( (field) =>{
          
          if( field.value.type=='enum'){
            this.enumValues = Object.keys(field.value.value);
          }
        });
        this.loaded = "true";
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'home'){
      this.router.navigate(['/editor']);
    }
    
  }

  onNext(value){
    this.listOfValues = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      if(property == "Number of scientific area"){
        if(value[property] <= 0){
          alert("Please enter more scientific area!(>0)");
          return;
        }
        this.num_scientific = value[property];
      }else if(property == "Payment method"){
        this.check_subscription = value[property];
      }else if(property == "ISSN number"){
        this.issn = value[property];
      }
      this.listOfValues.push(new FormDataDTO(property,value[property]));
    }
    this.listOfValues.push(new FormDataDTO('main_editor_username',this.tokenService.getUsername()));
    this.validateScientificFields(this.num_scientific);
    
  }

  validateScientificFields(value){
    let y = this.validateService.checkScientificField(value);

    y.subscribe(data=>{
      console.log(data);
      this.validateSF = data;
      if(this.validateSF.validate == false){
          alert(this.validateSF.message);  
      }else{
        this.submitVariables();
      }
    })
  }

  submitVariables(){
    if(this.view == 'fourPage'){
      let y = this.magazineService.changeVariables(this.processInstance,this.taskId_magazine,this.listOfValues);
      y.subscribe(
        data => {
          this.getScientificForm();
        }
      )

    }else{
      this.magazineService.submitForm(this.listOfValues,this.taskId_magazine)
      .subscribe(data=>{
        this.getScientificForm(); 
    })
    }
  }

  getScientificForm(){
    this.enumValues = [];
    let y = this.magazineService.getNextForm(this.processInstance, this.tokenService.getUsername());
    
    y.subscribe(
      result=>{
        console.log(result);
        this.view ="secondPage";
        this.formFieldsSecond = result.formFields;
        this.processInstance = result.processInstanceId;
        this.taskId_magazine = result.taskId;
        this.formFieldsSecond.forEach( (field) =>{
        if( field.value.type=='Object' && field.name == "all_scientific_area"){
          field.value.value.forEach((f)=>{
          this.enumValues.push(f);
        })
                
        }
        });
      })
  }

  createRange(name){
    var items: number[] = [];
    if(name == 'Scientific area'){
      for(var i = 1; i <= this.num_scientific; i++){
        items.push(i);
     }
    }else if(name == 'Add Editor'){
      items =[];
      for(var i = 1; i <= this.num_editors; i++){
        items.push(i);
     }
    }else if(name == 'Add Reviewer'){
      items =[];
      for(var i = 1; i <= this.num_reviewers; i++){
        items.push(i);
     }
    }
   
    return items;
  }

  cancelClicked(){
    this.router.navigate(['editor']);
  }

  onSubmit(value){
    let listScientific = [];
    let listToSave = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property].title);
      this.checkFields(listScientific,value[property]);
      if(this.check == true){
        alert("Please choose different fields!");
        return;
      }
      listScientific.push(value[property]);
     
    }
    listToSave.push(new FormDataObjectDTO("scientific_area_list",listScientific));

    let x = this.magazineService.submitFormObject(listToSave,this.taskId_magazine);

    x.subscribe(
      data =>{
        this.getTasks();
       
      })
  }

  getTasks(){
    this.enumR = [];
    this.enumE = [];
    let y = this.magazineService.getTasks(this.processInstance,this.tokenService.getUsername());

        y.subscribe(
          data=>{
            if(data ==null){
              alert("Successfully magazine updated!");
              // this.router.navigate(['editor']);

              let x  = this.databaseMagazine.getOneMagazine(this.issn);
              x.subscribe(
                data=>{
                  console.log(data.message);
                  window.location.href = "https://localhost:4000/choose/payment/false/bojana/" + 
                  data.message +"/" + this.processInstance;
                 
                }
              )

   
            }else{
              if(data[0].formFields[0].name == 'Title'){
                alert("Please change your ISSN number!");
                this.enumValues = [];
                this.view="fourPage";
                this.formFields = data[0].formFields;
                this.processInstance = data[0].processInstanceId;
                this.taskId_magazine = data[0].taskId;
                this.formFields.forEach(field=>{
                  if( field.value.type=='enum'){
                    this.enumValues = Object.keys(field.value.value);
                  }
                })
              }else{
                if(this.i==true){
                  alert("Successfully magazine created!");
                  this.i = false;

                }
                console.log(data);
                this.view ="thirdPage";
                this.tasks = data;
                this.tasks.forEach( (task) =>{
                  task.formFields.forEach(field =>{
                    if(field.value.type=='Object' && field.name == "all_reviewers_list"){
                      field.value.value.forEach(r2=>{
                        this.enumR.push(r2);

                      })
                    }else if(field.value.type=='Object' && field.name == "all_editors_list"){
                      field.value.value.forEach(r=>{
                        this.enumE.push(r);

                      })
                  }
                  })     
            });
    
              }      
            }
            
          }
        )
  }

  onNextTask(value,task){
    let listEditors = [];
    let listReviewers = [];
    let listObjects = [];
    this.listOfValues = [];
    console.log(task);
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      if(property == "Number of editors"){
        if(value[property] < 0){
          alert("Please enter more editors!(=>0)");
          return;
        }
        this.num_editors = value[property];
        this.submitedNumbE = true;
      }else if(property == "Number of reviewer"){
        if(value[property] < 2){
          alert("Please enter more reviewers!(=>2)");
          return;
        }
        this.num_reviewers = value[property];
        this.submitedNumbR = true;

      }else{
        if(task.formFields[0].name == 'all_reviewers_list'){
          console.log("uslo u task reviewer");
          console.log(value[property])
          console.log(value[property].id)
          this.checkFields(listReviewers,value[property]);
          if(this.check == true){
            alert("Please choose different fields!");
            return;
          }
          listReviewers.push(new User(value[property].id,value[property].username));
          this.add = true;
        }else if(task.formFields[0].name == 'all_editors_list'){
          console.log("uslo u task editor");
          this.checkFields(listEditors,value[property]);
          if(this.check == true){
            alert("Please choose different fields!");
            return;
          }
          listEditors.push(new User(value[property].id,value[property].username));
          this.add = true;
        }
      }
      this.listOfValues.push(new FormDataDTO(property,value[property]));
    }

    console.log("Broj: " +listReviewers.length);
    this.validateReviewersEditors(listReviewers,listObjects,task.taskId,listEditors);
    
  }

  validateReviewersEditors(listReviewers,listObjects,taskId,listEditors){

    if(this.submitedNumbR == true){
      let y = this.validateService.checkReviewersNumber(this.num_reviewers,this.processInstance);

      y.subscribe(data=>{
        console.log(data);
        this.validateR = data;
        if(this.validateR.validate == false){
            alert(this.validateR.message); 
            this.submitedNumbR = false;
 
        }else{
          this.submitVariableTasks(listReviewers,listObjects,taskId,listEditors);
          this.submitedNumbR = false;
        }
      })
    }else if(this.submitedNumbE == true){
      let y = this.validateService.checkEditorsNumber(this.num_editors,this.processInstance);

      y.subscribe(data=>{
        console.log(data);
        this.validateR = data;
        if(this.validateR.validate == false){
            alert(this.validateR.message);
            this.submitedNumbE = false;
  
        }else{
          this.submitVariableTasks(listReviewers,listObjects,taskId,listEditors);
          this.submitedNumbE = false;

        }
      })
    }else{
      this.submitVariableTasks(listReviewers,listObjects,taskId,listEditors);
      // this.add = false;

    }
   
  }

  submitVariableTasks(listReviewers,listObjects,taskId,listEditors){

    if(listReviewers.length!=0){
      listObjects.push(new FormDataObjectDTO('reviewer_list', listReviewers));
      console.log(taskId);
      let x = this.magazineService.submitFormObject(listObjects,taskId);
      x.subscribe(
        data =>{
            this.getTasks();
        }
      )
    }else if(listEditors.length !=0){
      listObjects.push(new FormDataObjectDTO('editor_list',listEditors));
      let y = this.magazineService.submitFormObject(listObjects,taskId);
      y.subscribe(
        data =>{
            this.getTasks();
        }
      )
    }else{
      let y = this.magazineService.submitForm(this.listOfValues,taskId);
      y.subscribe(
        data =>{
            this.getTasks();
        }
      )
    }
  }

  checkFields(list,value){
    let i = 0;
    list.forEach(field=>{
      if(JSON.stringify(field) === JSON.stringify(value)){
        this.check = true;
        i = 1;
      }
    })
    if(i == 0){
      this.check = false;
    }
  }


}
