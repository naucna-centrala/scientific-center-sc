import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { RegistrationComponent } from '../registration/registration.component';
import { SearchType, SearchDTO } from 'src/app/dto/SearchDTO';
import { SearchService } from 'src/app/service/search.service';
import { PaperService } from 'src/app/service/paper.service';
import { saveAs } from "file-saver";
import { LogicalQueryDTO } from 'src/app/dto/LogicalQueryDTO';


@Component({
  selector: 'app-guest-home',
  templateUrl: './guest-home.component.html',
  styleUrls: ['./guest-home.component.css']
})
export class GuestHomeComponent implements OnInit {

  type:string;
  value:string;
  field:string;
  listSearch: any;
  clickSearch = false;
  clickLogical = false;
  queryLogical: string ="";
  fieldAdded =false;
  operationAdded = false;
  logicValue:any = null;
  logicButton = false;
  firstField = true;
  valueAded = false;
  states: string[] = [];
  countFields = 0;

  constructor(public dialog: MatDialog, private searchService:SearchService,
     private paperService: PaperService) { }

  ngOnInit() {
    document.body.classList.add('bg-img');
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'login'){
      this.showLogin();
    } else if(feature == 'registration'){
      this.showRegistration();
    }else if(feature == 'home'){

    }
  }


  showLogin(){

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
      id: 1,
      title: "Bojana"
      };

    const dialogRef = this.dialog.open(LoginComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
    console.log("Dialog was closed")
    console.log(result)
    });

  }

  showRegistration(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
      id: 1,
      title: "Bojana"
      };

    const dialogRef = this.dialog.open(RegistrationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
    console.log("Dialog was closed")
    console.log(result)
    });

  }

  searchClicked(){
    if(this.clickSearch){
      this.clickSearch = false;
      this.type = null;
      this.value = null;
      this.field = null;
    }else{
      this.clickSearch = true;
      if(this.clickLogical){
        this.logicalClicked();
      }
    }
  }

  logicalClicked(){
    if(this.clickLogical){
      this.clickLogical = false;
      this.queryLogical="";
      this.fieldAdded =false;
      this.operationAdded = false;
      this.logicValue= null;
      this.logicButton = false;
      this.firstField = true;
      this.valueAded = false;
      this.states = [];
    }else{
      this.clickLogical = true;
      if(this.clickSearch){
        this.searchClicked();

      }
    }
  }

  search(){
    console.log(this.type);
    console.log(this.value);
    let object = new SearchDTO();
    if(this.type == "REGULAR"){
      object.searchType = SearchType.REGULAR;
    }else if(this.type == "FUZZY"){
      object.searchType = SearchType.FUZZY;

    }else if(this.type == "PHRASE"){
      object.searchType = SearchType.PHRASE;

    }else if(this.type == "RANGE"){
      object.searchType = SearchType.RANGE;

    }else if(this.type == "PREFIX"){
      object.searchType = SearchType.PREFIX;
    }

    object.field = this.field;
    object.value = this.value;

    let x = this.searchService.searchQuery(object);
    x.subscribe(
      data=>{
        this.listSearch = data;
        console.log(data);
      }
    )
  }

  download(paper) {
    let name = paper.filename +".pdf";
    this.paperService.downloadFile(name)
      .subscribe(
        data => {
          console.log("Save");
          this.downloadFile(data, name);
        },
        error => {
          console.log(error);
        }
      );
  }

  downloadFile(data, fileName) {
    var blob = new Blob([data], { type: 'application/pdf' });
    saveAs(blob, fileName);
    alert("Successfully file downloaded!");
  }

  clickField(field){
    if(this.fieldAdded){
      if(!this.valueAded){
        alert("Please add value of field!");

      }else{
        alert("Please add operator!");
      }
    }else{
      if(this.operationAdded){
        
        this.queryLogical = this.queryLogical + field + "=";
        this.fieldAdded = true;
        this.operationAdded = false;
        this.logicButton = false;
        this.valueAded = false;
        if(this.firstField){
          this.firstField = false;
        }
        this.countFields = this.countFields + 1;
        this.states.push(field + "=");
      }else{
        if(this.firstField){
          this.firstField = false;
          this.queryLogical = this.queryLogical + field + "=";
          this.fieldAdded = true;
          this.operationAdded = false;
          this.logicButton = false;
          this.valueAded = false;
          this.countFields = this.countFields + 1;
          this.states.push(field + "=");
        }else{
          alert("Please add operator!");
        }
      }
     
    }
  }

  clickOperation(operation){
    if(this.firstField){
      alert("Please add field first")
    }else{
      if(this.operationAdded){
        alert("Please add field!");
      }else{
        if(this.fieldAdded){
          
          if(this.logicValue == null || !this.valueAded){
            alert("Please add value of field!")
          }else{
            this.queryLogical = this.queryLogical + " " + operation + " ";
            this.operationAdded = true;
            this.logicButton = false;
            this.states.push(operation);
          }
          
        }else{
          this.queryLogical = this.queryLogical + " " + operation + " ";
          this.operationAdded = true;
          this.logicButton = false;
          this.states.push(operation);

        }
      }
    }
    
  }

  valuechange(newValue) {
    this.logicValue = newValue;
    console.log(newValue)
  } 
  addValue(){
    if(this.logicValue == null){
      alert("Please add value of field!")
    }else{
      this.queryLogical = this.queryLogical + this.logicValue;
      this.states.push(this.logicValue);
      this.logicValue = null;
      this.fieldAdded = false;
      this.valueAded = true;

      if(this.countFields > 1){
        this.logicButton = true;

      }

    }
  }

  undo(){
    let value = this.states[this.states.length-1];
    if(value == "AND" || value == "OR" || value =="NOT"){
      this.operationAdded = false;
      this.logicButton = true;
      this.queryLogical = this.queryLogical.substring(0,this.queryLogical.length-(value.length +2));


    }else if(value.includes("=")){
      this.fieldAdded = false;
      this.operationAdded = true;
      this.logicButton = false;
      this.valueAded = false;
      this.countFields = this.countFields - 1;

      if(this.states.length == 1){
        this.firstField = true;
        this.operationAdded = false;
      }
      this.queryLogical = this.queryLogical.substring(0,this.queryLogical.length-value.length);


    }else{
      this.logicValue == null;
      this.fieldAdded = true;
      this.logicButton = false;
      this.valueAded = false;
      this.queryLogical = this.queryLogical.substring(0,this.queryLogical.length-value.length);

    }
    this.states.pop();
    console.log(value);
  }

  logicSearch(){
    console.log(this.queryLogical);
    let x = this.searchService.searchLogical(new LogicalQueryDTO(this.queryLogical));
    x.subscribe(
      data=>{
        this.listSearch = data;
        console.log(data);

      }
    )

  }

  makeStringAuthor(author){
    let field = author.first_name + " " + author.last_name + " " + author.city;
    return {value:field};
  }

  

}
