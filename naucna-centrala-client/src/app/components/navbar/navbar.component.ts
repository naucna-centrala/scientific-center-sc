import { Component, OnInit, Output, EventEmitter,HostBinding, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { fromEvent } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  share,
  throttleTime
} from 'rxjs/operators';

enum VisibilityState {
  Visible = 'visible',
  Hidden = 'hidden'
}

enum Direction {
  Up = 'Up',
  Down = 'Down'
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  styles: [
    `
      :host {
        position: fixed;
        top: 0;
        width: 100%;
      }
    `
  ],
  animations: [
    trigger('toggle', [
      state(
        VisibilityState.Hidden,
        style({ opacity: 0, transform: 'translateY(-100%)' })
      ),
      state(
        VisibilityState.Visible,
        style({ opacity: 1, transform: 'translateY(0)' })
      ),
      transition('* => *', animate('200ms ease-in'))
    ])
  ]
})
export class NavbarComponent implements OnInit,AfterViewInit {

  private isVisible = true;

  @HostBinding('@toggle')
  get toggle(): VisibilityState {
    return this.isVisible ? VisibilityState.Visible : VisibilityState.Hidden;
  }

  @Output() featureSelected = new EventEmitter<string>();
  showView: string = 'guest';
  private roles: string[];
 
  constructor(private router: Router, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
  
  }
  ngAfterViewInit() {
    const scroll$ = fromEvent(window, 'scroll').pipe(
      throttleTime(10),
      map(() => window.pageYOffset),
      pairwise(),
      map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
      distinctUntilChanged(),
      share()
    );

    const goingUp$ = scroll$.pipe(
      filter(direction => direction === Direction.Up)
    );

    const goingDown$ = scroll$.pipe(
      filter(direction => direction === Direction.Down)
    );

    goingUp$.subscribe(() => (this.isVisible = true));
    goingDown$.subscribe(() => (this.isVisible = false));
  }

  administratorView(){
    this.showView = 'admin';
  }

  authorView(){
    this.showView = 'author';
  }

  editorView(){
    this.showView = 'editor';
  }

  guestView(){
    this.showView = 'guest';
  }

  reviewerView(){
    this.showView = 'reviewer'
  }


  clickButtonLogin() {
    this.featureSelected.emit('login');
}

  clickButtonSignOut(){
    this.featureSelected.emit('logout');
  }

  addMagazineClicked(){
    this.featureSelected.emit('addmagazine');
  }

  clickButtonRegistration() {
    this.featureSelected.emit('registration');
}

  confirmReviewers(){
    this.featureSelected.emit('confirm');
  }

  activateMagazine(){
    this.featureSelected.emit('active');

  }

  homeClick(){
    this.featureSelected.emit('home');
  }


}
