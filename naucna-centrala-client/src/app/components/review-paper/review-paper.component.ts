import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';
import { ProcessService } from 'src/app/service/process.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { PaperService } from 'src/app/service/paper.service';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';

@Component({
  selector: 'app-review-paper',
  templateUrl: './review-paper.component.html',
  styleUrls: ['./review-paper.component.css']
})
export class ReviewPaperComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  tasks = [];
  enumR = [];
  listOfValues = [];

  constructor(private router: Router,private processService: ProcessService, private tokenService: TokenStorageService,
    private paperService: PaperService) { }

  ngOnInit() {

    this.tokenService.getAuthorities().forEach(element => {
      if(element == 'AUTHOR'){
        this.header.authorView();
      }else if(element == 'EDITOR'){
        this.header.editorView();
      }else if(element == 'REVIEWER'){
        this.header.reviewerView();
      }
    });

    this.getTasks();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }
    
  }

  getTasks(){
    
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Review paper");

    x.subscribe(
      res => {
        console.log(res);
        this.tasks = res;
      },
      err => {
        console.log("Error occured");
      }
    );

  }

  loadPicture(name){

    window.open('assets/papers/' + name +".pdf", '_blank', 'fullscreen=yes');
  
  }

  createRange(index){
    this.enumR = [];
    index.formFields.forEach(element => {
      if(element.name == "recommendation"){
        this.enumR = Object.keys(element.value.value);
      }
    });
  
    var items: number[] = [];
    items.push(1);
  
    return items;
  
  }

  confirm(value, index){
    this.listOfValues = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);
      
      this.listOfValues.push(new FormDataDTO(property,value[property]));
    }

    let x = this.processService.submitForm(this.listOfValues,index.taskId);
    x.subscribe(
      data=>{
        alert("Review finished!");
        this.getTasks();
      }
    )

  }
}
