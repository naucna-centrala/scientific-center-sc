import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../../navbar/navbar.component';
import { Router } from '@angular/router';
import { ProcessService } from 'src/app/service/process.service';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';

@Component({
  selector: 'app-activate-magazine',
  templateUrl: './activate-magazine.component.html',
  styleUrls: ['./activate-magazine.component.css']
})
export class ActivateMagazineComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  private tasks = [];
  confirm_status: string;
  listOfValues: FormDataDTO[] = [];
  list_reviewers = [];
  list_editors= [];
  list_fields = [];
  loaded = "false";

  constructor(private router: Router,private magazineService: ProcessService) { }

  ngOnInit() {
    this.header.administratorView();
    this.getConfirmReviewerTasks();

  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'confirm'){
      this.router.navigate(['admin']);
    }else if(feature == 'active'){
      this.router.navigate(['activate/magazine']);
    }else if(feature == 'home'){
      this.router.navigate(['admin']);

    }
    
  }

  getConfirmReviewerTasks(){
    let x = this.magazineService.getTasksUsername("admin", "Activate magazine");

    x.subscribe(
      res => {
        console.log(res);
        this.tasks = res;
        this.loaded = "true";
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  confirm(value,index){
    //index-ceo task
    this.listOfValues = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);
      
      this.confirm_status = value[property];
      this.listOfValues.push(new FormDataDTO('Active',this.confirm_status));

      let x = this.magazineService.submitForm(this.listOfValues,index.taskId);

      x.subscribe(
        res => {
          alert("Status of magazine successfully changed!")
          console.log("Sacuvano");
          this.listOfValues = [];
          this.getConfirmReviewerTasks();
          
        },
        err => {
          console.log("Error occured");
        }
      );
  }
}

}
