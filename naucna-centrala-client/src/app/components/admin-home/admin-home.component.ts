import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';
import { ProcessService } from 'src/app/service/process.service';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';


@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  private tasks = [];
  confirm_status: string;
  listOfValues: FormDataDTO[] = [];
  loaded = "false";


  constructor(private router: Router, private registrationService: ProcessService) { }

  ngOnInit() {
    this.header.administratorView();
    this.getConfirmReviewerTasks();

  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'confirm'){
      this.router.navigate(['admin']);
    }else if(feature == 'active'){
      this.router.navigate(['activate/magazine']);
    }else if(feature == 'home'){
      this.router.navigate(['/admin']);
    }
    
  }

  getConfirmReviewerTasks(){
    let x = this.registrationService.getTasksUsername("admin", "Confirm status");

    x.subscribe(
      res => {
        console.log(res);
        this.tasks = res;
        this.loaded = "true";
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  confirm(value,index){
    //index-ceo task
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);
      
      this.confirm_status = value[property];
      this.listOfValues.push(new FormDataDTO('Confirm status',this.confirm_status));

      let x = this.registrationService.submitForm(this.listOfValues,index.taskId);

      x.subscribe(
        res => {
          console.log("Sacuvano");
          this.getConfirmReviewerTasks();
          
        },
        err => {
          console.log("Error occured");
        }
      );
  }
}

 
    
}
