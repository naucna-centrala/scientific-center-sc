import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { ProcessService } from 'src/app/service/process.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { FormDataObjectDTO } from 'src/app/dto/FormDataObjectDTO';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FormMixDataSubmitDTO } from 'src/app/dto/FormMixDataSubmitDTO';
import { Coauthor } from 'src/app/model/Coauthor';
import { PaperService } from 'src/app/service/paper.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { OrderDTO } from 'src/app/dto/OrderDTO';
import { MagazineService } from 'src/app/service/magazine.service';

@Component({
  selector: 'app-publish-paper',
  templateUrl: './publish-paper.component.html',
  styleUrls: ['./publish-paper.component.css']
})
export class PublishPaperComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  role:string;
  formFields: any;
  processInstance: any;
  taskId: any;
  view :string ="firstPage";
  listOfValues: FormDataObjectDTO[] = [];
  enumValues = [];
  productForm: FormGroup;
  moreCoauthors = false;
  listCoauthors: Coauthor[] = [];
  magazine:any;
  orderDTO: OrderDTO = new OrderDTO();
  link:any;
  viewStart: string = "first";

  constructor(private tokenService:TokenStorageService, private processService: ProcessService,
    private router: Router, private fb: FormBuilder, private paperService: PaperService,
    private route: ActivatedRoute, private magazineService:MagazineService) { }

  ngOnInit() {
    this.tokenService.getAuthorities().forEach(element => {
      if(element == 'AUTHOR'){
        this.header.authorView();
        this.role = element;
      }else if(element == 'EDITOR'){
        this.header.editorView();
        this.role = element;
      }else if(element == 'REVIEWER'){
        
      }
    });
    
    this.productForm = this.fb.group({
      time: this.fb.array([])
    });
   
}

  showCreate(){

    let id = this.route.snapshot.paramMap.get('id');
    if(id == "no"){
      this.startProcess();
    }else{
      this.processInstance = id;
      this.sendMessage();
    }
    this.viewStart = "start";

  }

  sendMessage(){
    let x = this.processService.sendMessage(this.processInstance);
    x.subscribe(
      data=>{
        console.log(data.message);
        this.getNextTask();
      }
    )
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'home'){
      if(this.role == 'AUTHOR'){
        this.router.navigate(['/author']);
      }
      
    }else if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    } 
  }

  startProcess(){
    let x = this.processService.startProcess("publish_paper",this.tokenService.getUsername());

    x.subscribe(
      data=>{
        this.formFields = data.formFields;
        this.processInstance = data.processInstanceId;
        this.taskId = data.taskId;
        console.log(this.formFields);
      }
    )
  }

  chooseMagazine(m){
    console.log(m);
    this.magazine = m;
    let listScientific = [];
    listScientific.push(m);
    this.listOfValues.push(new FormDataObjectDTO("magazine",listScientific));

    let x = this.processService.submitFormObject(this.listOfValues,this.taskId);
    x.subscribe(
      data=>{
        this.getNextTask();
      }
    )
  }

  getNextTask(){
    let x = this.processService.getNextForm(this.processInstance, this.tokenService.getUsername());
    x.subscribe(
      data=>{

        if(data == null){
          this.paySubscription();
        }else{
          this.formFields = data.formFields;
          this.taskId = data.taskId;
          this.view = "secondPage";
          console.log(this.formFields);
          this.formFields.forEach( (field) =>{
              if( field.value.type=='Object' && field.name == "paper_fields"){
                field.value.value.forEach((f)=>{
                this.enumValues.push(f);
                console.log(f);
            })
                    
            }
            });
        }
      }
    )
  }

  paySubscription(){
    console.log(this.magazine.uuid);
    this.orderDTO.description ="Pay subscription for paper in magazine";
    this.orderDTO.failUrl ="https://localhost:4200/fail";
    this.orderDTO.sellerUuid = this.magazine.uuid;
    this.orderDTO.successUrl = "https://localhost:4200/publish/paper/" + this.processInstance;
    this.magazineService.buyMagazine(this.orderDTO)
      .subscribe(data=>{
        console.log(data.transactionId)
        console.log(data.uuid)
        console.log(data.amount)
        this.link = "https://localhost:4000/payment/" + data.transactionId + "/" + data.uuid + "/" + data.amount;
        console.log(this.link)
        window.location.href = this.link;

      })
  }

  addCoauthors(){
    this.Times.push(this.fb.group({f_name: '',l_name:'',email:'',city:'',state:''}));
    this.moreCoauthors = true;
  }

  get Times() {
    return this.productForm.get('time') as FormArray;
  }

  saveMainInfo(value){
    let listValues = [];
    this.listOfValues = [];
    let listScientific = [];
    for (var property in value) {
      if(property == 'paper_fields'){
        console.log(value[property]);
        listScientific.push(value[property]);
      }else{
        console.log(property);
        console.log(value[property]);
        listValues.push(new FormDataDTO(property,value[property]));
      }
      
    }
    this.listOfValues.push(new FormDataObjectDTO("picked_field", listScientific));
    let y = this.processService.submitMixForm(new FormMixDataSubmitDTO(listValues,this.listOfValues),this.taskId);
    y.subscribe(
      d=>{
        let x = this.processService.getNextForm(this.processInstance, this.tokenService.getUsername());
          x.subscribe(
            data=>{
            
              this.formFields = data.formFields;
              this.taskId = data.taskId;
              this.view = "thirdPage";
              console.log(this.formFields);
              
            }
          )
      }
    )
    
  }

  saveCoauthors(){
    this.listOfValues = [];
    this.listCoauthors = [];
    for (var i = 0; i < this.productForm.value.time.length; i++){
      let coauthor = new Coauthor();
      coauthor.city = this.productForm.value.time[i].city;
      coauthor.email = this.productForm.value.time[i].email;
      coauthor.first_name = this.productForm.value.time[i].f_name;
      coauthor.last_name = this.productForm.value.time[i].l_name;
      coauthor.state = this.productForm.value.time[i].state;
      
      this.listCoauthors.push(coauthor);
      
    }
    this.listOfValues.push(new FormDataObjectDTO("list_coauthors", this.listCoauthors));
    console.log(this.listOfValues)
    this.submitCoauthors();

  }

  skipCoauthors(){

    this.listOfValues = [];
    this.listOfValues.push(new FormDataObjectDTO("list_coauthors", this.listCoauthors));
    this.submitCoauthors();
  }

  submitCoauthors(){
    let x = this.processService.submitFormObject(this.listOfValues,this.taskId);
    x.subscribe(
      r=>{
        let y = this.processService.getNextForm(this.processInstance,this.tokenService.getUsername());
        y.subscribe(
          data=>{
            this.formFields = data.formFields;
            this.taskId = data.taskId;
            this.view = "fourPage";
            console.log(this.formFields);
          }
        )
      }
    )
  }

  isEmptyDrop = true;
  localPDF;
  pdfFile;
  pdfSrc;
  pdfBufferRender;
  selectedFiles: FileList;

  pdfOnload(event) {
    this.selectedFiles = event.target.files;
    const pdfTatget: any = event.target;
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
        this.localPDF = this.pdfSrc;
      };
      this.pdfBufferRender = pdfTatget.files[0];
      reader.readAsArrayBuffer(pdfTatget.files[0]);
    }
    this.isEmptyDrop = false;
  }

  progress: { percentage: number } = { percentage: 0 };
  currentFileUpload: File;

  uploadDocument(){
    let listValues = [];
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.paperService.uploadDocument(this.currentFileUpload).subscribe(event => {
      listValues.push(new FormDataDTO("file_name",event.message));
      let x = this.processService.submitForm(listValues,this.taskId);
      x.subscribe(
        data=>{
          alert("Paper successfully added!");
          // this.router.navigate(['/author']);
        }
      )
      }
    );
  }


}
