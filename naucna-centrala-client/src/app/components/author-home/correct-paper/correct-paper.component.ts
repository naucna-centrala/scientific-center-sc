import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../../navbar/navbar.component';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { Router } from '@angular/router';
import { ProcessService } from 'src/app/service/process.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { PaperService } from 'src/app/service/paper.service';

@Component({
  selector: 'app-correct-paper',
  templateUrl: './correct-paper.component.html',
  styleUrls: ['./correct-paper.component.css']
})
export class CorrectPaperComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  tasks = [];
  listOfValues: FormDataDTO[] = [];
  tasksR = [];
  showView: string = "begin";
  loaded = "false";

  constructor(private router: Router,private processService: ProcessService,
     private tokenService:TokenStorageService, private paperService: PaperService) { }

  ngOnInit() {
    this.header.authorView();
    // this.getTasks();
    this.showView = "begin";
  }

  showCorrectFormat(){
    this.getTasksCorrect();
    this.showView = "format";
  }

  showCorrectionReview(){
    this.getTasksReview();
    this.showView = "review";
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }
    
  }

  getTasks(){
    this.getTasksCorrect();
    this.getTasksReview();
  }

  getTasksCorrect(){
    
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Correct paper");

    x.subscribe(
      res => {
        console.log(res);
        this.tasks = res;
        this.loaded = "true";
      },
      err => {
        console.log("Error occured");
      }
    );

  }

  getTasksReview(){
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Correct paper after review");

    x.subscribe(
      res => {
        console.log(res);
        this.tasksR = res;
        this.loaded = "true";

      },
      err => {
        console.log("Error occured");
      }
    );
  }

  loadPicture(name){

    window.open('assets/papers/' + name +".pdf", '_blank', 'fullscreen=yes');
  
  }

  isEmptyDrop = true;
  localPDF;
  pdfFile;
  pdfSrc;
  pdfBufferRender;
  selectedFiles: FileList;

  pdfOnload(event) {
    this.selectedFiles = event.target.files;
    const pdfTatget: any = event.target;
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
        this.localPDF = this.pdfSrc;
      };
      this.pdfBufferRender = pdfTatget.files[0];
      reader.readAsArrayBuffer(pdfTatget.files[0]);
    }
    this.isEmptyDrop = false;
  }

  progress: { percentage: number } = { percentage: 0 };
  currentFileUpload: File;

  confirm(value,index){
    var file;
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);
      
      if(property == "file_name"){
        this.deleteDocument(value[property]);
         
      }else{
        this.listOfValues.push(new FormDataDTO(property,value[property]));
      }
      
    }
    this.currentFileUpload = this.selectedFiles.item(0);
    this.paperService.uploadDocument(this.currentFileUpload)
    .subscribe(event => {
      this.listOfValues.push(new FormDataDTO("file_name",event.message));
      this.submitTask(index.taskId);
      }
    );
  }

  submitTask(taskId){
    let x = this.processService.submitForm(this.listOfValues,taskId);
      x.subscribe(
        data=>{
          alert("Paper successfully corrected!");
          this.getTasks();
        }
      )
  }

  deleteDocument(filename){

    let x = this.paperService.deleteDocument(filename);
    x.subscribe(
      data=>{ })
  }



}
