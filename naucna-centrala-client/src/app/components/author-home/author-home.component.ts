import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { MagazineService } from 'src/app/service/magazine.service';
import { Magazine } from 'src/app/model/Magazine';
import { SubscriptionRequestNCDTO } from 'src/app/dto/SubscriptionRequestNCDTO';
import { SubscriptionService } from 'src/app/service/subscription.service';
import { MagazinePricesDTO } from 'src/app/dto/MagazinePricesDTO';
import { PaperService } from 'src/app/service/paper.service';

@Component({
  selector: 'app-author-home',
  templateUrl: './author-home.component.html',
  styleUrls: ['./author-home.component.css']
})
export class AuthorHomeComponent implements OnInit {

  username:string;
  listNotSubscribe: Magazine[] = [];
  listPrices: MagazinePricesDTO[]= [];
  listPapers = [];
  listSubscribe = [];
  listOpenAccess = [];
  loaded = "false";

  @ViewChild("header") header: NavbarComponent;

  constructor(private router: Router, private tokenService:TokenStorageService,
     private magazineService:MagazineService, private subscribeService: SubscriptionService,
     private paperService: PaperService) { }

  ngOnInit() {
    this.header.authorView();
    this.username = this.tokenService.getUsername();
    // this.checkSubscription();
    this.getSubscribe();
    this.getNotSubscribe();
    this.getPapers();
    this.getOpenAccess();
  }

  checkSubscription(){
    let x = this.subscribeService.checkSubscription(this.tokenService.getUsername());
    x.subscribe(
      d=>{
        console.log(d);
        this.getSubscribe();
        this.getNotSubscribe();
        this.loaded = "true";
      }
    )
  }

  getPapers(){
    let x = this.paperService.getPapers(this.tokenService.getUsername());
    x.subscribe(
      data=>{
        this.listPapers = data;
      }
    )
  }

  getNotSubscribe(){
    let x = this.magazineService.getNotSubscribe(this.username);
    x.subscribe(
      data=>{
        this.listNotSubscribe = data;
        this.getPrices();

      }
    )
  }

  getPrices(){
    let x = this.magazineService.getPrices(this.listNotSubscribe);
    x.subscribe(
      data=>{
        this.listPrices = data;
      }
    )
  }

  getSubscribe(){
    let x = this.subscribeService.getAllSubscribe(this.username);
    x.subscribe(
      data=>{
        this.listSubscribe = data;
        console.log(this.listSubscribe);
        
      }
    )
  }

  getOpenAccess(){
    let x = this.magazineService.getOpenAccess();
    x.subscribe(
      data=>{
        this.listOpenAccess = data;
        console.log(this.listSubscribe);
        
      }
    )
  }

  makeString(m){
    let field = "";
    m.magazine_fields.forEach(element => {
      element.title = element.title.concat(" ");
      field = field.concat(element.title);
    });
    return {value:field};
  }
  
  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'home'){
      this.router.navigate(['/author']);
    }
    
  }

  subscription(id){
    let a = new SubscriptionRequestNCDTO(this.username,id);

    this.loaded = "false";

    let x = this.subscribeService.subscribe(a);
    x.subscribe(
      data=>{
        window.location.href = data.redirectUrl;
      }
    )
  }

  unsubscribe(m){
    let x = this.subscribeService.unsubscribe(m.subscriptionId);
    this.loaded = "false";
    x.subscribe(
      data=>{
        console.log(data);
        alert("Successfully unsubscribe!");
        this.getSubscribe();
        this.getNotSubscribe();
        this.loaded = "true";

      }
    )
  }

}
