import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarComponent } from '../../navbar/navbar.component';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { PaperService } from 'src/app/service/paper.service';

@Component({
  selector: 'app-paper-view',
  templateUrl: './paper-view.component.html',
  styleUrls: ['./paper-view.component.css']
})
export class PaperViewComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  listPapers = [];
  loaded = "false";

  constructor(private router: Router,private tokenService:TokenStorageService,
    private paperService: PaperService) { }

  ngOnInit() {
    this.header.authorView();
    this.getPapers();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'home'){
      this.router.navigate(['/author']);
    }
  }

  getPapers(){
    let x = this.paperService.getPapers(this.tokenService.getUsername());
    x.subscribe(
      data=>{
        this.listPapers = data;
        console.log(this.listPapers);
        this.loaded = "true";
      }
    )
  }

}
