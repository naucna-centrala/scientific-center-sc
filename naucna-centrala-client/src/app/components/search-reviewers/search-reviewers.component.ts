import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';
import { MagazineService } from 'src/app/service/magazine.service';
import { PaperService } from 'src/app/service/paper.service';
import { SearchReviewerDTO, SearchReviewerType } from 'src/app/dto/SearchReviewerDTO';
import { SearchService } from 'src/app/service/search.service';

@Component({
  selector: 'app-search-reviewers',
  templateUrl: './search-reviewers.component.html',
  styleUrls: ['./search-reviewers.component.css']
})
export class SearchReviewersComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  listPapers:any;
  start = true;
  choosenPaper:any= null;
  listReviewers:any;
  type:any;
  choosenField: string;
  choosenFilename:string;
  first = true;

  constructor(private router: Router, private paperService: PaperService, 
    private searchService: SearchService) { }

  ngOnInit() {
    this.header.guestView();
    this.loadPapers();
    this.loadReviewers();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'login'){
      // this.showLogin();
    } else if(feature == 'registration'){
      // this.showRegistration();
    }else if(feature == 'home'){
      this.router.navigate(['']);
    }
  }

  loadPapers(){
    let x = this.paperService.getAllPapers();
    x.subscribe(
      data=>{
        this.listPapers = data;
        console.log(data);
      }
    )
  }

  loadReviewers(){
    let x = this.paperService.getAllReviewers();
    x.subscribe(
      data=>{
        this.listReviewers = data;
        console.log(data)
      }
    )
  }

  choosePaper(m){

    this.choosePaper = m;
    this.choosenField = m.paper_fields.title;
    this.choosenFilename = m.filename;
    this.start =false;
    console.log(this.choosePaper);

  }

  makeString(m){
    let field = "";
    if(this.first){
      m.user_fields.forEach(element => {
        element.title = element.title.concat(" ");
        field = field.concat(element.title);
      });
    }else{
      m.fields.forEach(element => {
        element.title = element.title.concat(" ");
        field = field.concat(element.title);
      });
    }
    
    return {value:field};
  }

  makeStringAuthor(author){
    let field = author.first_name + " " + author.last_name + " " + author.city;
    return {value:field};
  }

  filter(){
    let query = new SearchReviewerDTO();
    console.log(this.type);
    if(this.type == "REGULAR"){
      query.field = "fields.title"
      query.searchType = SearchReviewerType.REGULAR;
      query.value = this.choosenField;
      this.searchReviewer(query);
      
      
    }else if(this.type == "MORE_LIKE_THIS"){
      query.value = this.choosenFilename;
     
      query.searchType = SearchReviewerType.MORE_LIKE_THIS;
      query.field=""
      this.searchReviewer(query);

    }else if(this.type == "GEO"){
      query.value = this.choosenFilename;
     
      query.searchType = SearchReviewerType.GEO;
      query.field="";
      this.searchReviewer(query);
    }else if(this.type == "ALL"){
      this.first = true;
      this.loadReviewers();
    }
  }

  searchReviewer(query){
    let x = this.searchService.searchReviewer(query);
      x.subscribe(
        data=>{
          this.first= false;
          this.listReviewers = data;
          console.log(data);
        }
      )
  }

}
