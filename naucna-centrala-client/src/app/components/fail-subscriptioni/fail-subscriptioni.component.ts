import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/auth/token-storage.service';

@Component({
  selector: 'app-fail-subscriptioni',
  templateUrl: './fail-subscriptioni.component.html',
  styleUrls: ['./fail-subscriptioni.component.css']
})
export class FailSubscriptioniComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  role:string;

  constructor(private router: Router,private tokenService:TokenStorageService) { }

  ngOnInit() {
    this.tokenService.getAuthorities().forEach(element => {
      if(element == 'AUTHOR'){
        this.header.authorView();
        this.role = element;
      }else if(element == 'EDITOR'){
        this.header.editorView();
        this.role = element;
      }else if(element == 'REVIEWER'){   
      }
    });
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'home'){
      if(this.role == 'AUTHOR'){
        this.router.navigate(['/author']);
      }else if(this.role == 'EDITOR'){
        this.router.navigate(['/editor']);
  
      }
      
    }else if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    } 
  }

  back(){
    if(this.role == 'AUTHOR'){
      this.router.navigate(['/author']);
    }else if(this.role == 'EDITOR'){
      this.router.navigate(['/editor']);

    }
  }

}
