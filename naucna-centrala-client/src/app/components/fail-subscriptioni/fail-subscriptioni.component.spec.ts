import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FailSubscriptioniComponent } from './fail-subscriptioni.component';

describe('FailSubscriptioniComponent', () => {
  let component: FailSubscriptioniComponent;
  let fixture: ComponentFixture<FailSubscriptioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FailSubscriptioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FailSubscriptioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
