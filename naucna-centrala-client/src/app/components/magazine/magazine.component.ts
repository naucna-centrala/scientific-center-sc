import { Component, OnInit, ViewChild } from '@angular/core';
import { MagazineService } from 'src/app/service/magazine.service';
import { OrderDTO } from 'src/app/dto/OrderDTO';
import { HttpClient } from '@angular/common/http';
import { MagazinePricesDTO } from 'src/app/dto/MagazinePricesDTO';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';
import { PaperService } from 'src/app/service/paper.service';

@Component({
  selector: 'app-magazine',
  templateUrl: './magazine.component.html',
  styleUrls: ['./magazine.component.css']
})
export class MagazineComponent implements OnInit {

  magazines: any;
  orderDTO: OrderDTO = new OrderDTO();
  link : string;
  listPrices: MagazinePricesDTO[]= [];
  @ViewChild("header") header: NavbarComponent;
  role:string;
  loaded = "true";

  constructor(private magazineService:MagazineService, private tokenService:TokenStorageService,
    private router:Router) { }

  ngOnInit() {
    this.tokenService.getAuthorities().forEach(element => {
      if(element == 'AUTHOR'){
        this.header.authorView();
        this.role = element;
      }else if(element == 'EDITOR'){
        this.header.editorView();
        this.role = element;
      }else if(element == 'REVIEWER'){
        
      }
    });
    this.magazineService.getNotSubscribe(this.tokenService.getUsername())
      .subscribe(data =>{
        this.magazines = data;
        this.getPrices();
      })
    
   
  }

  makeString(m){
    let field = "";
    m.magazine_fields.forEach(element => {
      element.title = element.title.concat(" ");
      field = field.concat(element.title);
    });
    return {value:field};
  }

  getPrices(){
    let x = this.magazineService.getPrices(this.magazines);
    x.subscribe(
      data=>{
        this.listPrices = data;
      }
    )
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'home'){
      if(this.role == 'AUTHOR'){
        this.router.navigate(['/author']);
      }
      
    }else if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    } 
  }

  buyMagazine(m){
    this.orderDTO.description ="Order something";
    this.orderDTO.failUrl ="https://localhost:4200/fail";
    this.orderDTO.sellerUuid = m.uuid;
    this.orderDTO.successUrl = "https://localhost:4200/success"
    this.loaded = "false";
    this.magazineService.buyMagazine(this.orderDTO)
      .subscribe(data=>{
        console.log(data.transactionId)
        console.log(data.uuid)
        console.log(data.amount)
        this.link = "https://localhost:4000/payment/" + data.transactionId + "/" + data.uuid + "/" + data.amount;
        console.log(this.link)
        window.location.href = this.link;

      })


  }

}
