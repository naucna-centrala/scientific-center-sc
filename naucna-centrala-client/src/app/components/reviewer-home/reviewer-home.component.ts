import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reviewer-home',
  templateUrl: './reviewer-home.component.html',
  styleUrls: ['./reviewer-home.component.css']
})
export class ReviewerHomeComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;

  constructor(private router: Router) { }

  ngOnInit() {
    this.header.reviewerView();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'home'){
      this.router.navigate(['/reviewer']);
    }
    
  }

}
