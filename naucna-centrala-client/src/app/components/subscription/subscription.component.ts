import { Component, OnInit, ViewChild } from '@angular/core';
import { MagazineService } from 'src/app/service/magazine.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { MagazinePricesDTO } from 'src/app/dto/MagazinePricesDTO';
import { SubscriptionRequestNCDTO } from 'src/app/dto/SubscriptionRequestNCDTO';
import { SubscriptionService } from 'src/app/service/subscription.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {

  magazines: any;
  link : string;
  listPrices: MagazinePricesDTO[]= [];
  @ViewChild("header") header: NavbarComponent;
  role:string;

  constructor(private magazineService:MagazineService, private tokenService:TokenStorageService,
    private router:Router, private subscribeService: SubscriptionService) { }

  ngOnInit() {
    this.tokenService.getAuthorities().forEach(element => {
      if(element == 'AUTHOR'){
        this.header.authorView();
        this.role = element;
      }else if(element == 'EDITOR'){
        this.header.editorView();
        this.role = element;
      }else if(element == 'REVIEWER'){
        
      }
    });
    this.magazineService.getNotSubscribe(this.tokenService.getUsername())
      .subscribe(data =>{
        this.magazines = data;
        this.getPrices();
      })
  }

  makeString(m){
    let field = "";
    m.magazine_fields.forEach(element => {
      element.title = element.title.concat(" ");
      field = field.concat(element.title);
    });
    return {value:field};
  }

  getPrices(){
    let x = this.magazineService.getPrices(this.magazines);
    x.subscribe(
      data=>{
        this.listPrices = data;
      }
    )
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'home'){
      if(this.role == 'AUTHOR'){
        this.router.navigate(['/author']);
      }
      
    }else if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    } 
  }

  subscription(id){
    let a = new SubscriptionRequestNCDTO(this.tokenService.getUsername(),id);

    let x = this.subscribeService.subscribe(a);
    x.subscribe(
      data=>{
        window.location.href = data.redirectUrl;
      }
    )
  }

}
