import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessMagazineAddComponent } from './success-magazine-add.component';

describe('SuccessMagazineAddComponent', () => {
  let component: SuccessMagazineAddComponent;
  let fixture: ComponentFixture<SuccessMagazineAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessMagazineAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessMagazineAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
