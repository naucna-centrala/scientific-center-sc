import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { ProcessService } from 'src/app/service/process.service';
import { MagazineService } from 'src/app/service/magazine.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';

@Component({
  selector: 'app-success-magazine-add',
  templateUrl: './success-magazine-add.component.html',
  styleUrls: ['./success-magazine-add.component.css']
})
export class SuccessMagazineAddComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  processInstance: string;
  view:string = 'first';
  role:string;

  constructor(private router: Router, private route: ActivatedRoute, private magazineService: MagazineService,
    private tokenService: TokenStorageService) { }

  ngOnInit() {
    this.tokenService.getAuthorities().forEach(element => {
      if(element == 'AUTHOR'){
        this.header.authorView();
        this.role = element;
      }else if(element == 'EDITOR'){
        this.header.editorView();
        this.role = element;
      }else if(element == 'REVIEWER'){   
      }
    });

    this.processInstance = this.route.snapshot.paramMap.get('id');

    let x = this.magazineService.continueProcessAddMagazine(this.processInstance);
    x.subscribe(
      data=>{
        if(data == null){
          this.view = 'second';
        }  
      }
    )


  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }else if(feature == 'home'){
      if(this.role == 'EDITOR'){
        this.router.navigate(['/editor']);
  
      }
    }else if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }
    
  }

  back(){
    if(this.role == 'EDITOR'){
      this.router.navigate(['/editor']);

    }
  }

}
