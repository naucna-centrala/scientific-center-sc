import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';

@Component({
  selector: 'app-editor-home',
  templateUrl: './editor-home.component.html',
  styleUrls: ['./editor-home.component.css']
})
export class EditorHomeComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;

  constructor(private router: Router) { }

  ngOnInit() {
    this.header.editorView();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }else if(feature == 'home'){
      this.router.navigate(['/editor']);
    }
    
  }

}
