import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReviewerPaperComponent } from './add-reviewer-paper.component';

describe('AddReviewerPaperComponent', () => {
  let component: AddReviewerPaperComponent;
  let fixture: ComponentFixture<AddReviewerPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReviewerPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReviewerPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
