import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../../navbar/navbar.component';
import { Router } from '@angular/router';
import { PaperService } from 'src/app/service/paper.service';
import { ProcessService } from 'src/app/service/process.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { FormMixDataSubmitDTO } from 'src/app/dto/FormMixDataSubmitDTO';
import { FormDataObjectDTO } from 'src/app/dto/FormDataObjectDTO';

@Component({
  selector: 'app-add-reviewer-paper',
  templateUrl: './add-reviewer-paper.component.html',
  styleUrls: ['./add-reviewer-paper.component.css']
})
export class AddReviewerPaperComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  tasksNumber = [];
  listOfValues: FormDataDTO[]=[];
  tasksReviewers=[];
  enumR = [];
  check:any;
  tasksDeadline=[];

  constructor(private router: Router,private processService: ProcessService, private tokenService: TokenStorageService,
    private paperService: PaperService) { }

  ngOnInit() {
    this.header.editorView();
    this.getTasks();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }
    
  }

  getTasks(){
    this.getTasksNumber();
    this.getTasksReviewers();
    this.getTasksDeadline();
  }

  getTasksNumber(){
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Number of reviewers");

    x.subscribe(
      res => {
        console.log(res);
        this.tasksNumber = res;
      },
      err => {
        console.log("Error occured");
      }
    );

  }
  getTasksReviewers(){
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Add reviewers to paper");

    x.subscribe(
      res => {
        console.log(res);
        this.tasksReviewers = res;
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  getTasksDeadline(){
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Write the deadline for reviewers");

    x.subscribe(
      res => {
        console.log(res);
        this.tasksDeadline = res;
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  confirmNumber(value,index){
    //index-ceo task 
    this.listOfValues = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);
               
      let check = this.checkNumber(index,value[property]);

      if(check){
        this.listOfValues.push(new FormDataDTO(property,value[property]));
        let x = this.processService.submitForm(this.listOfValues,index.taskId);

        x.subscribe(
          res => {
            alert("Number of reviewers confirmed!")
            this.listOfValues = [];
            this.getTasks();
            
          },
          err => {
            console.log("Error occured");
          }
        );

      }else{
        alert("Number not valid! Please enter again!")
      }
      
    }
}

checkNumber(index, checkSize){
  let size;
  index.formFields.forEach(element => {
    if(element.name == "list_all_reviewers"){
      size = element.value.value.length;
    }
  });
  let min;

  if(size < 2){
    min = size;
  }else{
    min = 2;
  }

  if(size < checkSize || checkSize<=0 || checkSize<min){
    return false;
  }else{
    return true;
  }
}
createRange(index){
  this.enumR = [];
  let size;
  index.formFields.forEach(element => {
    if(element.name == "Number of reviewers for paper"){
      size = element.value.value;
    }else if(element.name == "list_all_reviewers"){
      element.value.value.forEach(e=>{
        this.enumR.push(e);
      })
    }
  });

  var items: number[] = [];

  for(var i = 1; i <= size; i++){
    items.push(i);
  } 
   
  return items;

}

checkFields(list,value){
  let i = 0;
  list.forEach(field=>{
    if(JSON.stringify(field) === JSON.stringify(value)){
      this.check = true;
      i = 1;
    }
  })
  if(i == 0){
    this.check = false;
  }
}

confirmReviewer(value, index){
  this.listOfValues = [];
  let listReviewers = [];
  let finalList = [];
  
  for (var property in value) {
    console.log(property);
    console.log(value[property]);
    console.log(index.taskId);
    
    this.checkFields(listReviewers,value[property]);
    if(this.check == true){
      alert("Please choose different fields!");
      return;
    }

    listReviewers.push(value[property].username);
  }

  this.listOfValues.push(new FormDataDTO("chosen_reviewers", "complete"));
  finalList.push(new FormDataObjectDTO("picked_reviewers",listReviewers));

  let listMixed = new FormMixDataSubmitDTO(this.listOfValues,finalList) ;
  let x = this.processService.submitMixForm(listMixed,index.taskId);
  x.subscribe(
    data=>{
      alert("Reviewers picked!");
      this.getTasks();
    }
  )

}

confirmDeadline(value, index){
  this.listOfValues = [];
  for (var property in value) {
    console.log(property);
    console.log(value[property]);
    console.log(index.taskId);

    var splited = value[property].split("-");
    var date = splited[2] + "/" + splited[1] + "/" + splited[0];
    console.log(date);
    
    this.listOfValues.push(new FormDataDTO(property,date));
    
  }

  let x = this.processService.submitForm(this.listOfValues,index.taskId);
  x.subscribe(
    data=>{
      alert("Deadline confirmed!");
      this.getTasks();
    }
  )

}


}
