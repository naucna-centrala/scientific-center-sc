import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormatPaperComponent } from './format-paper.component';

describe('FormatPaperComponent', () => {
  let component: FormatPaperComponent;
  let fixture: ComponentFixture<FormatPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormatPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormatPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
