import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ProcessService } from 'src/app/service/process.service';
import { NavbarComponent } from '../../navbar/navbar.component';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { PaperService } from 'src/app/service/paper.service';

@Component({
  selector: 'app-format-paper',
  templateUrl: './format-paper.component.html',
  styleUrls: ['./format-paper.component.css']
})
export class FormatPaperComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  tasks = [];
  confirm_status: string;
  listOfValues: FormDataDTO[] = [];
  list_coauthors = [];

  constructor(private router: Router,private processService: ProcessService, private tokenService: TokenStorageService,
    private paperService: PaperService) { }

  ngOnInit() {
    this.header.editorView();
    this.getTasks();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }
    
  }

  getTasks(){
    
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Check format of paper");

    x.subscribe(
      res => {
        console.log(res);
        this.tasks = res;
      },
      err => {
        console.log("Error occured");
      }
    );

  }

  isEmptyDrop = true;
  pdfSrc;
  pdfBufferRender;

  loadPicture(name){

    window.open('assets/papers/' + name +".pdf", '_blank', 'fullscreen=yes');
  
  }

  confirm(value,index){
    //index-ceo task
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);
      
      if(property == "Check_format"){
        if(value[property] == ''){
          value[property] = false;  
        }

        this.confirm_status = value[property];
        this.listOfValues.push(new FormDataDTO('Check_format',this.confirm_status));
         
      }else{
        this.listOfValues.push(new FormDataDTO(property,value[property]));
      }
      
    }
      
    let x = this.processService.submitForm(this.listOfValues,index.taskId);

      x.subscribe(
        res => {
          alert("Check format done!")
          this.listOfValues = [];
          this.getTasks();
          
        },
        err => {
          console.log("Error occured");
        }
      );

}



}
