import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckThematicPaperComponent } from './check-thematic-paper.component';

describe('PaperTasksComponent', () => {
  let component: CheckThematicPaperComponent;
  let fixture: ComponentFixture<CheckThematicPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckThematicPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckThematicPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
