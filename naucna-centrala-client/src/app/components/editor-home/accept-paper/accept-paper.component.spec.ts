import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptPaperComponent } from './accept-paper.component';

describe('AcceptPaperComponent', () => {
  let component: AcceptPaperComponent;
  let fixture: ComponentFixture<AcceptPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
