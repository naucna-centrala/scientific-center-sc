import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ProcessService } from 'src/app/service/process.service';
import { NavbarComponent } from '../../navbar/navbar.component';
import { FormDataDTO } from 'src/app/dto/FormDataDTO';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { PaperService } from 'src/app/service/paper.service';

@Component({
  selector: 'app-accept-paper',
  templateUrl: './accept-paper.component.html',
  styleUrls: ['./accept-paper.component.css']
})
export class AcceptPaperComponent implements OnInit {

  @ViewChild("header") header: NavbarComponent;
  tasks = [];
  listOfValues: FormDataDTO[] = [];
  enumR = [];
  showDate = "false";
  tasksR = [];

  constructor(private router: Router,private processService: ProcessService, private tokenService: TokenStorageService,
    private paperService: PaperService) { }

  ngOnInit() {
    this.header.editorView();
    this.getTasks();
  }

  onNavigate(feature: string){
    console.log(feature);
    if(feature == 'logout') {
      window.sessionStorage.clear();
      this.router.navigate(['']);
      window.alert("Successfully Logged out!");
    }else if(feature == 'addmagazine'){
      this.router.navigate(['addmagazine']);
    }
    
  }

  getTasks(){
    this.getTasksA();
    this.getTasksR();
  }

  getTasksA(){
    
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Make a decision");

    x.subscribe(
      res => {
        console.log(res);
        this.tasks = res;
        this.showDate = "false";
      },
      err => {
        console.log("Error occured");
      }
    );

  }

  getTasksR(){
    let x = this.processService.getTasksUsername(this.tokenService.getUsername(), "Editor final decision");

    x.subscribe(
      res => {
        console.log(res);
        this.tasksR = res;
      },
      err => {
        console.log("Error occured");
      }
    )
  }

  loadPicture(name){

    window.open('assets/papers/' + name +".pdf", '_blank', 'fullscreen=yes');
  
  }

  createRange(index){
    this.enumR = [];
    index.formFields.forEach(element => {
      if(element.name == "decision"){
        this.enumR = Object.keys(element.value.value);
      }
    });
  
    var items: number[] = [];
    items.push(1);
  
    return items;
  
  }

  confirm(value, index){
    this.listOfValues = [];
    for (var property in value) {
      console.log(property);
      console.log(value[property]);
      console.log(index.taskId);

      if(property =="deadline"){
        if(this.showDate == "true"){
          var splited = value[property].split("-");
          var date = splited[2] + "/" + splited[1] + "/" + splited[0];
          value[property] = date;
          console.log(date);
          this.listOfValues.push(new FormDataDTO(property,value[property]));

        }
       
      }else{
        this.listOfValues.push(new FormDataDTO(property,value[property]));

      }
      
    }

    let x = this.processService.submitForm(this.listOfValues,index.taskId);
    x.subscribe(
      data=>{
        alert("Approval finished!");
        this.getTasks();
      }
    )

  }

  checkForDate(val){
    if(val == "1: accept_with_small_error" || val == "2: accept_conditionally"){
      this.showDate = "true";
    }else{
      this.showDate = "false";
    }
  }

}
